trigger UpdateContactAchievementProfiles on Contact (after insert, after update, after delete) {

    List<Achievement_Set__c> profiles = new List<Achievement_Set__c>();

    if (Trigger.isAfter && Trigger.isInsert) {
        profiles = new List<Achievement_Set__c>();

        for (Contact c : Trigger.new) {
            profiles.add(new AchievementUtils.AchievementSetBuilder('').asContactProfile(c).build());
        }

        insert profiles;

    } else if (Trigger.isAfter && Trigger.isUpdate) {
        Set<Id> contactIds = new Set<Id>();
        for (Contact c : Trigger.new) {
            if (c.FirstName != Trigger.oldMap.get(c.Id).FirstName || c.LastName != Trigger.oldMap.get(c.Id).LastName) {
                contactIds.add(c.Id);
            }
        }
        if (!contactIds.isEmpty()) {
            profiles = [SELECT Name, Contact__r.FirstName, Contact__r.LastName FROM Achievement_Set__c WHERE Contact__c IN :contactIds];
            for (Achievement_Set__c p : profiles) {
                new AchievementUtils.AchievementSetBuilder(p).asContactProfile(p.Contact__r);
            }
            update profiles;
        }

    } else if (Trigger.isAfter && Trigger.isDelete) {
        delete [SELECT Id FROM Achievement_Set__c WHERE Contact__c = null AND RecordTypeId=:AchievementUtils.getAchievementSetContactProfileRecordType().Id];
    }
    
}