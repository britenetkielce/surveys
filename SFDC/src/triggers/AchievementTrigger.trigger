trigger AchievementTrigger on Achievement__c (before insert, before update) {

    if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
        ID skillTypeId = AchievementUtils.getAchievementSkillRecordType().Id;
        ID groupTypeId = AchievementUtils.getAchievementGroupRecordType().Id;

        for (Achievement__c a : Trigger.new) {
            if (a.RecordTypeId != skillTypeId && a.RecordTypeId != groupTypeId) {
                a.Level_1_Details__c = null;
                a.Level_2_Details__c = null;
                a.Level_3_Details__c = null;
            }
        }
    }
}