trigger AchievementSetItemTrigger on Achievement_Set_Item__c (after insert, after update, after delete, after undelete) {

    if (Trigger.isAfter && !System.isBatch()) {
        List<Achievement_Set_Item__c> items = new List<Achievement_Set_Item__c>();
        if (Trigger.isInsert || Trigger.isUndelete) {
            items = Trigger.new;
        } else if (Trigger.isDelete) {
            items = Trigger.old;
        } else if (Trigger.isUpdate) {
            for (Achievement_Set_Item__c item : Trigger.new) {
                if (item.Level__c != Trigger.oldMap.get(item.Id).Level__c || item.Amount__c != Trigger.oldMap.get(item.Id).Amount__c ||
                    item.Min_Speciality_Match__c != Trigger.oldMap.get(item.Id).Min_Speciality_Match__c) {
                    items.add(item);
                }
            }
        }

        Set<ID> profileIds = new Set<ID>();
        for (Achievement_Set_Item__c item : items) {
            profileIds.add(item.Achievement_Set__c);
        }

        Set<ID> contactPRT = AchievementUtils.getContactProfileRecordTypeIds();
        Set<ID> contactTargetPRT = AchievementUtils.getContactTargetProfileRecordTypeIds();
        Set<ID> profilesRT = new Set<ID>(contactPRT);
        profilesRT.addAll(contactTargetPRT);

        Set<ID> contactProfileIds = new Set<ID>();
        Set<ID> contactTargetProfilesIds = new Set<ID>();
        for (Achievement_Set__c profile : AchievementService.getAchievementSets(profileIds, profilesRT, false)) {
            if (contactPRT.contains(profile.RecordTypeId)) {
                contactProfileIds.add(profile.Id);
            } else if (contactTargetPRT.contains(profile.RecordTypeId)) {
                contactTargetProfilesIds.add(profile.Id);
            }
        }

        if (contactProfileIds.isEmpty()) {
            contactProfileIds = null;
        }
        if (contactTargetProfilesIds.isEmpty()) {
            contactTargetProfilesIds = null;
        }

        if (contactProfileIds != null) {
            Set<ID> achievementIds = new Set<ID>();
            for (Achievement_Set_Item__c item : items) {
                if (contactProfileIds.contains(item.Achievement_Set__c)) {
                    achievementIds.add(item.Achievement__c);
                }
            }
            if (!achievementIds.isEmpty()) {
                achievementIds = AchievementService.getGroupsByMemberIds(achievementIds);
                AchievementService.calculateContactAchievementGroupValues(contactProfileIds, achievementIds);
            }
        }

        AchievementService.calculateContactProfileScores(contactProfileIds, contactTargetProfilesIds);
    }
}