public with sharing class SurveyDefaultService /*extends SurveyUtils.SurveyService*/ {

    public SurveyDefaultService() {
    }

/*
    public override SurveyModel.Template assemblyTemplate(TemplateRequest request, SurveyTemplate__c template, List<SurveyTemplateItem__c> surveyTemplateItems) {
        SurveyModel.Template templateDetails = new SurveyModel.Template(template);
        Set<ID> questionIds = new Map<ID, SurveyTemplateItem__c>(surveyTemplateItems).keySet();

        List<SurveyTemplateItem__c> items = [
            SELECT Name, Order__c, ServiceName__c, SurveyQuestion__c, 
                SurveyTemplate__r.Description__c,
                SurveyTemplate__r.StartDate__c,
                SurveyTemplate__r.EndDate__c,
                SurveyQuestion__r.Id,
                SurveyQuestion__r.Name,
                SurveyQuestion__r.RecordType.DeveloperName,
                SurveyQuestion__r.Question__c
            FROM SurveyTemplateItem__c 
            WHERE Id IN :questionIds
                AND SurveyQuestion__c != NULL 
                AND ServiceName__c = :getServiceName()
            ORDER BY Order__c ASC
            LIMIT :questionIds.size()];

        if(!items.isEmpty()){
            List<Id> selectItemIds = getItemSelectIds(items);
            Map<Id, List<SurveyModel.Option>> optionsMap = getQuestionItems(selectItemIds);
            for (SurveyTemplateItem__c item : items) {
                SurveyModel.Question question = new SurveyModel.Question(item, optionsMap.get(item.SurveyQuestion__r.Id));
                question.question = item.SurveyQuestion__r.Question__c;
                question.type = item.SurveyQuestion__r.RecordType.DeveloperName;
                templateDetails.addQuestion(question);
            }
        }

        return templateDetails; 
    }

    public override SurveyModel.Survey assemblySurvey(SurveyRequest request, SurveyTemplate__c template, Survey__c survey) {
        SurveyModel.Survey surveyDetails = new SurveyModel.Survey(survey);

        surveyDetails.answers = getSurveyAnswers(survey);

        return surveyDetails;
    }

    public override void saveSurvey(String rawSurvey, SurveyModel.Survey baseSurveyInfo, List<SurveyModel.Answer> baseSurveyInfoAnswers, SurveyTemplate__c template, Survey__c survey) {
        upsert survey;

        List<SurveyAnswer__c> surveyAnswer = new List<SurveyAnswer__c>();
        for (SurveyModel.Answer item: baseSurveyInfoAnswers) {
            surveyAnswer.add(new SurveyAnswer__c(Id = item.Id ,Survey__c = baseSurveyInfo.Id, Value__c = item.value, SurveyTemplateItem__c = item.questionId));
        }

        upsert surveyAnswer;
    }

    private List<SurveyModel.Answer> getSurveyAnswers(Survey__c survey) {
        List<SurveyModel.Answer> answers = new List<SurveyModel.Answer>();
            for (SurveyAnswer__c item: [SELECT Id, SurveyTemplateItem__c, Value__c FROM SurveyAnswer__c WHERE Survey__c = :survey.Id]) {
                answers.add(new SurveyModel.Answer(item.Id, item.SurveyTemplateItem__c, item.Value__c, '', getServiceName()));
            }

        return answers;
    }

    private List<Id> getItemSelectIds (List<SurveyTemplateItem__c> surveyTemplateItem) {
        List<Id> surveyTemplateItemSelectIds = new List<Id>();
        for (SurveyTemplateItem__c item: surveyTemplateItem){
            if (item.SurveyQuestion__r.RecordType.DeveloperName == SurveyUtils.QUESTION_TYPE_SELECT) {
                surveyTemplateItemSelectIds.add(item.SurveyQuestion__r.Id);
            }
        }

        return surveyTemplateItemSelectIds;
    }

    private Map<Id, List<SurveyModel.Option>> getQuestionItems(List<Id> selectItemIds) {

        Map<Id, List<SurveyQuestionItem__c>> options = new Map<Id, List<SurveyQuestionItem__c>>();
        for (SurveyQuestion__c item: [SELECT Id, (SELECT Name FROM SurveyQuestionItems__r) FROM SurveyQuestion__c WHERE Id IN : selectItemIds]) {
            options.put(item.Id, item.SurveyQuestionItems__r);
        }
        return prepareQuestionOption(options);
    }

    private Map<Id,  List<SurveyModel.Option>> prepareQuestionOption(Map<Id, List<SurveyQuestionItem__c>> questionItem){
        
        Map<Id, List<SurveyModel.Option>> mapOptions = new Map<Id, List<SurveyModel.Option>>();
        for (Id item: questionItem.keySet()) {
            List<SurveyModel.Option> options = new List<SurveyModel.Option>();
            for(SurveyQuestionItem__c qItem: questionItem.get(item)){
                options.add( new SurveyModel.Option(qItem.Id, qItem.Name));
            }
            mapOptions.put(item, options);
        }

        return mapOptions;
    }
*/
}