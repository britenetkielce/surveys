global with sharing class AchievementEmployeeProfileSurveyService extends AchievementSurveyService {

    global override Achievement_Set__c getAchievementSetWithItems(SurveyUtils.SurveyRequest surveyInfo, SurveyTemplate__c template) {
        String userId = surveyInfo.userId;

        if (String.isBlank(userId)) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Please provide User Id, this field is mandatory!');
        }

        List<Achievement_Set__c> achievementSets = AchievementService.getAchievementSets(null, AchievementUtils.getContactProfileRecordTypeIds(), true, 'Contact__r.Email = \'' + userId + '\'');

        if (achievementSets.isEmpty()) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Cannot find proper survey!');
        }
        if (achievementSets.size() > 1) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Found to many surveys!');
        }

        return achievementSets[0];
    }
}