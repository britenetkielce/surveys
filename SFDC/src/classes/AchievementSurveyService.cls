global abstract with sharing class AchievementSurveyService {

/*
    public static final String ROOT_ID = 'root';
    public static final String SERVICE_NAME = 'AchievementSurveyService';


    public override virtual SurveyModel.Survey assemblySurvey(SurveyUtils.SurveyRequest request, SurveyTemplate__c template, Survey__c survey) {
        Achievement_Set__c achievementSet = getAchievementSetWithItems(request, template);
        Survey surveyDetails = getSurveyDetails(request, template, survey, achievementSet);
        Map<ID, ID> questionIdToAchievementIdMap = getQuestionIdToAchievementIdMapFromSurveyTemplate(surveyDetails);
        prepareSurveyAnswersFromAchievementSetItems(questionIdToAchievementIdMap, achievementSet, surveyDetails);

        return surveyDetails;
    }

    public override void saveSurvey(String rawSurvey, SurveyModel.Survey baseSurveyInfo, List<SurveyModel.Answer> baseSurveyInfoAnswers, SurveyTemplate__c template, Survey__c survey) {
        Survey surveyInfo = (Survey)prepareSurveyDetails(rawSurvey, baseSurveyInfo);
        Achievement_Set__c achievementSet = getAchievementSetWithItems(new SurveyRequest(surveyInfo), template);

        updateSurvey(survey, achievementSet);

        Map<ID, ID> questionIdToAchievementIdMap = getQuestionIdToAchievementIdMapFromSurveyTemplate(surveyInfo);
        List<Achievement_Set_Item__c> achievementSetItems = prepareAchievementSetItemsFromSurveyAnswers(questionIdToAchievementIdMap, achievementSet, surveyInfo, baseSurveyInfoAnswers);
        List<Achievement> achievements = AchievementService.convert(achievementSet.Id, questionIdToAchievementIdMap.values(), achievementSetItems);
        AchievementService.save(achievements);
    }

    protected virtual Survey getSurveyDetails(SurveyUtils.SurveyRequest request, SurveyTemplate__c template, Survey__c survey, Achievement_Set__c achievementSet) {
        return new Survey(survey, achievementSet.Id);
    }

    protected void updateSurvey(Survey__c survey, Achievement_Set__c achievementSet) {
        if (survey.Id == null) {
            survey.TargetId__c = achievementSet.Id;
        }
        survey.Name = achievementSet.Name;
        upsert survey;
        survey = [SELECT Id, Name, UserId__c, DueDate__c, SurveyTemplate__c FROM Survey__c WHERE Id =:survey.Id LIMIT 1];
    }

    protected virtual Survey prepareSurveyDetails(String rawSurvey, SurveyModel.Survey baseSurveyInfo) {
        return (Survey) JSON.deserialize(rawSurvey, AchievementSurveyService.Survey.class);
    }

    global abstract Achievement_Set__c getAchievementSetWithItems(SurveyUtils.SurveyRequest surveyInfo, SurveyTemplate__c template);

    protected virtual Map<ID, ID> getQuestionIdToAchievementIdMapFromSurveyTemplate(Survey surveyInfo) {
        Map<ID, ID> questionIdToAchievementId = new Map<ID, ID>();
        List<SurveyTemplateItem__c> achievementItems = [
            SELECT Id, Achievement__c
            FROM SurveyTemplateItem__c 
            WHERE Achievement__c != NULL
                AND ServiceName__c = :getServiceName()
                AND SurveyTemplate__r.ApiName__c = :surveyInfo.templateApiName
            ORDER BY Order__c ASC];
        for (SurveyTemplateItem__c item : achievementItems) {
            questionIdToAchievementId.put(item.Id, item.Achievement__c);
        }

        return questionIdToAchievementId;
    }

    protected virtual List<Achievement_Set_Item__c> prepareAchievementSetItemsFromSurveyAnswers(Map<ID, ID> questionIdToAchievementIdMap, Achievement_Set__c achievementSet, Survey surveyInfo, List<SurveyModel.Answer> baseSurveyInfoAnswers) {
        List<Achievement_Set_Item__c> achievementSetItems = new List<Achievement_Set_Item__c>();
        Map<ID, Achievement_Set_Item__c> achievementIdToItemMap = mapAchievementIdToItems(achievementSet.Achievements__r);
        for (SurveyModel.Answer answer : baseSurveyInfoAnswers) {
            ID questionId = answer.questionId;
            ID achievementId = questionIdToAchievementIdMap.get(questionId);
            Achievement_Set_Item__c item;
            if (achievementIdToItemMap.containsKey(achievementId)) {
                item = achievementIdToItemMap.get(achievementId);
            } else {
                item = new Achievement_Set_Item__c(
                    Achievement__c = achievementId,
                    Achievement_Set__c = achievementSet.Id
                );
            }
            item.Level__c = answer.value;

            //TODO
            //item.Amount__c = ;
            //item.Min_Speciality_Match__c = ;

            achievementSetItems.add(item);
        }

        return achievementSetItems;
    }

    protected virtual void prepareSurveyAnswersFromAchievementSetItems(Map<ID, ID> questionIdToAchievementIdMap, Achievement_Set__c achievementSet, Survey surveyInfo) {
        List<SurveyModel.Answer> baseSurveyInfoAnswers = new List<SurveyModel.Answer>();
        Map<ID, Achievement_Set_Item__c> achievementIdToItemMap = mapAchievementIdToItems(achievementSet.Achievements__r);
        for (ID questionId : questionIdToAchievementIdMap.keySet()) {
            ID achievementId = questionIdToAchievementIdMap.get(questionId);
            if (achievementIdToItemMap.containsKey(achievementId)) {
                surveyInfo.addAnswer(getServiceName(), questionId, achievementIdToItemMap.get(achievementId));
            }
        }
    }

    protected virtual Map<ID, Achievement_Set_Item__c> mapAchievementIdToItems(List<Achievement_Set_Item__c> items) {
        Map<ID, Achievement_Set_Item__c> achievementIdToItemMap = new Map<ID, Achievement_Set_Item__c>();
        for (Achievement_Set_Item__c oldItem : items) {
            achievementIdToItemMap.put(oldItem.Achievement__c, oldItem);
        }
        return achievementIdToItemMap;
    }

    public with sharing class Template extends SurveyModel.Template {

        private String userId;
        private String achievementSetId;

        public Template(SurveyTemplate__c template, String userId, String achievementSetId) {
            super(template);
            this.userId = userId;
            this.achievementSetId = achievementSetId;
            questions = new List<SurveyModel.Question>();
        }
    }

    global with sharing class Survey extends SurveyModel.Survey {

        global Survey(){
        }

        global Survey(Survey__c survey, String achievementSetId) {
            super(survey);
            this.targetId = achievementSetId;
        }

        global void addAnswer(String serviceName, ID questionId, Achievement_Set_Item__c item) {
            answers.add(new SurveyModel.Answer(null, questionId, item.Level__c, '', serviceName));
        }
    }

    public class AchievementSurveyServiceException extends SurveyUtils.SurveyException {}
*/
}