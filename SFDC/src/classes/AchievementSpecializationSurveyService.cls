global with sharing class AchievementSpecializationSurveyService /*xtends AchievementSurveyService*/ {
/*
    public override Achievement_Set__c getAchievementSetWithItems(SurveyUtils.SurveyRequest surveyInfo, SurveyTemplate__c template) {

        if (template.AchievementSet__c == null) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Achievement Set was not set on the Survey Template!');
        }

        List<Achievement_Set__c> achievementSets = AchievementService.getAchievementSets(new Set<ID>{template.AchievementSet__c}, AchievementUtils.getSpecializationRecordTypeIds(), true);

        if (achievementSets.isEmpty()) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Cannot find proper survey!');
        }
        if (achievementSets.size() > 1) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Found to many surveys!');
        }
        
        return achievementSets[0];
    }
*/
}