@isTest
private class SurveyUtilsTest {

    @isTest
    static void shouldReturnValidSurveyServiceConfiguration() {
        // TO DO
    }

    @isTest
    static void shouldGetTemplateProvider() {
        //GIVEN
        String classProvider = 'AchievementSurveyUtils.AchievementQuestionProvider';
        String confName = 'SurveyDefaultConfiguration';

        SurveyTestUtils.SurveyConfigBuilder conf = new SurveyTestUtils.SurveyConfigBuilder();
        SurveyConfiguration__c providerconf = conf.setServiceConfig(confName, classProvider, null, SurveyUtils.getSurveyConfigRecordType(SurveyUtils.SURVEY_CONFIGURATION_SURVEY_QUESTION_PROVIDER_RECORD_TYPE).Id, true).buildAndSave();

        //WHEN
        Map<String, SurveyUtils.SurveyQuestionProvider> providers = SurveyUtils.getQuestionProviders();

        //THEN
        System.assertEquals(1, providers.size());
    }

    @isTest
    static void shouldGetSurveyHandler(){
        //GIVEN
        String classProvider = 'AchievementSurveyUtils.AchievementBaseHandler';
        String confName = 'HandlerDefaultConfiguration';

        SurveyTestUtils.SurveyConfigBuilder conf = new SurveyTestUtils.SurveyConfigBuilder();
        SurveyConfiguration__c providerconf = conf.setServiceConfig(confName, classProvider, null, SurveyUtils.getSurveyConfigRecordType(SurveyUtils.SURVEY_CONFIGURATION_SURVEY_HANDLER_RECORD_TYPE).Id, true).buildAndSave();

        //WHEN
        Map<String,SurveyUtils.SurveyHandler> handlers = SurveyUtils.getSurveyHandlers();

        //THEN
        System.assertEquals(1, handlers.size());
    }
/*
    @isTest
    static void shouldSetSimpleTypesInMap() {
        Map<String, Object> targetMap = new Map<String, Object>();
        Map<String, Object> srcMap = new Map<String, Object> {
            'a' => 1,
            'b' => 'text',
            'c' => 1.1
        };

        SurveyUtils.exdendMap(targetMap, srcMap);

        System.assertEquals(srcMap.get('a'), targetMap.get('a'));
        System.assertEquals(srcMap.get('b'), targetMap.get('b'));
        System.assertEquals(srcMap.get('c'), targetMap.get('c'));
    }

    @isTest
    static void shouldSetListValuesInMap() {
        Map<String, Object> targetMap = new Map<String, Object>();
        Map<String, Object> srcMap = new Map<String, Object> {
            'a' => new List<String>{'a1', 'a2', 'a3'}
        };

        SurveyUtils.exdendMap(targetMap, srcMap);

        System.assertEquals(srcMap.get('a'), targetMap.get('a'));
        System.assertEquals(srcMap.get('b'), targetMap.get('b'));
        System.assertEquals(srcMap.get('c'), targetMap.get('c'));
    }

    @isTest
    static void shouldExtendListValuesInMap() {
        List<String> srcList = new List<String>{'a1'};
        List<String> targetList = new List<String>{'b1', 'b2', 'b3'};

        Map<String, Object> targetMap = new Map<String, Object>{
            'a' => srcList
        };
        Map<String, Object> srcMap = new Map<String, Object> {
            'a' => targetList
        };

        SurveyUtils.exdendMap(targetMap, srcMap);

        System.assertEquals(4, srcList.size());
        System.assertEquals('a1', srcList[0]);
        System.assertEquals('b1', srcList[1]);
        System.assertEquals('b2', srcList[2]);
        System.assertEquals('b3', srcList[3]);
    }
*/
    @isTest
    static void shouldChangeApiNameToLowercase() {
        //GIVEN
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder('test Template').buildAndSave();

        //WHEN

        //THEN
        SurveyTemplate__c newTemplate = [SELECT ApiName__c FROM SurveyTemplate__c];
        System.assertEquals('test_template', newTemplate.ApiName__c);
    }

    @isTest
    static void shouldChangeApiNameToLowercaseWhenUpdateSurveyTemplate() {
        //GIVEN
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder('test Template').buildAndSave();
        template.Name = 'Test template 2';
        update template;

        //WHEN

        //THEN
        SurveyTemplate__c newTemplate = [SELECT ApiName__c FROM SurveyTemplate__c];
        System.assertEquals('test_template_2', newTemplate.ApiName__c);
    }

    @isTest
    static void shouldChangeApiNameToLowercaseAndTrim() {
        //GIVEN
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder('test Template ').buildAndSave();

        //WHEN

        //THEN
        SurveyTemplate__c newTemplate = [SELECT ApiName__c FROM SurveyTemplate__c];
        System.assertEquals('test_template', newTemplate.ApiName__c);
    }

    @isTest
    static void shouldChangeApiNameToLowercaseAndTrimAndReplaceSpecialChars() {
        //GIVEN
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder('test Template !@#$%^&*()-+=').buildAndSave();

        //WHEN

        //THEN
        SurveyTemplate__c newTemplate = [SELECT ApiName__c FROM SurveyTemplate__c];
        System.assertEquals('test_template', newTemplate.ApiName__c);
    }
/*
    static testMethod void shouldDisplayErrorDuplicateOrders() {

        //GIVEN
        SurveyTemplate__c surveyTemplate = new SurveyTemplate__c(Name = 'Salesforce survey 1', Description__c = 'Lorem ipsum', MultiSurvey__c = false, Importance__c = 'Medium', ApiName__c = 'SalesforceSurvey');
        insert surveyTemplate;

        List<SurveyQuestion__c> surveyQuestions = new List<SurveyQuestion__c>{
            new SurveyQuestion__c(Question__c = 'Test question 1'),
            new SurveyQuestion__c(Question__c = 'Test question 2')
        };
        insert surveyQuestions;

        SurveyTemplateItem__c surveyTemplateItem = new SurveyTemplateItem__c(SurveyTemplate__c = surveyTemplate.Id, SurveyQuestion__c = surveyQuestions[0].Id, Order__c = 1);
        insert surveyTemplateItem;

        //WHEN
        try{
            surveyTemplateItem = new SurveyTemplateItem__c(SurveyTemplate__c = surveyTemplate.Id, SurveyQuestion__c = surveyQuestions[1].Id, Order__c = 1);
            insert surveyTemplateItem;
        } catch(Exception e) {
            String message = e.getMessage();
        }

        //THEN
        List<SurveyTemplateItem__c> surveyTemplateItems = [SELECT Name, UniqueOrder__c FROM SurveyTemplateItem__c];

        System.assertEquals(1, surveyTemplateItems.size());
        System.assertEquals('Salesforce survey 1-1', surveyTemplateItems[0].UniqueOrder__c);
    }

    static testMethod void shouldDisplayErrorDuplicateQuestion() {

        //GIVEN
        SurveyTemplate__c surveyTemplate = new SurveyTemplate__c(Name = 'Salesforce survey 1', Description__c = 'Lorem ipsum', MultiSurvey__c = false, Importance__c = 'Medium', ApiName__c = 'SalesforceSurvey');
        insert surveyTemplate;

        SurveyQuestion__c surveyQuestion = new SurveyQuestion__c(Question__c = 'Test question 1');
        insert surveyQuestion;

        SurveyTemplateItem__c surveyTemplateItem = new SurveyTemplateItem__c(SurveyTemplate__c = surveyTemplate.Id, SurveyQuestion__c = surveyQuestion.Id, Order__c = 1);
        insert surveyTemplateItem;

        //WHEN
        try{
            surveyTemplateItem = new SurveyTemplateItem__c(SurveyTemplate__c = surveyTemplate.Id, SurveyQuestion__c = surveyQuestion.Id, Order__c = 2);
            insert surveyTemplateItem;
        } catch(Exception e) {
            String message = e.getMessage();
        }

        //THEN
        List<SurveyTemplateItem__c> surveyTemplateItems = [SELECT Name, UniqueOrder__c FROM SurveyTemplateItem__c];

        System.assertEquals(1, surveyTemplateItems.size());
        System.assertEquals('Salesforce survey 1-1', surveyTemplateItems[0].UniqueOrder__c);
    }

    static testMethod void shouldAddSurveyTemplateItemWithoutErrors() {

        //GIVEN
        SurveyTemplate__c surveyTemplate = new SurveyTemplate__c(Name = 'Salesforce survey 1', Description__c = 'Lorem ipsum', MultiSurvey__c = false, Importance__c = 'Medium', ApiName__c = 'SalesforceSurvey');
        insert surveyTemplate;

        List<SurveyQuestion__c> surveyQuestions = new List<SurveyQuestion__c>{
            new SurveyQuestion__c(Question__c = 'Test question 1'),
            new SurveyQuestion__c(Question__c = 'Test question 2')
        };
        insert surveyQuestions;

        //WHEN
       List<SurveyTemplateItem__c> surveyTemplateItems = new List<SurveyTemplateItem__c>{
            new SurveyTemplateItem__c(SurveyTemplate__c = surveyTemplate.Id, SurveyQuestion__c = surveyQuestions[0].Id, Order__c = 1),
            new SurveyTemplateItem__c(SurveyTemplate__c = surveyTemplate.Id, SurveyQuestion__c = surveyQuestions[1].Id, Order__c = 2)
        };
        insert surveyTemplateItems;

        //THEN
        List<SurveyTemplateItem__c> stItems = [SELECT Name, UniqueOrder__c FROM SurveyTemplateItem__c];

        System.assertEquals(2, stItems.size());
        System.assertEquals('Salesforce survey 1-1', stItems[0].UniqueOrder__c);
        System.assertEquals('Salesforce survey 1-2', stItems[1].UniqueOrder__c);
    }
*/
}