@isTest
private class SurveyControllerTest {
    
    @isTest
    private static void shouldRetrieveTemplatesInfo() {
        String userId = 'a.b.c.test@test.com';
        String configClassProvider = 'AchievementSurveyUtils.AchievementEmployeeProfileQuestionProvider';
        String configClassHandler = 'AchievementSurveyUtils.AchievementEmployeeProfileHandler';
        String templateConfig = '{}';
        List<Achievement__c> achievements = createAchievmentWithCategory('categoryName');
        String templateItemConfig = '{"achievmentId":"","getAll":"true"}';
        String apiName = prepateTemplate(configClassProvider, configClassHandler, templateConfig, templateItemConfig, false);
        Career_Path__c careerPath = new AchievementUtils.CareerPathBuilder('careerPath').buildAndSave();
        Contact c = new Contact(FirstName = 'abc', LastName = 'test', Email=userId);
        insert c;
        Double minSpecialityMatch = 0.30;
        Achievement_Set__c achievementSet  = new AchievementUtils.AchievementSetBuilder('EmployeeProfile').asContactProfile(c)
            .addSkill(achievements[0], 'Junior')
            .addCert(achievements[1])
            .addBadge(achievements[2], 10)
            .addSpecialization(achievements[2], minSpecialityMatch)
            .buildAndSave();
        SurveyUtils.Request request = new SurveyUtils.Request(userId);

        //WHEN
        List<SurveyModel.TemplateInfo> templates = SurveyController.getTemplatesInfo(request);

        //THEN
        System.assertEquals('surveyTemplateTest', templates[0].name);
        System.assertEquals(2, templates[0].surveys.size());
    }

    @isTest
    private static void shouldRetvireTemplate() {
        String templateApiName = 'template1';
        String userId = 'a.b.c.test@test.com';
        SurveyUtils.TemplateRequest request = new SurveyUtils.TemplateRequest(userId, templateApiName);
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder(templateApiName).buildAndSave();
        SurveyTemplateItem__c templateItem = SurveyTestUtils.createDefaultTemplateItem(template);

        SurveyModel.Template templates = SurveyController.getTemplate(request);

        System.assertEquals(templateApiName, templates.templateApiName);
        System.assertEquals(1, templates.questions.size());
    }

    @isTest
    private static void shouldRetriveSurvey() {
        String templateApiName = 'template1';
        String userId = 'a.b.c.test@test.com';
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder(templateApiName).buildAndSave();
        SurveyTemplateItem__c templateItem = SurveyTestUtils.createDefaultTemplateItem(template);
        List<Achievement__c> achievements = createAchievmentWithCategory('categoryName');
        Career_Path__c careerPath = new AchievementUtils.CareerPathBuilder('careerPath').buildAndSave();
        Achievement_Set__c achievementSet  = new AchievementUtils.AchievementSetBuilder('PositionProfile').asPrositionProfile(careerPath)
            .addSkill(achievements[0], 'Junior')
            .addCert(achievements[1])
            .addBadge(achievements[2], 10)
            .buildAndSave();
        SurveyUtils.SurveyRequest request = new SurveyUtils.SurveyRequest(userId, templateApiName, achievementSet.Id);

        SurveyModel.Survey templateSurvey = SurveyController.getSurvey(request);

        System.assertEquals(achievementSet.Id, templateSurvey.surveyId);
        System.assertEquals(3, templateSurvey.answers.size());
        System.assertEquals(templateApiName, templateSurvey.templateApiName);
        System.assertEquals(userId, templateSurvey.userId);
    }

    @isTest
    private static void shouldSaveSurvey() {
        String userId = 'a.b.c.test@test.com';
        String configClassProvider = 'AchievementSurveyUtils.AchievementEmployeeProfileQuestionProvider';
        String configClassHandler = 'AchievementSurveyUtils.AchievementEmployeeProfileHandler';
        String templateConfig = '{}';
        List<Achievement__c> achievements = createAchievmentWithCategory('categoryName');
        String templateItemConfig = '{"achievmentId":"","getAll":"true"}';
        String apiName = prepateTemplate(configClassProvider, configClassHandler, templateConfig, templateItemConfig, false);
        Career_Path__c careerPath = new AchievementUtils.CareerPathBuilder('careerPath').buildAndSave();
        Contact c = new Contact(FirstName = 'abc', LastName = 'test');
        insert c;
        Double minSpecialityMatch = 0.30;
        Achievement_Set__c achievementSet  = new AchievementUtils.AchievementSetBuilder('EmployeeProfile').asContactProfile(c)
            .addSkill(achievements[0], 'Junior')
            .addCert(achievements[1])
            .addBadge(achievements[2], 10)
            .addSpecialization(achievements[2], minSpecialityMatch)
            .buildAndSave();
        SurveyUtils.SurveyRequest request = new SurveyUtils.SurveyRequest(userId, apiName, achievementSet.Id);
        SurveyModel.Survey survey = SurveyFrontController.getInstance().getSurvey(request);
        survey.answers[0].value = 'Professional';
        survey.answers[2].value = '25';

        SurveyModel.Survey newSurvey = SurveyController.saveSurvey(JSON.serialize(survey));

        System.assertEquals('abc test - Employee Profile', newSurvey.name);
        System.assertEquals(userId, newSurvey.userId);
        System.assertEquals(3, newSurvey.answers.size());
        System.assertEquals('Professional', newSurvey.answers[0].value);
        System.assertEquals('25', newSurvey.answers[2].value);
    }



    static private List<Achievement__c> createAchievmentWithCategory(String name) {
        Achievement_Category__c category = new AchievementUtils.AchievementCategoryBuilder(name).buildAndSave();
        List<Achievement__c> achievements = new List<Achievement__c>{
            new AchievementUtils.AchievementBuilder('AchievementSkill', category).asSkill().build(),
            new AchievementUtils.AchievementBuilder('AchievementCertificate', category).asCert().build(),
            new AchievementUtils.AchievementBuilder('AchievementBadge', category).asBadge().build(),
            new AchievementUtils.AchievementBuilder('AchievementGroup', category).asGroup().build(),
            new AchievementUtils.AchievementBuilder('AchievementSpecialization', category).asSpecialization().build()
        };
        insert achievements;
        return achievements;
    }

    static private String  prepateTemplate (String configClassProvider, String configClassHandler, String templateConfig, String templateItemConfig, Boolean addItemEmail) {
        String providerName = 'questionProvider';
        String handlerName = 'surveyHandler';
        String configSignature = '{}';
        List<SurveyTestUtils.SurveyConfigBuilder> cfgs = createConfig(providerName, handlerName, configClassProvider, configClassHandler);
        cfgs[0].buildAndSave();
        cfgs[1].buildAndSave();
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder('surveyTemplateTest').withHandler(cfgs[1].getConfigs().get(handlerName).Id).withConfig(templateConfig).buildAndSave();
        SurveyQuestion__c question = new SurveyTestUtils.QuestionBuilder().buildAndSave();
        SurveyTemplateItem__c templateItem = new SurveyTestUtils.TemplateItemBuilder(template, null, cfgs[0].getConfigs().get(providerName)).withConfig(templateItemConfig).buildAndSave();
        if (addItemEmail) {
            String templateItemEmailConfig = '{"achievmentId":"","getAll":"false","email": "true" }';
            SurveyTemplateItem__c templateItem1 = new SurveyTestUtils.TemplateItemBuilder(template, null, cfgs[0].getConfigs().get(providerName)).withConfig(templateItemEmailConfig).withOrder(2).buildAndSave();
        }
        return template.ApiName__c;
    }

    static private List<SurveyTestUtils.SurveyConfigBuilder> createConfig(String providerName, String handlerName, String configClassProvider, String configClassHandler) {
        String configSignature = '{}';
        Id providerRecordTypeId = SurveyUtils.getSurveyConfigRecordType(SurveyUtils.SURVEY_CONFIGURATION_SURVEY_QUESTION_PROVIDER_RECORD_TYPE).Id;
        Id handlerRecordTypeId = SurveyUtils.getSurveyConfigRecordType(SurveyUtils.SURVEY_CONFIGURATION_SURVEY_HANDLER_RECORD_TYPE).Id;

        return new List <SurveyTestUtils.SurveyConfigBuilder> {
            new SurveyTestUtils.SurveyConfigBuilder().setServiceConfig(providerName, configClassProvider, configSignature, providerRecordTypeId, true),
            new SurveyTestUtils.SurveyConfigBuilder().setServiceConfig(handlerName, configClassHandler, configSignature, handlerRecordTypeId, true)
        };

    }
}