public with sharing class AchievementServiceUtils {

    public virtual with sharing class AchievementQuestionProvider extends SurveyUtils.SurveyQuestionProvider {

        final private String ACHIEVMENT_ID_CONF_KEY = 'achievmentId';

        public AchievementQuestionProvider() {
        }

        public virtual override List<SurveyModel.Question> getQuestions(QuestionConfig cfg) {
            List<SurveyModel.Question> questions = new List<SurveyModel.Question>();
            if (!cfg.hasValue(ACHIEVMENT_ID_CONF_KEY)) {
                throw new SurveyUtils.AchievementEmployeeQuestionProviderException('Template Item doesn\'t have configuration');
            }
            Achievement__c achievement = AchievementService.getAchievement(cfg.getId(ACHIEVMENT_ID_CONF_KEY));
            if (achievement != null) {
                questions.add(prepareQuestion(achievement, cfg.getInteger()));
            }

           return questions;
        }

        protected SurveyModel.Question prepareQuestion(Achievement__c achievement, Integer order) {
            SurveyModel.Question question = new SurveyModel.Question(achievement.Id, achievement.Name);
            question.order = order;
            question.description = achievement.Details__c;
            if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SKILL_RECORD_TYPE) {
                if (!String.isBlank(achievement.Level_1_Details__c) || !String.isBlank(achievement.Level_2_Details__c) || !String.isBlank(achievement.Level_3_Details__c)) {
                    if(!String.isBlank(question.description)){
                        question.description += '<br />';
                    }
                    question.description += achievement.Level_1_Details__c + '<br />' + achievement.Level_2_Details__c + '<br />' + achievement.Level_3_Details__c;
                }
                question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                question.options = questionLevelOptions();
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_CERTIFICATE_RECORD_TYPE) {
                question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                question.options = new List<SurveyModel.Option> {
                    new SurveyModel.Option(null, Label.None),
                    new SurveyModel.Option('1', Label.Yes)
                };
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE) {
                if (achievement.Group_Type__c == AchievementUtils.ACHIEVEMENT_MANUAL_GROUP_TYPE) {
                    question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                    question.options = questionLevelOptions();
                } else {
                    question.readOnly = true;
                }
            } else {
                question.type = SurveyUtils.QUESTION_TYPE_TEXT;
            }

            return question;
        }

        private List<SurveyModel.Option> questionLevelOptions() {
            return new List<SurveyModel.Option> {
                new SurveyModel.Option(null, Label.None),
                new SurveyModel.Option(AchievementUtils.ACHIEVEMENT_LEVEL_1, Label.SkillLevelJunior),
                new SurveyModel.Option(AchievementUtils.ACHIEVEMENT_LEVEL_2, Label.SkillLevelProfessional),
                new SurveyModel.Option(AchievementUtils.ACHIEVEMENT_LEVEL_3, Label.SkillLevelSenior)
            };
        }
    }
}