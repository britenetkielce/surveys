@isTest
public class SurveyTestUtils {

    public static final String DEFAULT_SURVEY_HANDLER_CONFIGURATION_CLASS = 'AchievementSurveyUtils.AchievementPositionProfileHandler';
    public static final String DEFAULT_QUESTION_PROVIDER_CLASS = 'AchievementSurveyUtils.AchievementQuestionProvider';
    public static final String DEFAULT_SURVEY_CONFIGURATION_SIGNATURE = '{"id": ""}';

    public static List<SurveyModel.TemplateInfo> templatesInfoRequest(String userId) {
        return SurveyFrontController.getInstance().getTemplatesInfo(new SurveyUtils.Request(userID));
    }

    public static SurveyModel.Template templateRequest(String userId, String templateApiName) {
        return SurveyFrontController.getInstance().getTemplate(new SurveyUtils.TemplateRequest(userID, templateApiName));
    }

    public static SurveyModel.Survey surveyRequest(String userId, String templateApiName, String surveyId) {
        return SurveyFrontController.getInstance().getSurvey(new SurveyUtils.SurveyRequest(userId, templateApiName, surveyId));
    }

    public static List<SurveyQuestion__c> createDefaultQuestions() {
        List<SurveyQuestion__c> questions = new List<SurveyQuestion__c>{
            new SurveyTestUtils.QuestionBuilder().build(),
            new SurveyTestUtils.QuestionBuilder().asSelectType().addItem('Value 1').addItem('Value 2').build()
        };
        insert questions;
        return questions;
    }

    public static SurveyTemplateItem__c createDefaultTemplateItem(SurveyTemplate__c template) {
        return buildDefaultTemplateItem(template).buildAndSave();
    }

    public static TemplateItemBuilder buildDefaultTemplateItem(SurveyTemplate__c template) {
        SurveyTestUtils.SurveyConfigBuilder cfg = SurveyTestUtils.createDefaultQuestionProvider('questionProvider');
        SurveyQuestion__c question = new SurveyTestUtils.QuestionBuilder().buildAndSave();
        Achievement_Category__c category = new AchievementUtils.AchievementCategoryBuilder('name').buildAndSave();
        Achievement__c achievement = new AchievementUtils.AchievementBuilder('Apex', category).asSkill().buildAndSave();

        return new SurveyTestUtils.TemplateItemBuilder(template, question, cfg.getConfigs().get('questionProvider')).withConfig('{"id": "'+achievement.Id+'", "getAll":"true"}');
    }

    public static SurveyTemplate__c createDefaultTemplate(String templateName) {
        return buildDefaultTemplate(templateName).buildAndSave();
    }

    public static TemplateBuilder buildDefaultTemplate(String templateName) {
        return new SurveyTestUtils.TemplateBuilder(templateName).addItems(createDefaultQuestions());
    }

    public static SurveyConfigBuilder createDefaultSurveyHandler(String name) {
        SurveyConfigBuilder cfg = new SurveyConfigBuilder().setServiceConfig(name, DEFAULT_SURVEY_HANDLER_CONFIGURATION_CLASS, DEFAULT_SURVEY_CONFIGURATION_SIGNATURE, null, true).setSurveyHandlerRecordType(name);
        cfg.buildAndSave();
        return cfg;
    }

    public static SurveyConfigBuilder createDefaultQuestionProvider(String name) {
        SurveyConfigBuilder cfg = new SurveyConfigBuilder().setServiceConfig(name, DEFAULT_QUESTION_PROVIDER_CLASS, DEFAULT_SURVEY_CONFIGURATION_SIGNATURE, null, true).setQuestionProviderRecordType(name);
        cfg.buildAndSave();
        return cfg;
    }

    public virtual class SurveyConfigBuilder {
        private Map<String, SurveyConfiguration__c> config;

        public SurveyConfigBuilder() {
            config = new Map<String, SurveyConfiguration__c>();
        }

        public SurveyConfigBuilder setServiceConfig(String configName, String configClass, String configSignature, ID recordType, Boolean active) {
            SurveyConfiguration__c cfg = config.containsKey(configName) ? config.get(configName) : new SurveyConfiguration__c(Name=configName);

            if (configClass != null){
                cfg.Class__c = configClass;
            }
            if (active != null){
                cfg.Active__c = active;
            }
            if (configSignature != null){
                cfg.ConfigSignature__c = configSignature;
            }
            if (recordType != null){
                cfg.RecordTypeId = recordType;
            }
            config.put(cfg.Name, cfg);
            return this;
        }

        public SurveyConfiguration__c build(){
            return config.values();
        }

        public SurveyConfiguration__c buildAndSave(){
            upsert config.values();
            return config.values();
        }

        public SurveyConfigBuilder setConfigSignature(String configName, String configSignature) {
            return setServiceConfig(configName, null, configSignature, null, true);
        }

        public SurveyConfigBuilder setConfigClass(String configName, String configClass) {
            return setServiceConfig(configName, configClass, null, null, true);
        }

        public SurveyConfigBuilder setQuestionProviderRecordType(String configName) {
            return setServiceConfig(configName, null, null, SurveyUtils.getSurveyConfigRecordType(SurveyUtils.SURVEY_CONFIGURATION_SURVEY_QUESTION_PROVIDER_RECORD_TYPE).Id, true);
        }

        public SurveyConfigBuilder setSurveyHandlerRecordType(String configName) {
            return setServiceConfig(configName, null, null, SurveyUtils.getSurveyConfigRecordType(SurveyUtils.SURVEY_CONFIGURATION_SURVEY_HANDLER_RECORD_TYPE).Id, true);
        }

        public Map<String, SurveyConfiguration__c> getConfigs(){
            return config;
        }
    }

    public class TemplateAccessBuilder {
        private SurveyTemplateAccesses__c record;

        public TemplateAccessBuilder(SurveyTemplate__c tamplate) {
            record = new SurveyTemplateAccesses__c(
                OnlyLeaders__c = true,
                SurveyTemplate__c = tamplate.Id
            );
        }

        public TemplateAccessBuilder withContact(Contact contact) {
            record.OnlyLeaders__c = false;
            record.Contact__c = contact.Id;
            return this;
        }

        public SurveyTemplateAccesses__c build() {
            return record;
        }

        public SurveyTemplateAccesses__c buildAndSave() {
            upsert record;
            return record;
        }
    }

    public class TemplateBuilder {

        protected SurveyTemplate__c record;
        protected List<SurveyTemplateItem__c> items = new List<SurveyTemplateItem__c>();

        public TemplateBuilder(String name) {
            record = new SurveyTemplate__c(
                Name = name,
                ApiName__c = name,
                Active__c = true,
                Description__c = 'Lorem ipsum',
                StartDate__c = System.today() - 7,
                EndDate__c = System.today() + 7,
                Importance__c = 'Medium',
                MultiSurvey__c = false,
                Private__c = false,
                Submittable__c = false,
                Config__c = '{}',
                SurveyHandler__c = createDefaultSurveyHandler('defaultName').config.get('defaultName').Id
            );
        }

        public SurveyTemplate__c buildAndSave() {
            upsert record;

            if (!items.isEmpty()) {
                for (SurveyTemplateItem__c item : items){
                    if (item.SurveyTemplate__c == null) {
                        item.SurveyTemplate__c = record.Id;
                        item.Config__c = '';
                        item.QuestionProvider__c = createDefaultQuestionProvider('defaultQuestionProvider').config.get('defaultQuestionProvider').Id;
                    }
                }
                upsert items;
            }

            return record;
        }

        public SurveyTemplate__c build() {
            return record;
        }

        public List<SurveyTemplateItem__c> getItems() {
            return items;
        }

        public TemplateBuilder withHandler(Id recordTypeId) {
            record.SurveyHandler__c = recordTypeId;
            return this;
        }

        public TemplateBuilder withNameAndApiName(String name) {
            record.Name = name;
            record.ApiName__c = name;
            return this;
        }

        public TemplateBuilder withConfig(String config) {
            record.Config__c = config;
            return this;
        }

        public TemplateBuilder deactivate() {
            record.Active__c = false;
            return this;
        }

        public TemplateBuilder fromFuture() {
            record.StartDate__c = Date.today() + 1;
            record.EndDate__c = Date.today() + 2;
            return this;
        }

        public TemplateBuilder fromPast() {
            record.StartDate__c = Date.today() - 2;
            record.EndDate__c = Date.today() - 1;
            return this;
        }

        public TemplateBuilder withActualDate() {
            record.StartDate__c = Date.today() - 1;
            record.EndDate__c = Date.today() + 1;
            return this;
        }

        public TemplateBuilder withStartDate(Date startDate) {
            record.StartDate__c = startDate;
            return this;
        }

        public TemplateBuilder withEndDate(Date endDate) {
            record.EndDate__c = endDate;
            return this;
        }

        public TemplateBuilder asMultisurvey() {
            record.MultiSurvey__c = true;
            return this;
        }

        public TemplateBuilder privateSurvey() {
            record.Private__c = true;
            return this;
        }

        public TemplateBuilder submittable() {
            record.Submittable__c = true;
            return this;
        }

        public TemplateBuilder addItem(SurveyTemplateItem__c item) {
            item.Order__c = items.size();
            items.add(item);
            return this;
        }

        public TemplateBuilder addItem(SurveyQuestion__c item) {
            items.add(new TemplateItemBuilder(item).withOrder(items.size()).build());
            return this;
        }

        public TemplateBuilder addItems(SurveyQuestion__c[] items) {
            for (SurveyQuestion__c item : items) {
                addItem(item);
            }
            return this;
        }
    }

    public virtual class TemplateItemBuilder {

        protected SurveyTemplateItem__c record;

        public TemplateItemBuilder() {}

        public TemplateItemBuilder(SurveyQuestion__c surveyQuestion) {
            this(null, surveyQuestion, null);
        }

        public TemplateItemBuilder(SurveyTemplate__c surveyTemplate, SurveyQuestion__c surveyQuestion, SurveyConfiguration__c questionProvider) {
            record = new SurveyTemplateItem__c(
                SurveyTemplate__c = surveyTemplate == null ? null : surveyTemplate.Id,
                QuestionProvider__c = questionProvider == null ? null : questionProvider.Id,
                Config__c = '',
                Order__c = 1
            );
        }

        public SurveyTemplateItem__c buildAndSave() {
            upsert record;
            return record;
        }

        public SurveyTemplateItem__c build() {
            return record;
        }

        public TemplateItemBuilder withOrder(Integer order) {
            record.Order__c = order;
            return this;
        }

        public TemplateItemBuilder withConfig(String config){
            record.Config__c = config;
            return this;
        }

        public TemplateItemBuilder withQuestionProvider(SurveyConfiguration__c provider){
            record.QuestionProvider__c = provider.Id;
            return this;
        }
    }

    public class QuestionBuilder {

        private SurveyQuestion__c record;
        private List<SurveyQuestionItem__c> questionItems = new List<SurveyQuestionItem__c>();

        public QuestionBuilder(){
            record = new SurveyQuestion__c(
                Question__c = 'Test question ' + SurveyUtils.QUESTION_TYPE_TEXT,
                RecordTypeId = SurveyUtils.getSurveyQuestionRecordType(SurveyUtils.QUESTION_TYPE_TEXT).Id
            );
        }

        public SurveyQuestion__c buildAndSave() {
            upsert record;
            if (!questionItems.isEmpty()) {
                for (SurveyQuestionItem__c item : questionItems) {
                    if (item.SurveyQuestion__c == null) {
                        item.SurveyQuestion__c = record.Id;
                    }
                }
                upsert questionItems;
            }

            return record;
        }

        public QuestionBuilder addItem(String questionValue) {
            questionItems.add(new SurveyQuestionItem__c(Name = questionValue));
            return this;
        }

        public SurveyQuestion__c build() {
            return record;
        }

        public QuestionBuilder asSelectType() {
            record.Question__c = 'Test question '+SurveyUtils.QUESTION_TYPE_SELECT;
            record.RecordTypeId = SurveyUtils.getSurveyQuestionRecordType(SurveyUtils.QUESTION_TYPE_SELECT).Id;
            return this;
        }

        public QuestionBuilder asInfoType() {
            record.RecordTypeId = SurveyUtils.getSurveyQuestionRecordType(SurveyUtils.QUESTION_TYPE_INFO).Id;
            return this;
        }
    }

    public class SuveryBuilder {

        private Survey__c record;
        private List<SurveyAnswer__c> answers = new List<SurveyAnswer__c>();

        public SuveryBuilder(TemplateBuilder templateBuilder) {
            this(templateBuilder.build(), templateBuilder.getItems());
        }

        public SuveryBuilder(SurveyTemplate__c template, List<SurveyTemplateItem__c> items) {
            record = new Survey__c(
                SurveyTemplate__c = template.Id,
                Status__c = SurveyUtils.QUESTION_STATUS_OPEN,
                DueDate__c = System.today() + 7,
                SurveyId__c = 'SuRvEyCaSeSeInSeNsiTiVeId'
            );

            Integer i = 1;
            for (SurveyTemplateItem__c item : items) {
                answers.add(new SurveyAnswer__c(Value__c = 'Answer '+i, SurveyTemplateItem__c = item.Id));
                i ++;
            }
        }

        public SuveryBuilder(SurveyTemplate__c template) {
            this(template, new List<SurveyTemplateItem__c>());
        }

        public Survey__c buildAndSave() {
            upsert record;

            if (!answers.isEmpty()) {
                for (SurveyAnswer__c answer: answers) {
                    if (answer.Survey__c == null) {
                        answer.Survey__c = record.Id;
                    }
                }

                upsert answers;
            }

            return record;
        }

        public Survey__c build() {
            return record;
        }

        public SuveryBuilder inProgress() {
            record.Status__c = SurveyUtils.QUESTION_STATUS_IN_PROGRESS;
            return this;
        }

        public SuveryBuilder inSubmited() {
            record.Status__c = SurveyUtils.QUESTION_STATUS_SUBMITED;
            return this;
        }

        public SuveryBuilder withDueDate(Date dueDate) {
            record.DueDate__c = dueDate;
            return this;
        }

        public SuveryBuilder withUserId(String userId) {
            record.UserId__c = userId;
            return this;
        }

        public SuveryBuilder addAnswer(String answer, SurveyTemplateItem__c tamplateItem){
            answers.add(new SurveyAnswer__c(Value__c = answer, SurveyTemplateItem__c = tamplateItem.Id));
            return this;
        }
    }
}