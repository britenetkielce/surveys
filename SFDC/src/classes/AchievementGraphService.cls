public with sharing class AchievementGraphService {

    public AchievementGraphService(){
    }
    
    /**
     * Gets the sfdc host url.
     */
    public String getSFDCHost() {
        return URL.getSalesforceBaseUrl().getHost();
    }

    /**
     * Returns timeout for JavaScript remoting reqeust in milliseconds.
     *
     * @return timeout in milliseconds
     */
    public Integer getRemotingTimeout() {
        return 60000;
    }

    @RemoteAction
    public static List<NodeDetails> getNodeDetails() {
        List<NodeDetails> details = new List<NodeDetails>();
        return details;
    }
    
    @RemoteAction
    public static List<Node> getNodes() {
        List<Node> nodes = new List<Node>();
        return nodes;
    }

    @RemoteAction
    public static void saveNodeStatus(String id, NodeState state) {
    }

    public class Node {
        public String id;
        public String nodeDetailsId;
        public Map<String, NodeState> state;
        public Set<String> parentNodeIds;
    }
    
    public class NodeDetails {
        public String id;
        public String name;
        public String type;

        public String description;

        public String level1Description;
        public String level2Description;
        public String level3Description;
    }
    
    public class NodeState {
        public String level;
        public Integer amount;
        public Double value;
    }
}