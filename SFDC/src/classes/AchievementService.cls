/**
 * Achievement Service contains all logic related with Achievements process.
 * @author Adam Siwek
 */
public with sharing class AchievementService {

    public static List<Achievement_Set_Item__c> getAchievementSetItems(Id achievementSetId) {
        return achievementSetId != null ?
                [SELECT Level__c, Achievement__c, Amount__c, Min_Speciality_Match__c FROM Achievement_Set_Item__c WHERE Achievement_Set__c = :achievementSetId] :
                new List<Achievement_Set_Item__c>();
    }

    public static List<Achievement__c> getAchievementGroups(Set<ID> groupIds) {
        return groupIds != null ?
                [SELECT Id, Group_Type__c, (SELECT Id, Achievement__c FROM Achievement_Group_Items__r) FROM Achievement__c WHERE Id = :groupIds] :
                [SELECT Id, Group_Type__c, (SELECT Id, Achievement__c FROM Achievement_Group_Items__r) FROM Achievement__c];
    }

    public static List<Career_Path__c> careerPaths = null;

    public static List<Career_Path__c> getCareerPaths() {
        if (careerPaths == null) {
            careerPaths = [SELECT Name, Description__c FROM Career_Path__c ORDER BY Name];
        }
        return careerPaths;
    }

    public static Map<Id, Achievement__c> allAchievements = null;

    public static Achievement__c getAchievement(ID achievementId) {
        getAllAchievements();
        return allAchievements.get(achievementId);
    }

    public static List<Achievement__c> getAllAchievements() {
        if (allAchievements == null) {
            allAchievements = new Map<ID, Achievement__c>(
                [SELECT Name, Details__c, Level_1_Details__c, Level_2_Details__c, Level_3_Details__c,
                           Active__c, Group_Type__c, My_Achievement__c, 
                           RecordType.Name, RecordType.DeveloperName, Speciality__c,
                           Achievement_Category__r.Name,
                           (SELECT Achievement__c FROM Achievement_Group_Items__r WHERE Achievement__c != null)
                    FROM Achievement__c
                    ORDER BY Achievement_Category__r.Name, RecordType.DeveloperName, Name]);
        }
        return allAchievements.values();
    }

    public static List<Achievement_Category__c> getAllAchievementCategories() {
        return [SELECT Name, Number_of_Achievements__c FROM Achievement_Category__c ORDER BY Name];
    }

    public static List<AchievementUtils.AchievementGroup> getAchievementGroups(Id achievementSetId, Id achievementSetIdToCompare) {
        List<AchievementUtils.AchievementGroup> groups = new List<AchievementUtils.AchievementGroup>();
        Map<Id, Achievement_Set_Item__c> selectedAchivements = new Map<Id, Achievement_Set_Item__c>();
        Map<Id, Achievement_Set_Item__c> achievemetToCmp = new Map<Id, Achievement_Set_Item__c>();

        for (Achievement_Set_Item__c record : getAchievementSetItems(achievementSetId)) {
            selectedAchivements.put(record.Achievement__c, record);
        }

        if (achievementSetIdToCompare != null) {
            for (Achievement_Set_Item__c record : getAchievementSetItems(achievementSetIdToCompare)) {
                achievemetToCmp.put(record.Achievement__c, record);
            }
        }

        Set<String> uniqueGroupNames = new Set<String>();
        AchievementUtils.AchievementGroup actualGroup;

        for (Achievement__c a : getAllAchievements()) {

            if (!uniqueGroupNames.contains(a.Achievement_Category__c)) {
                actualGroup = new AchievementUtils.AchievementGroup(a.Achievement_Category__r.Name);
                uniqueGroupNames.add(a.Achievement_Category__c);
                groups.add(actualGroup);
            }
            Achievement_Set_Item__c oldRecord = selectedAchivements.get(a.Id);
            if (oldRecord == null) {
                oldRecord = new Achievement_Set_Item__c(Achievement_Set__c = achievementSetId, Achievement__c = a.Id);
            }
            Achievement ah = new Achievement(a, oldRecord);
            if (achievemetToCmp.containsKey(a.Id)) {
                ah.setDataToCmp(achievemetToCmp.get(a.Id));
            }
            actualGroup.achievements.add(ah);
        }

        return groups;
    }

    public static List<Achievement> convert(ID achievementSetId, List<ID> achievementIds, List<Achievement_Set_Item__c> oldItems) {
        List<Achievement> achievements = new List<Achievement>();
        Map<Id, Achievement_Set_Item__c> selectedAchivements = new Map<Id, Achievement_Set_Item__c>();

        for (Achievement_Set_Item__c record : oldItems) {
            selectedAchivements.put(record.Achievement__c, record);
        }

        for (ID achievementId : achievementIds) {
            Achievement_Set_Item__c oldRecord = selectedAchivements.get(achievementId);
            if (oldRecord == null) {
                oldRecord = new Achievement_Set_Item__c(Achievement_Set__c = achievementSetId, Achievement__c = achievementId);
            }
            achievements.add(new Achievement(getAchievement(achievementId), oldRecord));
        }
        return achievements;
    }

    public static void applyAchievementsToSet(Id achievementSetId, List<AchievementUtils.AchievementGroup> groups) {
        for (AchievementUtils.AchievementGroup grp : groups) {
            for (Achievement a : grp.achievements) {
                a.oldRecord.Achievement_Set__c = achievementSetId;
            }
        }
    }

    public static void save(List<AchievementUtils.AchievementGroup> groups) {
        List<Achievement_Set_Item__c> recordsToInsert = new List<Achievement_Set_Item__c>();
        List<Achievement_Set_Item__c> recordsToUpdate = new List<Achievement_Set_Item__c>();
        List<Achievement_Set_Item__c> recordsToDelete = new List<Achievement_Set_Item__c>();

        for (AchievementUtils.AchievementGroup g : groups) {
            prepareAchievementToSave(g.achievements, recordsToInsert, recordsToUpdate, recordsToDelete);
        }


        if (!recordsToInsert.isEmpty()) {
            insert recordsToInsert;
        }
        if (!recordsToUpdate.isEmpty()) {
            update recordsToUpdate;
        }
        if (!recordsToDelete.isEmpty()) {
            delete recordsToDelete;
        }
    }

    public static void save(List<Achievement> achievements) {
        List<Achievement_Set_Item__c> recordsToInsert = new List<Achievement_Set_Item__c>();
        List<Achievement_Set_Item__c> recordsToUpdate = new List<Achievement_Set_Item__c>();
        List<Achievement_Set_Item__c> recordsToDelete = new List<Achievement_Set_Item__c>();

        prepareAchievementToSave(achievements, recordsToInsert, recordsToUpdate, recordsToDelete);
        if (!recordsToInsert.isEmpty()) {
            insert recordsToInsert;
        }
        if (!recordsToUpdate.isEmpty()) {
            update recordsToUpdate;
        }
        if (!recordsToDelete.isEmpty()) {
            delete recordsToDelete;
        }
    }

    private static void prepareAchievementToSave(List<Achievement> achievements, List<Achievement_Set_Item__c> recordsToInsert, List<Achievement_Set_Item__c> recordsToUpdate,List<Achievement_Set_Item__c> recordsToDelete) {
        for (Achievement a : achievements) {
            if (a.getIsCert()) {
                if (!a.selected && !a.getIsNew()) {
                    recordsToDelete.add(a.oldRecord);
                } else if (a.selected && a.getIsNew()) {
                    recordsToInsert.add(a.oldRecord);
                }
            } else if (a.getIsBadge()) {
                if (!a.getIsNew() && (a.oldRecord.Amount__c == 0 || a.oldRecord.Amount__c == null)) {
                    recordsToDelete.add(a.oldRecord);
                } else if (a.oldRecord.Amount__c != 0 && a.oldRecord.Amount__c != null) {
                    if (!a.getIsNew()) {
                        recordsToUpdate.add(a.oldRecord);
                    } else {
                        recordsToInsert.add(a.oldRecord);
                    }
                }
            } else if (a.getIsSkill() || a.getIsGroup()) {
                if (!a.getIsNew() && (a.oldRecord.Level__c == '' || a.oldRecord.Level__c == null)) {
                    recordsToDelete.add(a.oldRecord);
                } else if (a.oldRecord.Level__c != '' && a.oldRecord.Level__c != null) {
                    if (!a.getIsNew()) {
                        recordsToUpdate.add(a.oldRecord);
                    } else {
                        recordsToInsert.add(a.oldRecord);
                    }
                }
            } else if (a.getIsSpecialization()) {
                if (!a.getIsNew() && (a.oldRecord.Min_Speciality_Match__c == 0 || a.oldRecord.Min_Speciality_Match__c == null)) {
                    recordsToDelete.add(a.oldRecord);
                } else if (a.oldRecord.Min_Speciality_Match__c != 0 && a.oldRecord.Min_Speciality_Match__c != null) {
                    if (!a.getIsNew()) {
                        recordsToUpdate.add(a.oldRecord);
                    } else {
                        recordsToInsert.add(a.oldRecord);
                    }
                }
            } 
        }
    }

    public static Map<Id, Achievement_Set__c> achievementSetsInfo = null;

    public static Map<Id, Achievement_Set__c> getAllAchievementSetsInfo() {
        if (achievementSetsInfo == null) {
            achievementSetsInfo = new Map<Id, Achievement_Set__c>();
            for (Achievement_Set__c item : [SELECT Id, Name FROM Achievement_Set__c]) {
                achievementSetsInfo.put(item.Id, item);
            }
        }
        return achievementSetsInfo;
    }

    public static Achievement_Set__c getAchievementSetInfo(Id key) {
        getAllAchievementSetsInfo();
        return achievementSetsInfo.get(key);
    }


    public static List<Achievement_Set__c> getAchievementSets(final Set<ID> ids, final Set<ID> recordTypeIds, Boolean withItems) {
        return getAchievementSets(ids, recordTypeIds, withItems, null);
    }

    public static List<Achievement_Set__c> getAchievementSets(final Set<ID> ids, final Set<ID> recordTypeIds, Boolean withItems, String customCondition) {
        return Database.query(getAchievementSetsQuery(ids, recordTypeIds, withItems, customCondition));
    }

    public static String getAchievementSetsQuery(final Set<ID> ids, final Set<ID> recordTypeIds, Boolean withItems) {
        return getAchievementSetsQuery(ids, recordTypeIds, withItems, null);
    }

    public static String getAchievementSetsQuery(final Set<ID> ids, final Set<ID> recordTypeIds, Boolean withItems, String customCondition) {
        String query = 'SELECT Id, Name, Contact__c, Contact__r.Email, Career_Path__c, Score_Minimal_Value__c, AboutWho__c, AboutWho__r.Email, Who__c, RecordTypeId, SelfSurvey__c, Parent_Profile__c, RecordType.DeveloperName, Description__c';
        if (withItems) {
            query += ', (SELECT Achievement__c, Amount__c, Level__c, Weight__c, Min_Speciality_Match__c, Type__c, Achievement__r.RecordType.DeveloperName FROM Achievements__r)';
        }
        query += ' FROM Achievement_Set__c WHERE';

        if ((recordTypeIds != null && recordTypeIds.isEmpty()) || (ids != null && ids.isEmpty())) {
            if (customCondition == null) {
                query += ' Id = null';
            }
        } else {
            query += ' Id != null';
            if (recordTypeIds != null && !recordTypeIds.isEmpty()) {
                query += ' AND RecordTypeId IN (\'' + String.join(new List<ID>(recordTypeIds), '\',\'') + '\')';
            }
            if (ids != null && !ids.isEmpty()) {
                query += ' AND Id IN (\'' + String.join(new List<ID>(ids), '\',\'') + '\')';
            }
        }
        if (customCondition != null) {
            query += ' AND ' + customCondition;
        }
        return query;
    }

    public static void calculateContactProfileScores(Set<ID> contactProfileIds, Set<ID> positionProfileIds) {
        if ((contactProfileIds != null && positionProfileIds != null &&
             contactProfileIds.size() <= AchievementUtils.EMPLOYEE_LIMIT &&
             positionProfileIds.size() <= AchievementUtils.POSITION_LIMIT) &&
            [SELECT Count() FROM AsyncApexJob WHERE JobType='BatchApex' AND ApexClass.Name = 'AchievementProfilePartialCalcuationJob' AND Status IN ('Queued', 'Holding') LIMIT 5] < 5) {
                
            Database.executeBatch(new AchievementProfilePartialCalculationJob(contactProfileIds));

        } else if ([SELECT Count() FROM AsyncApexJob WHERE JobType='BatchApex' AND ApexClass.Name = 'AchievementProfileCalcuationJob' AND Status IN ('Queued', 'Holding') LIMIT 1] == 0) {

            Database.executeBatch(new AchievementProfileCalcuationJob());
        }
    }

    public static void calculateContactProfileScoresWithoutBatch(List<Achievement_Set__c> contactProfiles, List<Achievement_Set__c> positionProfiles) {
        List<Contact_Profile_Score__c> profileScores = new List<Contact_Profile_Score__c>();
        Set<String> profileScoresToDelete = new Set<String>();
        for (Achievement_Set__c c : contactProfiles) {
            for (Achievement_Set__c p : positionProfiles) {
                Contact_Profile_Score__c cp = calculateContactProfileScore(c, p);
                if (cp.Match__c >= p.Score_Minimal_Value__c) {
                    profileScores.add(cp);
                } else {
                    profileScoresToDelete.add(cp.unique_name__c);
                }
            }
        }

        if (!profileScores.isEmpty()) {
            upsert profileScores unique_name__c;
        }
        if (!profileScoresToDelete.isEmpty()) {
            delete [SELECT Id FROM Contact_Profile_Score__c WHERE unique_name__c IN : profileScoresToDelete LIMIT :profileScoresToDelete.size()];
        }
    }

    public static Contact_Profile_Score__c calculateContactProfileScore(Achievement_Set__c c, Achievement_Set__c p) {
        Map<Id, Achievement_Set_Item__c> caMap = new Map<Id, Achievement_Set_Item__c>();
        for (Achievement_Set_Item__c ca : c.Achievements__r) {
            caMap.put(ca.Achievement__c, ca);
        }

        Double counter = 0;
        Double sum = 0;
        Double weight = 1;

        for (Achievement_Set_Item__c ppa : p.Achievements__r) {
            weight = ppa.Weight__c == null ? 1.0 : ppa.Weight__c;
            sum += weight;

            if (caMap.containsKey(ppa.Achievement__c)) {
                Achievement_Set_Item__c ca = caMap.get(ppa.Achievement__c);
                if (ppa.Achievement__r.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_CERTIFICATE_RECORD_TYPE) {
                    counter += weight;
                } else if (ppa.Achievement__r.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_BADGE_RECORD_TYPE && ca.Amount__c != null && ppa.Amount__c != null) {
                    Double cAmount = ca.Amount__c;
                    Double pAmount = ppa.Amount__c;
                    if (cAmount >= pAmount) {
                        counter += weight;
                    } else {
                        counter += ((pAmount - cAmount) / pAmount) * weight;
                    }
                } else if ((ppa.Achievement__r.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SKILL_RECORD_TYPE ||
                            (ppa.Achievement__r.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SKILL_RECORD_TYPE && ppa.Achievement__r.Group_Type__c == AchievementUtils.ACHIEVEMENT_MANUAL_GROUP_TYPE))
                            && ca.Level__c != null && ppa.Level__c != null) {
                    Double cAmount = AchievementUtils.getLevelValue(ca.Level__c);
                    Double pAmount = AchievementUtils.getLevelValue(ppa.Level__c);
                    if (cAmount >= pAmount) {
                        counter += weight;
                    } else {
                        counter += ((pAmount - cAmount) / pAmount) * weight;
                    }
                }
            }
        }
        return new Contact_Profile_Score__c(
                Contact__c = c.Contact__c,
                Contact_Profile__c = c.Id,
                Achievement_Profile__c = p.Id,
                unique_name__c = String.valueOf(c.Contact__c)+String.valueOf(p.Id),
                Match__c = sum == 0 ? 0 : counter/sum);
    }

    public static Set<ID> getGroupsByMemberIds(Set<ID> achievementIds) {
        Set<ID> groupIds = new Set<ID>();
        for (Achievement_Group_Item__c g : [SELECT Group__c FROM Achievement_Group_Item__c WHERE Achievement__c IN :achievementIds AND Group__r.Group_Type__c != :AchievementUtils.ACHIEVEMENT_MANUAL_GROUP_TYPE]) {
            groupIds.add(g.Group__c);
        }
        return groupIds;
    }

    public static void calculateContactAchievementGroupValues(Set<ID> contactProfileIds, Set<ID> groupIds) {
        if (!groupIds.isEmpty()) {
            if ((contactProfileIds != null && contactProfileIds.size() <= AchievementUtils.EMPLOYEE_LIMIT) &&
                [SELECT Count() FROM AsyncApexJob WHERE JobType='BatchApex' AND ApexClass.Name = 'AchievementGroupPartialCalculationJob' AND Status IN ('Queued', 'Holding') LIMIT 5] < 5) {
                Database.executeBatch(new AchievementGroupPartialCalculationJob(contactProfileIds, groupIds));
            } else if ([SELECT Count() FROM AsyncApexJob WHERE JobType='BatchApex' AND ApexClass.Name = 'AchievementGroupCalculationJob' AND Status IN ('Queued', 'Holding') LIMIT 1] == 0) {
                Database.executeBatch(new AchievementGroupCalculationJob(groupIds));
            }
        }
    }

    public static void calculateContactAchievementGroupValuesWithoutBatch(List<Achievement_Set__c> profiles, Set<Id> groups) {
        Map<ID, Set<ID>> skillIdToGroupIds = new Map<ID, Set<ID>>();
        Map<ID, Achievement__c> groupMap = new Map<ID, Achievement__c>();
        for (Achievement__c grp : AchievementService.getAchievementGroups(groups)) {
            for (Achievement_Group_Item__c item : grp.Achievement_Group_Items__r) {
                if (!skillIdToGroupIds.containsKey(item.Achievement__c)) {
                    skillIdToGroupIds.put(item.Achievement__c, new Set<ID>());
                }
                skillIdToGroupIds.get(item.Achievement__c).add(grp.Id);
            }
            groupMap.put(grp.Id, grp);
        }

        List<Achievement_Set_Item__c> items = new List<Achievement_Set_Item__c>();
        Map<ID, Map<ID, Achievement_Set_Item__c>> groupToItemMap = new Map<ID, Map<ID, Achievement_Set_Item__c>>();
        
        for (Achievement_Set__c profile : profiles) {
            for (Achievement_Set_Item__c item : profile.Achievements__r) {
                if (skillIdToGroupIds.containsKey(item.Achievement__c)) {
                    for (ID groupId : skillIdToGroupIds.get(item.Achievement__c)) {
                        Achievement__c grp = groupMap.get(groupId);
                        Achievement_Set_Item__c grpItem = addGroupAchievement(items, groupToItemMap, profile, grp);
                        if (grp.Group_Type__c == AchievementUtils.ACHIEVEMENT_GREATEST_VALUE_GROUP_TYPE) {
                            if (AchievementUtils.compareLevel(grpItem.Level__c, item.Level__c)) {
                                grpItem.Level__c = item.Level__c;
                            }
                        }
                    }
                }
                if (groupMap.containsKey(item.Achievement__c)) {
                    Achievement_Set_Item__c grpItem = addGroupAchievement(items, groupToItemMap, profile, groupMap.get(item.Achievement__c));
                    grpItem.Id = item.Id;
                }
            }
        }


        List<Achievement_Set_Item__c> itemsToUpsert = new List<Achievement_Set_Item__c>();
        List<Achievement_Set_Item__c> itemsToDelete = new List<Achievement_Set_Item__c>();
        for (Achievement_Set_Item__c item : items) {
            Achievement__c grp = groupMap.get(item.Achievement__c);
            if (grp != null) {
                if(grp.Group_Type__c == AchievementUtils.ACHIEVEMENT_GREATEST_VALUE_GROUP_TYPE) {
                    if (item.Level__c != null && item.Level__c != '') {
                        itemsToUpsert.add(item);
                    } else if (item.Id != null) {
                        itemsToDelete.add(item);
                    }
                }
            }
        }

        if (!itemsToUpsert.isEmpty()) {
            upsert itemsToUpsert;
        }
        if (!itemsToDelete.isEmpty()) {
            delete itemsToDelete;
        }
    }
    
    private static Achievement_Set_Item__c addGroupAchievement(List<Achievement_Set_Item__c> items, Map<ID, Map<ID, Achievement_Set_Item__c>> groupToItemMap, Achievement_Set__c profile, Achievement__c grp) {
        Achievement_Set_Item__c item;
        if (!groupToItemMap.containsKey(grp.Id)) {
            groupToItemMap.put(grp.Id, new Map<ID, Achievement_Set_Item__c>());
        }
        if (!groupToItemMap.get(grp.Id).containsKey(profile.Id)) {
            item = new Achievement_Set_Item__c(Achievement_Set__c=profile.Id, Achievement__c=grp.Id);
            items.add(item);
            groupToItemMap.get(grp.Id).put(profile.Id, item);
        }
        item = groupToItemMap.get(grp.Id).get(profile.Id);
        return item;
    }
}