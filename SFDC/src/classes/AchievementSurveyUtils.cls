public with sharing class AchievementSurveyUtils {
    final static public String ACHIEVEMENT_CERTIFICATE_VALUE = 'Yes';
    final static public String ACHIEVMENT_ID_CONF_KEY = 'achievmentId';
    final static public String ACHIEVMENTSET_ID_CONF_KEY = 'achievmentSetId';
    final static public String ACHIEVMENT_GET_ALL_CONF = 'getAll';
    final static public String ACHIEVMENT_EMAIL_CONF = 'email';

    public static Boolean checkItemsWithGetAll(List<SurveyTemplateItem__c> surveyTemplateItems) {
        Boolean getAll = false;
        for (SurveyTemplateItem__c item : surveyTemplateItems) {
            SurveyUtils.QuestionConfig cfg  = new SurveyUtils.QuestionConfig(item);
            if (cfg.getBoolean(ACHIEVMENT_GET_ALL_CONF)) {
                getAll = true;
                break;
            }
        }
        return getAll;
    }

    public virtual with sharing class AchievementQuestionProvider extends SurveyUtils.SurveyQuestionProvider {

        protected List<SurveyModel.Option> questionOptions = null;

        public AchievementQuestionProvider() {
        }

        public virtual override List<SurveyModel.Question> getQuestions(List<SurveyTemplateItem__c> surveyTemplateItems) {
           return getQuestionsFromTemplateItems(surveyTemplateItems);
        }

        protected virtual List<SurveyModel.Question> getQuestionsFromTemplateItems(List<SurveyTemplateItem__c> surveyTemplateItems) {
            List<SurveyModel.Question> questions = new List<SurveyModel.Question>();
            if (AchievementSurveyUtils.checkItemsWithGetAll(surveyTemplateItems)) {
                Integer order = 0;
                for ( Achievement__c item : AchievementService.getAllAchievements()) {
                    questions.add(prepareQuestion(item, ++order));
                }
            } else {
                System.debug('surveyTemplateItems:'+surveyTemplateItems);
                for (SurveyTemplateItem__c item : surveyTemplateItems) {
                    SurveyModel.Question question = getQuestion(new SurveyUtils.QuestionConfig(item));
                    if (question != null) {
                        questions.add(question);
                    }
                }
            }
            return questions;
        }

        protected virtual SurveyModel.Question getQuestion(QuestionConfig cfg) {
            List<SurveyModel.Question> questions = new List<SurveyModel.Question>();
            Id achievementId = cfg.getId(AchievementSurveyUtils.ACHIEVMENT_ID_CONF_KEY);
            if (achievementId != null) {
                Achievement__c achievement = AchievementService.getAchievement(achievementId);
                if (achievement != null) {
                    questions.add(prepareQuestion(achievement, cfg.getOrder()));
                } else {
                    throw new SurveyUtils.SurveyQuestionProviderException('Cannot found Achievement with ID: ' + cfg.getId(AchievementSurveyUtils.ACHIEVMENT_ID_CONF_KEY));
                }
            } else {
                throw new SurveyUtils.SurveyQuestionProviderException('AchievementId is mandatory!');
            }
           return questions[0];
        }

        protected virtual Question prepareQuestion(Achievement__c achievement, Integer order) {
            Question question = new Question(achievement.Id, achievement.Name);
            question.order = order;
            question.description = String.isBlank(achievement.Details__c) ? '' : achievement.Details__c;
            question.readOnly = false;
            if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SKILL_RECORD_TYPE) {
                if (!String.isBlank(achievement.Level_1_Details__c) || !String.isBlank(achievement.Level_2_Details__c) || !String.isBlank(achievement.Level_3_Details__c)) {
                    if(!String.isBlank(question.description)){
                        question.description += '<br />';
                    }
                    question.description += achievement.Level_1_Details__c + '<br />' + achievement.Level_2_Details__c + '<br />' + achievement.Level_3_Details__c;
                }
                question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                question.options = questionLevelOptions();
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_CERTIFICATE_RECORD_TYPE) {
                question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                question.options = new List<SurveyModel.Option> {
                    new SurveyModel.Option(null, Label.None),
                    new SurveyModel.Option(AchievementSurveyUtils.ACHIEVEMENT_CERTIFICATE_VALUE, Label.Yes)
                };
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE) {
                if (achievement.Group_Type__c == AchievementUtils.ACHIEVEMENT_MANUAL_GROUP_TYPE) {
                    question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                    question.options = questionLevelOptions();
                } else {
                    question.readOnly = true;
                }
            } else {
                question.type = SurveyUtils.QUESTION_TYPE_TEXT;
            }
            return question;
        }

        protected List<SurveyModel.Option> questionLevelOptions() {
            if (questionOptions == null) {
                questionOptions = new List<SurveyModel.Option> {
                    new SurveyModel.Option(null, Label.None),
                    new SurveyModel.Option(AchievementUtils.ACHIEVEMENT_LEVEL_1, Label.SkillLevelJunior),
                    new SurveyModel.Option(AchievementUtils.ACHIEVEMENT_LEVEL_2, Label.SkillLevelProfessional),
                    new SurveyModel.Option(AchievementUtils.ACHIEVEMENT_LEVEL_3, Label.SkillLevelSenior)
                };
            }
            return questionOptions;
        }
    }

    public with sharing class AchievementPositionProfileQuestionProvider extends AchievementQuestionProvider {

        public AchievementPositionProfileQuestionProvider() {
        }

        public override Question prepareQuestion(Achievement__c achievement, Integer order) {
            Question question = super.prepareQuestion(achievement, order);
            if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE) {
                if (!String.isBlank(achievement.Level_1_Details__c) || !String.isBlank(achievement.Level_2_Details__c) || !String.isBlank(achievement.Level_3_Details__c)) {
                    if (!String.isBlank(question.description)) {
                        question.description += '<br />';
                    }
                    question.description += achievement.Level_1_Details__c + '<br />' + achievement.Level_2_Details__c + '<br />' + achievement.Level_3_Details__c;
                }
                question.type = SurveyUtils.QUESTION_TYPE_SELECT;
                question.options = questionLevelOptions();
                question.readOnly = false;
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_BADGE_RECORD_TYPE) {
                question.type = SurveyUtils.QUESTION_TYPE_POSITIVE_INTEGER;
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SPECIALIZATION_RECORD_TYPE) {
                question.type = SurveyUtils.QUESTION_TYPE_PERCENT;
            }
            return question;
        }
    }

    public with sharing class AchievementEmployeeProfileQuestionProvider extends AchievementQuestionProvider {

        public AchievementEmployeeProfileQuestionProvider() {
        }

        protected override Question prepareQuestion(Achievement__c achievement, Integer order) {
            Question question = super.prepareQuestion(achievement, order);
            if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_BADGE_RECORD_TYPE) {
                question.type = SurveyUtils.QUESTION_TYPE_POSITIVE_INTEGER;
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE || achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SPECIALIZATION_RECORD_TYPE) {
                question.readOnly = true;
                question.type = SurveyUtils.QUESTION_TYPE_TEXT;
            }
            return question;
        }
    }

    public with sharing class AchievementSurveyQuestionProvider extends AchievementQuestionProvider {

        public AchievementSurveyQuestionProvider() {
        }

        protected override List<SurveyModel.Question> getQuestionsFromTemplateItems(List<SurveyTemplateItem__c> surveyTemplateItems){
            List<SurveyModel.Question> questions = new List<SurveyModel.Question>();
            Integer index = getIndexEmailQuestion(surveyTemplateItems);
            Question question = new Question(AchievementSurveyUtils.ACHIEVMENT_EMAIL_CONF, AchievementSurveyUtils.ACHIEVMENT_EMAIL_CONF);
            question.type = SurveyUtils.QUESTION_TYPE_EMAIL;
            question.order = (Integer) surveyTemplateItems.get(index).Order__c;
            question.required = surveyTemplateItems.get(index).Required__c;
            question.readOnly = false;
            questions.add(question);
            surveyTemplateItems.remove(index);
            questions.addAll(super.getQuestionsFromTemplateItems(surveyTemplateItems));
            return questions;
        }

        private Integer getIndexEmailQuestion(List<SurveyTemplateItem__c> surveyTemplateItems) {
            Integer index = null;
            for (Integer i=0; i < surveyTemplateItems.size(); i++) {
                QuestionConfig cfg = new QuestionConfig(surveyTemplateItems.get(i));
                if (cfg.getBoolean(AchievementSurveyUtils.ACHIEVMENT_EMAIL_CONF)) {
                    index = i;
                    break;
                }
            }
            if (index == null) {
                throw new SurveyUtils.SurveyQuestionProviderException('Tamplate not contains question of email');
            }
            return index;
        }

        protected override Question prepareQuestion(Achievement__c achievement, Integer order) {
            Question question = super.prepareQuestion(achievement, order);
            if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_BADGE_RECORD_TYPE) {
                question.type = SurveyUtils.QUESTION_TYPE_POSITIVE_INTEGER;
            } else if (achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE || achievement.RecordType.DeveloperName == AchievementUtils.ACHIEVEMENT_SPECIALIZATION_RECORD_TYPE) {
                question.readOnly = true;
                question.type = SurveyUtils.QUESTION_TYPE_TEXT;
            }
            return question;
        }
    }

    public with sharing class AchievementQuestionGraphProvider extends AchievementQuestionProvider {

        private Map<String, Question> questions = new Map<String, Question>();
        private Integer order = 1;

        public AchievementQuestionGraphProvider() {
        }

        public override List<SurveyModel.Question> getQuestions(List<SurveyTemplateItem__c> surveyTemplateItems) {
            Set<ID> recordTypeIds = new Set<ID> {
                AchievementUtils.getAchievementSetRecordType(AchievementUtils.ACHIEVEMENT_SET_POSITION_RECORD_TYPE).Id,
                AchievementUtils.getAchievementSetRecordType(AchievementUtils.ACHIEVEMENT_SET_PROFILE_RECORD_TYPE).Id
            };
            List<Achievement_Set__c> achievementSets = AchievementService.getAchievementSets(null, recordTypeIds, true);
            questions = new Map<String, Question>();
            questions.put('rootId', new QuestionInfo('rootId', 'Root', null, order, false));
            for (Career_Path__c item : AchievementService.getCareerPaths()) {
                questions.put(item.Id, new QuestionInfo(item.Id, item.Name, item.Description__c, ++order, false));
                questions.get(item.Id).addParentId('rootId');
            }
            for (Achievement_Set__c achievementSet : achievementSets) {
                String parentId = achievementSet.Parent_Profile__c == null ? achievementSet.Career_Path__c : achievementSet.Parent_Profile__c;
                if (!questions.containsKey(achievementSet.Id)) {
                    questions.put(achievementSet.Id, new QuestionInfo(achievementSet.Id, achievementSet.Name, achievementSet.Description__c, ++order, true));
                }
                questions.get(achievementSet.Id).addParentId(parentId);
                for (Achievement_Set_Item__c item : achievementSet.Achievements__r) {
                    Achievement__c achievement = AchievementService.getAchievement(item.Achievement__c);
                    if (!questions.containsKey(achievement.Id)) {
                        questions.put(achievement.Id, new Question(achievement, ++order, true));
                    }
                    questions.get(achievement.Id).addParentId(achievementSet.Id);
                    for (Achievement_Group_Item__c groupItem : achievement.Achievement_Group_Items__r) {
                        Achievement__c achievementFromGruop = AchievementService.getAchievement(groupItem.Achievement__c);
                        if (!questions.containsKey(item.Id)) {
                            questions.put(achievementFromGruop.Id, prepareQuestion(achievementFromGruop, ++order));
                        }
                        questions.get(achievementFromGruop.Id).addParentId(item.Id);
                    }
                }
            }
            for(Achievement__c achievement : AchievementService.getAllAchievements()){
                if (!questions.containsKey(achievement.Id)) {
                    questions.put(achievement.Id, prepareQuestion(achievement, ++order));
                }
            }
            return questions.values();
        }

    }

    public with sharing virtual class Question extends SurveyModel.Question {

        private Set<String> parentIds = new Set<String>();
        private Boolean canOpen;
        private Boolean required = false;

        public Question(String id, String name) {
            super(id, name);
            canOpen = true;
        }

        public Question(Achievement__c achievement, Integer order, Boolean canOpen) {
            super(achievement.Id, achievement.Name, order, 'text', null, false);
            this.canOpen = canOpen;
        }

        public void addParentId(String parentId) {
            parentIds.add(parentId);
        }
    }

    public with sharing class QuestionInfo extends Question {

        public QuestionInfo(String id, String name, String description, Integer order, Boolean canOpen) {
            super(id, name);
            this.type = SurveyUtils.QUESTION_TYPE_INFO;
            this.order = order;
            this.description = description;
            this.canOpen = canOpen;
            this.readOnly = true;
        }
    }

    public abstract with sharing class AchievementBaseHandler extends SurveyUtils.SurveyHandler {

        public virtual override List<SurveyModel.SurveyInfo> getSurveyToTemplateInfo(SurveyConfig cfg, SurveyModel.TemplateInfo templateInfo, SurveyUtils.Request request){
            List<SurveyModel.SurveyInfo> surveys = new List<SurveyModel.SurveyInfo>();
            for (Achievement_Set__c achievementSet : getAchievementSets(cfg, request)) {
                SurveyModel.SurveyInfo survey = new SurveyModel.SurveyInfo();
                survey.surveyId = achievementSet.Id;
                survey.userId = request.UserId;
                survey.name = achievementSet.Name;
                survey.templateApiName = templateInfo.templateApiName;
                surveys.add(survey);
            }
            return surveys;
        }

        protected virtual List<Achievement_Set__c> getAchievementSets(SurveyConfig cfg, SurveyUtils.Request request) {
            List<Achievement_Set__c> achievementSets = new List<Achievement_Set__c>();
            Id achievementId = cfg.getId(AchievementSurveyUtils.ACHIEVMENTSET_ID_CONF_KEY);
            if (achievementId != null) {
                achievementSets.add(AchievementService.getAchievementSetInfo(achievementId));
            } else {
                achievementSets.addAll(AchievementService.getAchievementSets(null, getRecordType(), true));
            }
            return achievementSets;
        }

        public virtual override SurveyModel.Survey getSurvey(SurveyConfig cfg) {
            Set<Id> ids = getSurveyId(cfg);
            List<Achievement_Set__c> achievementSets = AchievementService.getAchievementSets(ids, null, true);
            if(achievementSets.isEmpty()){
                throw new AchievementBaseHandlerException('Not found Achievement Set with Id ' + ids);
            }
            Achievement_Set__c achievementSet = achievementSets[0];
            return prepareSurvey(achievementSet, cfg.getTemplateApiName());
        }

        protected virtual Set<Id> getSurveyId(SurveyConfig cfg) {
            Set<Id> ids = new Set<Id>();
            Id achievementSetId = cfg.getId(AchievementSurveyUtils.ACHIEVMENTSET_ID_CONF_KEY);
            if (achievementSetId != null) {
                ids.add(achievementSetId);
            } else {
                SurveyUtils.SurveyContext context = (SurveyUtils.SurveyContext)this.getContext();
                SurveyUtils.SurveyRequest request = (SurveyUtils.SurveyRequest)context.getRequest();
                if (request.surveyId != '' && request.surveyId != null) {
                    ids.add(request.surveyId);
                }
            }
            if (ids.isEmpty()) {
                throw new AchievementSurveyUtils.AchievementBaseHandlerException('Achievement Set was not set on the Survey Template!');
            }
            return ids;
        }

        protected virtual SurveyModel.Survey prepareSurvey(Achievement_Set__c achievementSet, String templateApiName) {
            SurveyUtils.SurveyContext context = (SurveyUtils.SurveyContext)this.getContext();
            SurveyUtils.SurveyRequest request = (SurveyUtils.SurveyRequest)context.getRequest();
            SurveyTemplate__c template = context.surveyTemplate();
            SurveyModel.Survey survey = new SurveyModel.Survey();
            List<SurveyModel.Answer> answers = new List<SurveyModel.Answer>();
            List<Achievement_Set_Item__c> achievementSetItems = new List<Achievement_Set_Item__c>();
            survey.surveyId = achievementSet.Id;
            survey.userId = request.userId;
            survey.name = achievementSet.Name;
            survey.templateApiName = templateApiName;
            if (checkItemsWithGetAll(template.SurveyTemplateItems__r)) {
                achievementSetItems.addAll(achievementSet.Achievements__r);
            } else {
                Map<Id, Achievement_Set_Item__c> achievementItems = prepareAchievementSetItemMap(achievementSet.Achievements__r);
                for (SurveyTemplateItem__c item : template.SurveyTemplateItems__r) {
                    SurveyUtils.QuestionConfig cfg = new SurveyUtils.QuestionConfig(item);
                    achievementSetItems.add(achievementItems.get(cfg.getId(AchievementSurveyUtils.ACHIEVMENT_ID_CONF_KEY)));
                }
            }
            for(Achievement_Set_Item__c item : achievementSetItems) {
                answers.add(prepareAnswer(item));
            }
            survey.answers = answers;
            return survey;
        }

        private Map<Id, Achievement_Set_Item__c> prepareAchievementSetItemMap(List<Achievement_Set_Item__c> items) {
            Map<Id, Achievement_Set_Item__c> achievementSetItems = new Map<Id, Achievement_Set_Item__c>();
            for (Achievement_Set_Item__c item : items) {
                achievementSetItems.put(item.Achievement__c, item);
            }
            return achievementSetItems;
        }

        private SurveyModel.Answer prepareAnswer(Achievement_Set_Item__c item) {
            SurveyModel.Answer answer = new SurveyModel.Answer();
            answer.id = item.Id;
            answer.questionId = item.Achievement__c;
            if (item.Type__c == AchievementUtils.ACHIEVEMENT_SKILL_RECORD_TYPE || item.Type__c == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE) {
               answer.value = item.Level__c;
            } else if (item.Type__c == AchievementUtils.ACHIEVEMENT_CERTIFICATE_RECORD_TYPE) {
                answer.value = AchievementSurveyUtils.ACHIEVEMENT_CERTIFICATE_VALUE;
            } else if (item.Type__c == AchievementUtils.ACHIEVEMENT_SPECIALIZATION_RECORD_TYPE) {
                answer.value = String.valueOf(item.Min_Speciality_Match__c);
            } else {
                answer.value = String.valueOf(item.Amount__c);
            }
            return answer;
        }

        public override void save(SurveyConfig cfg) {
            Achievement_Set__c achievementSet = getAchievementSetWithItems(cfg);
            SurveyUtils.SurveyContext context = (SurveyUtils.SurveyContext)this.getContext();
            List<SurveyModel.Answer> answers = prepareAnswerToSave(context.getSurveyModel().answers);
            List<Id> achievementIds = prepareAchievmentIds(answers);
            SurveyTemplate__c template = context.surveyTemplate();
            Map<Id, Achievement__c> achievementsItems = prepareAchievementsFromTemplateItem(template.SurveyTemplateItems__r);
            Map<Id, String> answersValue = prepareAnswersValueMap(answers);
            List<Achievement_Set_Item__c> achievementSetItems = prepareAchievementSetItemsFromSurveyAnswers(achievementSet, achievementsItems, answers);
            List<Achievement> achievements = AchievementService.convert(cfg.getId(AchievementSurveyUtils.ACHIEVMENTSET_ID_CONF_KEY), achievementIds, achievementSetItems);
            for (Achievement achievement : achievements) {
                if (achievement.getIsCert()) {
                    achievement.selected = (answersValue.get(achievement.oldRecord.Achievement__c) == AchievementSurveyUtils.ACHIEVEMENT_CERTIFICATE_VALUE) ? true : false;
                }
            }
            AchievementService.save(achievements);
        }

        protected virtual List<SurveyModel.Answer> prepareAnswerToSave(List<SurveyModel.Answer> answers) {
            return answers;
        }

        protected virtual Achievement_Set__c getAchievementSetWithItems(SurveyConfig cfg) {
            Set<ID> achievementIds = new Set<ID>();
            achievementIds.addAll(getSurveyId(cfg));
            List<Achievement_Set__c> achievementSets = getAchievementSets(achievementIds);
            if (achievementSets.isEmpty()) {
                throw new AchievementSurveyUtils.AchievementBaseHandlerException('Cannot find proper survey!');
            }
            return achievementSets[0];
        }

        public abstract List<Achievement_Set__c> getAchievementSets(Set<ID> achievementIds);
        public abstract Set<ID> getRecordType();

        protected virtual List<Id> prepareAchievmentIds(List<SurveyModel.Answer> answers) {
            List<Id> ids = new List<Id>();
            for (SurveyModel.Answer item : answers) {
                ids.add(item.questionId);
            }
            return ids;
        }

        protected Map<Id, String> prepareAnswersValueMap(List<SurveyModel.Answer> answers) {
            Map<Id, String> values = new Map<Id, String>();
            for (SurveyModel.Answer item : answers) {
                values.put(item.questionId, item.value);
            }
            return values;
        }

        protected Map<Id, Achievement__c> prepareAchievementsFromTemplateItem(List<SurveyTemplateItem__c> templateItems){
            Map<Id, Achievement__c> achievements = new Map<Id, Achievement__c>();
            if (AchievementSurveyUtils.checkItemsWithGetAll(templateItems)) {
                for ( Achievement__c item : AchievementService.getAllAchievements()) {
                    achievements.put(item.Id, item);
                }
            } else {
                for (SurveyTemplateItem__c item : templateItems) {
                    SurveyUtils.QuestionConfig cfg = new SurveyUtils.QuestionConfig(item);
                    Id achievementId = cfg.getId(ACHIEVMENT_ID_CONF_KEY);
                    achievements.put(achievementId, AchievementService.getAchievement(achievementId));
                }
            }
            return achievements;
        }

        protected virtual List<Achievement_Set_Item__c> prepareAchievementSetItemsFromSurveyAnswers(Achievement_Set__c achievementSet, Map<Id, Achievement__c> achievements, List<SurveyModel.Answer> answers) {
            List<Achievement_Set_Item__c> achievementSetItems = new List<Achievement_Set_Item__c>();
            Map<ID, Achievement_Set_Item__c> achievementIdToItemMap = mapAchievementIdToItems(achievementSet.Achievements__r);
            for (SurveyModel.Answer answer : answers) {
                ID achievementId = answer.questionId;
                if (achievements.containsKey(achievementId)) {
                    if(answer.value != '') {
                        Achievement_Set_Item__c item;
                        if (achievementIdToItemMap.containsKey(achievementId)) {
                            item = achievementIdToItemMap.get(achievementId);
                        } else {
                            item = new Achievement_Set_Item__c(
                                Achievement__c = achievementId,
                                Achievement_Set__c = achievementSet.Id
                            );
                        }
                        String recortTypeName = achievements.get(achievementId).RecordType.DeveloperName;
                        if (recortTypeName == AchievementUtils.ACHIEVEMENT_SKILL_RECORD_TYPE || recortTypeName == AchievementUtils.ACHIEVEMENT_GROUP_RECORD_TYPE) {
                            item.Level__c = answer.value;
                        } else if (item.Type__c == AchievementUtils.ACHIEVEMENT_SPECIALIZATION_RECORD_TYPE) {
                            item.Min_Speciality_Match__c = Decimal.valueOf(answer.value);
                        } else if(recortTypeName == AchievementUtils.ACHIEVEMENT_BADGE_RECORD_TYPE) {
                            item.Amount__c = Decimal.valueOf(answer.value);
                        }
                        achievementSetItems.add(item);
                    }
                } else {
                    throw new AchievementSurveyUtils.AchievementBaseHandlerException('This answer: "'+answer.value+'" cannot be save this survey');
                }
            }
            return achievementSetItems;
        }

        protected virtual Map<ID, Achievement_Set_Item__c> mapAchievementIdToItems(List<Achievement_Set_Item__c> items) {
            Map<ID, Achievement_Set_Item__c> achievementIdToItemMap = new Map<ID, Achievement_Set_Item__c>();
            for (Achievement_Set_Item__c oldItem : items) {
                achievementIdToItemMap.put(oldItem.Achievement__c, oldItem);
            }
            return achievementIdToItemMap;
        }
    }

    public with sharing class AchievementPositionProfileHandler extends AchievementBaseHandler {
        public override List<Achievement_Set__c> getAchievementSets(Set<ID> achievementIds) {
            return AchievementService.getAchievementSets(achievementIds, getRecordType(), true);
        }

        public override Set<ID> getRecordType() {
            return AchievementUtils.getPositionProfileRecordTypeIds();
        }
    }

    public with sharing class AchievementSpecializationHandler extends AchievementBaseHandler {
        public override List<Achievement_Set__c> getAchievementSets(Set<ID> achievementIds) {
            return AchievementService.getAchievementSets(achievementIds, getRecordType(), true);
        }

        public override Set<ID> getRecordType() {
            return AchievementUtils.getSpecializationRecordTypeIds();
        }
    }

    public with sharing class AchievementEmployeeProfileHandler extends AchievementBaseHandler {
        public override List<Achievement_Set__c> getAchievementSets(Set<ID> achievementIds) {
            return AchievementService.getAchievementSets(achievementIds, getRecordType(), true);
        }

        public override Set<ID> getRecordType() {
            return AchievementUtils.getContactProfileRecordTypeIds();
        }
    }

    public with sharing class AchievementSurveyHandler extends AchievementBaseHandler {
        public override List<Achievement_Set__c> getAchievementSets(Set<ID> achievementIds) {
            return null;
        }

        public List<Achievement_Set__c> getAchievementSets(Contact who, Contact aboutWho) {
            return AchievementService.getAchievementSets(null, getRecordType(), true, 'Who__c = \'' + who.Id + '\' AND AboutWho__c = \'' + aboutWho.Id + '\'');
        }

        public override Set<ID> getRecordType() {
            return AchievementUtils.getSurveyRecordTypeIds();
        }

        protected override List<Achievement_Set__c> getAchievementSets(SurveyConfig cfg, SurveyUtils.Request request) {
            List<Achievement_Set__c> achievementSets = new List<Achievement_Set__c>();
            Map<String, Contact> contacts = getContactMap(new List<String>{request.userId});
            Contact who = contacts.get(request.userId);
            if (who == null) {
                throw new AchievementSurveyService.AchievementSurveyServiceException('Not found user: ' + request.userId);
            }
            achievementSets.addAll(AchievementService.getAchievementSets(null, getRecordType(), true, 'Who__c = \'' + who.Id + '\''));

            return achievementSets;
        }

        protected override SurveyModel.Survey prepareSurvey(Achievement_Set__c achievementSet, String templateApiName) {
            SurveyModel.Survey survey = super.prepareSurvey(achievementSet, templateApiName);
            SurveyModel.Answer answer = new SurveyModel.Answer();
            answer.id = null;
            answer.questionId = ACHIEVMENT_EMAIL_CONF;
            answer.value = achievementSet.AboutWho__r.Email;
            survey.answers.add(answer);
            return survey;
        }

        protected override Achievement_Set__c getAchievementSetWithItems(SurveyConfig cfg) {
            SurveyUtils.SurveyContext context = (SurveyUtils.SurveyContext)this.getContext();
            String targetId = getTargetId(context.getSurveyModel().answers);
            SurveyUtils.SurveyRequest request = (SurveyUtils.SurveyRequest)context.getRequest();
            String userId = request.userId;
            if (String.isBlank(targetId)) {
                throw new AchievementSurveyService.AchievementSurveyServiceException('Please provide Target Id, this field is mandatory!');
            }
            Map<String, Contact> contacts = getContactMap(new List<String>{userId, targetId});
            Contact who = contacts.get(userId);
            Contact aboutWho = contacts.get(targetId);
            if (who == null) {
                throw new AchievementSurveyService.AchievementSurveyServiceException('Cannot find contact with email = ' + userId);
            }
            if (aboutWho == null) {
                throw new AchievementSurveyService.AchievementSurveyServiceException('Cannot find contact with email = ' + targetId);
            }
            List<Achievement_Set__c> achievementSets = getAchievementSets(who, aboutWho);
            if (achievementSets.isEmpty()) {
                Achievement_Set__c newSurvey = new Achievement_Set__c(Name = who.FirstName + ' ' + who.LastName + ' -> ' + aboutWho.FirstName + ' ' + aboutWho.LastName, Who__c=who.Id, AboutWho__c=aboutWho.Id, RecordTypeId=AchievementUtils.getAchievementSetSurveyRecordType().Id);
                insert newSurvey;
                request.surveyId = newSurvey.Id;
                achievementSets = getAchievementSets(who, aboutWho);
            }
            return achievementSets[0];
        }

        private Map<String, Contact> getContactMap(List<String> emails){
            Map<String, Contact> users = new Map<String, Contact>();
            for(Contact item : [SELECT Id, Email, Name, Firstname, Lastname FROM Contact WHERE Email IN :emails LIMIT 2]){
                users.put(item.Email, item);
            }
            return users;
        }

        protected override List<SurveyModel.Answer> prepareAnswerToSave(List<SurveyModel.Answer> answers) {
            List<Integer> indexes = new List<Integer>();
            for (Integer i = 0; i < answers.size(); i ++) {
                 if (answers.get(i).questionId == SurveyUtils.QUESTION_TYPE_EMAIL) {
                    answers.remove(i);
                 }
            }
            return answers;
        }

        private String getTargetId(List<SurveyModel.Answer> answers) {
            String getTargetId = '';
            for (SurveyModel.Answer item : answers) {
                 if (item.questionId == SurveyUtils.QUESTION_TYPE_EMAIL) {
                    getTargetId = item.value;
                    break;
                 }
            }
            return getTargetId;
        }
    }


    public virtual class AchievementBaseHandlerException extends Exception {}
    public virtual class AchievementSurveyServiceException extends Exception {}
}