global with sharing class SurveyFrontController {

    private static SurveyFrontController instance = null;

    global static SurveyFrontController getInstance() {
        if (instance == null) {
            instance = new SurveyFrontController();
        }
        return instance;
    }

    private SurveyFrontController(){
    }

    global List<SurveyModel.TemplateInfo> getTemplatesInfo(SurveyUtils.Request request) {
        validateRequest(request);
        List<SurveyModel.TemplateInfo> templatesInfo = new List<SurveyModel.TemplateInfo>();

        for (SurveyTemplate__c template : [SELECT Id, Name, ApiName__c, StartDate__c, EndDate__c, Importance__c, Description__c, Private__c, Submittable__c, MultiSurvey__c, Active__c, SurveyHandler__r.Name, Config__c,
                                           (SELECT  OnlyLeaders__c, Contact__r.Email FROM SurveyTemplateAccesses__r)
                                           FROM SurveyTemplate__c
                                           WHERE Active__c = true
                                                AND (StartDate__c = null OR StartDate__c <= TODAY)
                                                AND (EndDate__c = null OR EndDate__c >= TODAY)]) {

            if (!template.Private__c || (template.Private__c && checkTemplateAccess(request.userId, template.SurveyTemplateAccesses__r))) {
                SurveyModel.TemplateInfo templateInfo = new SurveyModel.TemplateInfo(template);
                SurveyUtils.SurveyConfig cfg = new SurveyUtils.SurveyConfig(template);
                templateInfo.surveys.addAll(SurveyUtils.getSurveyHandler(template.SurveyHandler__r.Name).getSurveyToTemplateInfo(cfg, templateInfo, request));
                templatesInfo.add(templateInfo);
            }
        }
        return templatesInfo;
    }


    global SurveyModel.Template getTemplate(SurveyUtils.TemplateRequest request) {
        validateTemplateRequest(request);

        SurveyUtils.TemplateContext templateContext = new SurveyUtils.TemplateContext(request);

        SurveyModel.Template template = templateContext.getTemplate();

        validateTamplateQuestions(template);

        return template;
    }


    global SurveyModel.Survey getSurvey(SurveyUtils.SurveyRequest request) {
        validateSurveyRequest(request);

        SurveyUtils.SurveyContext templateContext = new SurveyUtils.SurveyContext(request);
        SurveyModel.Survey survey = templateContext.getSurvey();

        return survey;
    }

    global SurveyModel.Survey saveSurvey(String rawSurvey) {
        SurveyModel.Survey survey = parseSurvey(rawSurvey);
        SurveyUtils.SurveyRequest request = new SurveyUtils.SurveyRequest(survey);
        validateSurveyRequest(request);

        SurveyUtils.SurveyContext surveyContext = new SurveyUtils.SurveyContext(request, survey);

        surveyContext.saveSurvey();

        return getSurvey(request);
    }

    public List<SurveyModel.Survey> getSurveysToCompare(SurveyUtils.SurveyRequest request) {
        return null;
    }

    public Map<ID, Contact> getUsers() {
        return new Map<Id, Contact>([SELECT Id, Email, Name FROM Contact WHERE SkillAssessment__c = true]);
    }

    private void validateRequest(SurveyUtils.Request request) {
        if (String.isBlank(request.userId)) {
            throw new SurveyFrontControllerException('userId field value is mandatory!');
        }

    }

    private void validateTemplateRequest(SurveyUtils.TemplateRequest request) {
        validateRequest(request);
        if (String.isBlank(request.templateApiName)) {
            throw new SurveyFrontControllerException('templateApiName field value is mandatory!');
        }
    }

    private void validateSurveyRequest(SurveyUtils.SurveyRequest request) {
        validateTemplateRequest(request);
        /*
        if (String.isBlank(request.surveyId)) {
            throw new SurveyFrontControllerException('surveyId field value is mandatory!');
        }
        */
    }

    private void validateTamplateQuestions(SurveyModel.Template template) {
        validateTamplateDates(template);
        if (template.questions.isEmpty()) {
            throw new SurveyFrontControllerException(template.Name + ' dosen\'t contain any questions');
        }
    }

    private void validateTamplateDates(SurveyModel.Template template) {
        if (!(template.startDate <= System.today() && template.endDate >= System.today()) && (template.startDate != null && template.endDate != null)) {
            throw new SurveyFrontControllerException(template.Name + ' is not unavailable');
        }
    }

    private SurveyModel.Survey parseSurvey(String rawSurvey) {
        if (String.isBlank(rawSurvey)) {
            throw new SurveyFrontControllerException('Survey Response cannot be blank!');
        }
        return (SurveyModel.Survey)JSON.deserialize(rawSurvey, SurveyModel.Survey.class);
    }

    private Boolean checkTemplateAccess(String userId, List<SurveyTemplateAccesses__c> templateAccesses) {
        if (isUserHasAccess(templateAccesses, userId) || isLeader(userId)) {
            return true;
        } else {
            return false;
        }
    }

    private Boolean isUserHasAccess(List<SurveyTemplateAccesses__c> templateAccesses, String userId) {
        Boolean access = false;
        for (SurveyTemplateAccesses__c item : templateAccesses) {
            if (!item.OnlyLeaders__c && item.Contact__r.Email == userId) {
                access = true;
                break;
            }
        }
        return access;
    }

    private Boolean isLeader(String userId) {
        List<Contact> contacts = new List<Contact>();
        contacts.addAll([SELECT IsLeader__c FROM Contact WHERE Email= :userId LIMIT 1]);
        if (!contacts.isEmpty()) {
            return contacts[0].IsLeader__c;
        } else {
            return false;
        }
    }

    public class SurveyFrontControllerException extends SurveyUtils.SurveyException {}































/*
    global Map<String, Object> getTemplate(SurveyUtils.TemplateRequest request) {
        validateTemplateRequest(request);

        SurveyTemplate__c template = loadSurveyTemplate(request.templateApiName, null);

        if (!template.AutogeneratedQuestions__c && template.SurveyTemplateItems__r.isEmpty()) {
            throw new SurveyFrontControllerException('This template doesn\'t contains any question');
        }

        return prepareTemplate(request, template);
    }

    global List<SurveyModel.TemplateInfo> getTemplatesInfo(SurveyUtils.Request request) {
        validateRequest(request);

        return loadSurveyTemplatesInfo(request);
    }

    global Map<String, Object> getSurvey(SurveyUtils.SurveyRequest request) {
        validateTemplateRequest(request);
        SurveyTemplate__c template = loadSurveyTemplate(request.templateApiName, request.userId);
        validateSurveyRequest(request, template.Private__c);

        if (template.Surveys__r.isEmpty()) {
            throw new SurveyFrontControllerException('There are no survey');
        }

        return prepareSurvey(request, template, template.Surveys__r[0]);
    }

    global Map<String, Object> saveSurvey(String rawSurvey) {
        SurveyModel.Survey surveyInfo = parseSurvey(rawSurvey);
        SurveyUtils.SurveyRequest request = new SurveyUtils.SurveyRequest(surveyInfo);
        validateTemplateRequest(request);

        SurveyTemplate__c template = loadSurveyTemplate(request.templateApiName, request.userId);
        validateSurveyRequest(request, template.Private__c);

        Survey__c survey = !template.Surveys__r.isEmpty() ?
            template.Surveys__r[0] :
            new Survey__c(UserId__c = surveyInfo.userId, SurveyTemplate__c = template.Id);

        saveSurvey(rawSurvey, surveyInfo, template, survey);

        return getSurvey(request);
    }

    private void validateSurveyRequest(SurveyUtils.SurveyRequest request, Boolean isPrivate) {
        if (String.isBlank(request.surveyId) && isPrivate) {
            throw new SurveyFrontControllerException('surveyId field value is mandatory!');
        }
    }

    private void validateTemplateRequest(SurveyUtils.TemplateRequest request) {
        validateRequest(request);
        if (String.isBlank(request.templateApiName)) {
            throw new SurveyFrontControllerException('templateApiName field value is mandatory!');
        }
    }

    private void validateRequest(SurveyUtils.Request request) {
        if (String.isBlank(request.userId)) {
            throw new SurveyFrontControllerException('userId field value is mandatory!');
        }
    }

    private SurveyTemplate__c loadSurveyTemplate(String templateApiName, String userId) {
        List<SurveyTemplate__c> templates =
            [SELECT Id, Name, ApiName__c, StartDate__c, EndDate__c, Importance__c, Description__c, Private__c, Submittable__c, MultiSurvey__c, Active__c, AutogeneratedQuestions__c, ServiceName__c,
                (SELECT Name, Order__c, ServiceName__c FROM SurveyTemplateItems__r ORDER BY Order__c),
                (SELECT Name, DueDate__c, Status__c, SurveyTemplate__c, UserId__c, TargetId__c, SurveyTemplate__r.ApiName__c FROM Surveys__r WHERE UserId__c = :userId)
            FROM SurveyTemplate__c
            WHERE Active__c = true
                AND (StartDate__c = null OR StartDate__c <= TODAY)
                AND (EndDate__c = null OR EndDate__c >= TODAY)
                AND ApiName__c=:templateApiName LIMIT 1];

        if (templates.isEmpty()) {
            throw new SurveyFrontControllerException('Cannot found template with templateApiName : '+ templateApiName);
        }
        return templates[0];
    }

    private List<SurveyModel.TemplateInfo> loadSurveyTemplatesInfo(SurveyUtils.Request request) {
        List<SurveyModel.TemplateInfo> templatesInfo = new List<SurveyModel.TemplateInfo>();
        Date currentDate = Date.today();

        for (SurveyTemplate__c template : [SELECT Id, Name, ApiName__c, StartDate__c, EndDate__c, Importance__c, Description__c, Private__c, Submittable__c, MultiSurvey__c, Active__c, AutogeneratedQuestions__c, ServiceName__c,
                                                (SELECT Name, DueDate__c, Status__c, SurveyTemplate__c, UserId__c, TargetId__c FROM Surveys__r WHERE UserId__c = :request.userId)
                                           FROM SurveyTemplate__c
                                           WHERE Active__c = true
                                                AND (StartDate__c = null OR StartDate__c <= TODAY)
                                                AND (EndDate__c = null OR EndDate__c >= TODAY)]) {

            if (!template.Private__c || (!template.Surveys__r.isEmpty() && template.Private__c)) {

                SurveyModel.TemplateInfo templateInfo = new SurveyModel.TemplateInfo(template);

                for (Survey__c survey : template.Surveys__r) {
                    templateInfo.surveys.add(new SurveyModel.SurveyInfo(survey, template.ApiName__c));
                }

                templatesInfo.add(templateInfo);
            }
        }
        return templatesInfo;
    }

    private Map<String, Object> prepareTemplate(SurveyUtils.TemplateRequest templateRequest, SurveyTemplate__c template) {
        Map<String, Object> response = new Map<String, Object>();
        Map<String, List<SurveyTemplateItem__c>> serviceNameToQuestions = new Map<String, List<SurveyTemplateItem__c>>();
        for (SurveyTemplateItem__c question : template.SurveyTemplateItems__r) {
            if (!serviceNameToQuestions.containsKey(question.ServiceName__c)) {
                serviceNameToQuestions.put(question.ServiceName__c, new List<SurveyTemplateItem__c>());
            }
            serviceNameToQuestions.get(question.ServiceName__c).add(question);
        }

        Map<String, SurveyUtils.SurveyService> services = getSurveyServices();
        for (String serviceName : services.keySet()) {
            SurveyUtils.SurveyService service = services.get(serviceName);
            if (serviceNameToQuestions.containsKey(serviceName) || (template.AutogeneratedQuestions__c && service.getCfg().SupportAutogeneratedQuestions__c && serviceName == template.ServiceName__c)) {
                List<SurveyTemplateItem__c> questions = serviceNameToQuestions.remove(serviceName);
                SurveyModel.Template surveyTemplate = service.assemblyTemplate(templateRequest, template, questions == null ? new List<SurveyTemplateItem__c>() : questions);
                SurveyUtils.exdendMap(response, (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(surveyTemplate)));
            }
        }

        List<Object> questions = response.get('questions') != null ? (List<Object>)response.get('questions') : new List<Object>();
        for (String serviceName : serviceNameToQuestions.keySet()) {
            for (SurveyTemplateItem__c question : serviceNameToQuestions.get(serviceName)) {
                questions.add(new SurveyModel.QuestionError(question, 'Survey Service : \'' + question.ServiceName__c + '\' was not configured'));
            }
        }
        response.put('questions', questions);
        return response;
    }

    private Map<String, Object> prepareSurvey(SurveyUtils.SurveyRequest request, SurveyTemplate__c template, Survey__c survey) {
        Map<String, Object> response = new Map<String, Object>();
        Map<String, SurveyUtils.SurveyService> services = getSurveyServices();

        for (String serviceName: services.keySet()) {
            SurveyUtils.SurveyService service = services.get(serviceName);
            if (!service.getCfg().SupportAutogeneratedQuestions__c || (template.AutogeneratedQuestions__c && service.getCfg().SupportAutogeneratedQuestions__c && serviceName == template.ServiceName__c)) {
                SurveyModel.Survey surveyDetails = service.assemblySurvey(request, template, survey);
                SurveyUtils.exdendMap(response, (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(surveyDetails)));
            }
        }

        return response;
    }

    private void saveSurvey(String rawSurvey, SurveyModel.Survey baseSurveyInfo, SurveyTemplate__c template, Survey__c survey) {
        Map<String, SurveyUtils.SurveyService> services = getSurveyServices();
        Map<String, List<SurveyModel.Answer>> serviceToAnswers = new Map<String, List<SurveyModel.Answer>>();

        for (SurveyModel.Answer answer : baseSurveyInfo.answers) {
            if (!serviceToAnswers.containsKey(answer.serviceName)) {
                serviceToAnswers.put(answer.serviceName, new List<SurveyModel.Answer>());
            }
            serviceToAnswers.get(answer.serviceName).add(answer);
        }

        for (String serviceName: services.keySet()) {
            if (serviceToAnswers.containsKey(serviceName)) {
                services.get(serviceName).saveSurvey(rawSurvey, baseSurveyInfo, serviceToAnswers.get(serviceName), template, survey);
            }
        }
    }



    private Map<String, SurveyUtils.SurveyService> getSurveyServices() {
        Map<String, SurveyUtils.SurveyService> services = SurveyUtils.getServices();
        if (services.isEmpty()) {
            throw new SurveyFrontControllerException('Surveys service configuration does not exists!');
        }
        return services;
    }
*/
}