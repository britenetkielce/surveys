/**
 * UpdateContactAchievementProfiles Trigger Test
 * @author Adam Siwek
 */
@IsTest
private class UpdateContactAchievementTriggerTest {

    static testMethod void shouldCreateContactProfileOnContactInsertion() {
        System.assertEquals(0, [SELECT Count() FROM Achievement_Set__c WHERE Contact__c != null LIMIT 1]);

        Contact c1 = new Contact(FirstName='Janusz', LastName='D');
        insert c1;

        System.assertEquals(1, [SELECT Count() FROM Achievement_Set__c WHERE Contact__c = :c1.Id AND Name LIKE '%Janusz%' LIMIT 2]);
    }

    static testMethod void shouldUpdateContactProfileNameOnContactNameUpdate() {
        Contact c1 = new Contact(FirstName='Janusz', LastName='D');
        insert c1;

        System.assertEquals(0, [SELECT Count() FROM Achievement_Set__c WHERE Contact__c = :c1.Id AND Name LIKE '%Marek%' LIMIT 1]);

        c1.FirstName = 'Marek';
        update c1;

        System.assertEquals(1, [SELECT Count() FROM Achievement_Set__c WHERE Contact__c = :c1.Id AND Name LIKE '%Marek%' LIMIT 2]);
    }

    static testMethod void shouldDeleteContactProfileOnContactDelete() {
        Contact c1 = new Contact(FirstName='Janusz', LastName='D');
        insert c1;

        System.assertEquals(1, [SELECT Count() FROM Achievement_Set__c WHERE Contact__c = :c1.Id LIMIT 2]);

        delete c1;

        System.assertEquals(0, [SELECT Count() FROM Achievement_Set__c WHERE Contact__c = :c1.Id LIMIT 1]);
    }
}