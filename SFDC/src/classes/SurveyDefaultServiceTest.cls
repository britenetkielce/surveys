@isTest
private class SurveyDefaultServiceTest {

    @isTest
    private static void shouldConstructSurveyDefaultService() {
        SurveyDefaultService sds = new SurveyDefaultService();
    }
/*
    @isTest
    private static void shouldGetTemplate() {
        //GIVEN
        String templateName = 'template';
        String userId = 'testuser@test.com';
        SurveyTestUtils.SurveyConfigBuilder cfg = SurveyTestUtils.createDefaultQuestionProvider('questionProvider');
        SurveyQuestion__c question = new SurveyTestUtils.QuestionBuilder().buildAndSave();
        SurveyTemplate__c template = new SurveyTestUtils.TemplateBuilder(templateName).buildAndSave();
        Achievement_Category__c category = new AchievementUtils.AchievementCategoryBuilder('name').buildAndSave();
        Achievement__c achievement = new AchievementUtils.AchievementBuilder('Apex', category).asSkill().buildAndSave();
        SurveyTemplateItem__c templateItem = new SurveyTestUtils.TemplateItemBuilder(template, question, cfg.getConfigs().get('questionProvider')).withConfig('{"id": "'+achievement.Id+'", "getAll":"true"}').buildAndSave();

        //WHEN
        SurveyModel.Template response = SurveyTestUtils.templateRequest(userId, templateName);

        //THEN
        System.assertEquals('template', response.name);
        System.assertEquals(1, response.questions.size());
        System.assertEquals('select', response.questions[0].type);
    }

    @isTest
    private static void shouldGetTemplateInfo() {
        //GIVEN
        String templateName = 'template';
        String userId = 'testuser@test.com';
        SurveyTemplate__c template = SurveyTestUtils.createDefaultTemplate(templateName);

        //WHEN
        List<SurveyModel.TemplateInfo> response = SurveyTestUtils.templatesInfoRequest(userId);

        //THEN
        System.assertEquals(1, response.size());
        System.assertEquals(templateName, response[0].name);
    }

    @isTest
    private static void shouldGetTemplatesInfo() {
        //GIVEN
        String userId = 'testuser@test.com';
        SurveyTemplate__c template1 = SurveyTestUtils.createDefaultTemplate('template1');
        SurveyTemplate__c template2 = SurveyTestUtils.createDefaultTemplate('template2');
        SurveyTemplate__c template3 = SurveyTestUtils.createDefaultTemplate('template3');

        //WHEN
        List<SurveyModel.TemplateInfo> response = SurveyTestUtils.templatesInfoRequest(userId);

        //THEN
        System.assertEquals(3, response.size());
        System.assertEquals('template1', response[0].name);
        System.assertEquals('template2', response[1].name);
        System.assertEquals('template3', response[2].name);
    }

    @isTest
    private static void shouldGetSurvey() {
        GIVEN
        String userId = 'testuser@test.com';
        String templateApiName = 'template';
        SurveyTestUtils.TemplateBuilder templateBuilder = SurveyTestUtils.buildDefaultTemplate(templateApiName);
        SurveyTemplate__c template = templateBuilder.buildAndSave();
        System.debug(SurveyTestUtils.SuveryBuilder(template, templateBuilder.getItems()).buildAndSave());

        WHEN
        SurveyModel.Survey surveyModel = getSurvey(userId, templateApiName, SurveyTestUtils.SuveryBuilder(template, templateBuilder.getItems()).buildAndSave().Id);

        //THEN
        System.assertEquals(2, surveyModel.answers.size());
        System.assertEquals('SurveyDefaultService', surveyModel.answers[0].serviceName);
        System.assertEquals('SurveyDefaultService', surveyModel.answers[1].serviceName);
    }

    @isTest
    private static void shouldSaveAnswersSurvey() {
        //GIVEN
        String userId = 'testuser@test.com';
        String templateApiName = 'template';
        String serviceName = 'SurveyDefaultService';

        SurveyTestUtils.TemplateBuilder templateBuilder = new SurveyTestUtils.TemplateBuilder(templateApiName).addItems(createQuestions());
        SurveyTemplate__c template = templateBuilder.buildAndSave();
        Survey__c survey = createSurvey(template, templateBuilder.getItems(), userId);

        //WHEN
        SurveyFrontController.getInstance().saveSurvey(createRawSurvey(survey.Id, serviceName));

        //THEN
        List<SurveyAnswer__c> answers = [SELECT Survey__c, SurveyTemplateItem__c, Value__c FROM SurveyAnswer__c WHERE Survey__c = :survey.Id];
        System.assertEquals(2, answers.size());
        System.assertEquals('1 answer', answers[0].Value__c);
        System.assertEquals('2 answer', answers[1].Value__c);
    }

    private static String createRawSurvey(Id surveyId, String serviceName) {
        SurveyModel.Survey survey = createSurveyInfo(surveyId);
        survey.answers = getSurveyAnswers(surveyId, serviceName);
        system.debug(JSON.serialize(survey));
        return JSON.serialize(survey);
    }

    //private static SurveyModel.Survey createSurveyInfo(Id surveyId) {
    //    Survey__c survey = [SELECT Name, SurveyTemplate__r.ApiName__c, UserId__c, DueDate__c FROM Survey__c WHERE Id = :surveyId];
    //    return new SurveyModel.Survey(survey);
    //}

    private static List<SurveyModel.Answer> getSurveyAnswers(Id surveyId, String serviceName) {
        List<SurveyModel.Answer> answers = new List<SurveyModel.Answer>();
        Integer counter = 1;
        List<SurveyAnswer__c> surveyAnswer = [SELECT Survey__c, SurveyTemplateItem__c, Value__c FROM SurveyAnswer__c WHERE Survey__c = :surveyId];
        String surveyTemplateItem = surveyAnswer[surveyAnswer.size()-1].SurveyTemplateItem__c;
        system.debug(surveyAnswer[surveyAnswer.size()-1]);
        delete surveyAnswer[surveyAnswer.size()-1];
        surveyAnswer.remove(surveyAnswer.size()-1);

        for (SurveyAnswer__c item: surveyAnswer) {
            answers.add(new SurveyModel.Answer(item.Id, item.SurveyTemplateItem__c, counter+' answer', null, serviceName));
            counter ++;
        }
        answers.add(new SurveyModel.Answer(null, surveyTemplateItem, counter+' answer', null, serviceName));

        return answers;
    }
*/

}