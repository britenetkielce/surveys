global with sharing class AchievementEmployeeRateSurveyService extends AchievementSurveyService {

    protected override AchievementSurveyService.Survey getSurveyDetails(SurveyUtils.SurveyRequest request, SurveyTemplate__c template, Survey__c survey, Achievement_Set__c achievementSet) {
        return new AchievementSurveyService.Survey(survey, request.targetId);
    }

    global override Achievement_Set__c getAchievementSetWithItems(SurveyUtils.SurveyRequest surveyInfo, SurveyTemplate__c template) {
        String userId = surveyInfo.userId;
        String targetId = surveyInfo.targetId;

        if (String.isBlank(userId)) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Please provide User Id, this field is mandatory!');
        }

        if (String.isBlank(targetId)) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Please provide Target Id, this field is mandatory!');
        }

        Map<String, Contact> contacts = getContactMap(new List<String>{userId, targetId});
        Contact who = contacts.get(userId);
        Contact aboutWho = contacts.get(targetId);

        if (who == null) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Cannot find contact with email = ' + userId);
        }
        if (aboutWho == null) {
            throw new AchievementSurveyService.AchievementSurveyServiceException('Cannot find contact with email = ' + targetId);
        }

        List<Achievement_Set__c> achievementSets = AchievementService.getAchievementSets(null, AchievementUtils.getSurveyRecordTypeIds(), true, 'Who__c = \'' + who.Id + '\' AND AboutWho__c = \'' + aboutWho.Id + '\'');

        if (achievementSets.isEmpty()) {
            achievementSets.add(new AchievementUtils.AchievementSetBuilder('').asSurvey(who, aboutWho).buildAndSave());
        }

        return achievementSets[0];
    }

    private Map<String, Contact> getContactMap(List<String> emails){
        Map<String, Contact> users = new Map<String, Contact>();
        for(Contact item : [SELECT Id, Email, Name, Firstname, Lastname FROM Contact WHERE Email IN :emails LIMIT 2]){
            users.put(item.Email, item);
        }
        return users;
    }
}