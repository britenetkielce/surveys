# Mobile SF App Front-end Boilerplate

### Requirements:

- **[node.js](https://nodejs.org/en/)**
- **[Git CLI](https://git-scm.com/downloads)**

### Setting up workspace

**Bower**

Install Bower globally using node's package manager:

```sh
npm install -g bower
```

**Developer tools**

Tools needed for building the project are listed in *package.json* file in project folder. Install them using:

```sh
npm install
```

This process takes some time to complete.

**Libraries**

JavaScript libraries and other packages are managed by Bower and are listed in *bower.json* file. Install them using:

```sh
bower install
```

After every update in *bower.json* file you should use command above to update dependencies in project. 

### Building project

#### Project name

Set main module name in files:

- gulp/build.js: line 22,
- index.*.js files in src/app directory,
- bower.json and package.json in root folder

#### Configuration

In directory */gulp* there is *salesforce.conf.js* in which you can set up credentials to SF organisation and static resource options.

#### Compilation

Project building is defined by Gulp scripts listed in folder */gulp*. There are 2 tasks created to handle application build & deployment:

**Building**

```sh
gulp sfBuild
```
This task gathers all project files and compiles them. It searches for library files from bower packages (*/bower_components* directory) and uses production versions of files. All *.js* files are concatenated into one *vendor.js* file in */dist/scripts* directory. Same thing with *.css* files - concatenated files are in */dist/styles/vendor.css*. Fonts (*eot,svg,ttf,woff*) are being copied to */dist/fonts*.

For application part, script takes all files in */app* directory. **JavaScript** files are concatenated and minified into */dist/scripts/app.js* file. For styling there is **SASS** preprocessor set up so all *.scss* files from */app* folder are compiled into CSS, concatenated and minified, autoprefixed and put into */dist/styles/app.css*.

Another important thing is *.html* files handling. Script takes all found template files and puts them into Angular's **$templateCache** map using their address (relative to *src* folder) as key. So if there is file */src/app/views/calendar/calendar.template.html* it will be compiled **into** */dist/scripts/app.js* as a `angular.$templateCache.put('app/views/calendar/calendar.template.html', '<div>content</div>')`. This can be then easily used in all Angular template-related things (routing or *ng-include* directive).

Last thing that this task does is creation of Salesforce **staticresource** package with settings from *salesforce.conf.js*. It's temporarily located in */.tmp* directory.

**Deploying**
```sh
gulp sfDeploy
```
This task tries to deploy mentioned static resource into Salesforce using credentials from */gulp/salesforce.conf.js* file. 

For ease of use you can line up these commands. Example in Windows PowerShell:
```sh
gulp sfBuild; gulp sfDeploy
```

### Adding new libraries
As libraries are managed by Bower, you can add new ones using

```sh
bower install --save package_name
```
Note the `--save` flag. It automatically adds an entry for that library in *bower.json* file so other developers after pulling changes can just type `bower install` to download libraries on their computer.

If the Bower package is described properly by author (in particular: field *main* in packages *bower.json* file) Gulp takes proper files and uses them in compilation (`gulp sfBuild`)

### Other features

**Unit tests**

With this boilerplate there is **Karma** test runner bundled. Unit tests should be written using *[Jasmine](http://jasmine.github.io/)*. Test files should be placed in */app* folder - they could be directly to corresponding JavaScript files, and **IMPORTANT** have name ending in *\*.spec.js*. For example when testing Angular controller *listview.controller.js* test file should be named *listview.controller.spec.js*. These files are not being compiled and are only used by Karma.

To run test use command:

```sh
gulp test
```

**E2E tests**

There is also integration test runner bundled - **Protractor**. This one's test files should  be placed in */e2e* folder in root project directory. How to write e2e tests for Angular you can find in [Protractor documentation](https://angular.github.io/protractor/#/api).

To run all tests use:

```sh
gulp protractor
```

**ESLint**

Project also uses JavaScript linter *ESLint* with Angular plugins. It displays found errors (syntax, good-practices etc.) during build phase. Found errors are just for information purposes so they don't abort building process. You can disable some files from error checking by writing entries in *.eslintignore* file.

**Live reloading**

Test application using *gulp serve* command. It'll open application in browser and whenever there is a change in source files it'll reload automatically.