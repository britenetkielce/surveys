'use strict';

angular.module('btn.survey')
    .service('btnSurveyAlert', ["$window", function($window) {
        this.alert = {};
        this.addAlert = function(message, type, timeout, closable){
            //$window.scrollTo(0, 0);
            this.alert = {
                message: message,
                type: type,
                closable: closable !== undefined ? closable : true
            };
        };
        this.removeAlert = function(){
            this.alert = null;
        }
    }]);
