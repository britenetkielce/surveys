'use strict';

angular
    .module('btn.survey')
    .service('btnSurveyConnection', ["$http", "$q", "$window","btnSurveySfdcConf", "btnSurveyConf", "btnSurveyAlert", "btnSurveyLabel", function ($http, $q, $window, btnSurveySfdcConf, btnSurveyConf, btnSurveyAlert, btnSurveyLabel) {
        const ERROR = 'error',
            EXCEPTION = 'exception';
        var sendRemoteAction = function(action, params) {
                var deferred = $q.defer(),
                callback = function(result, event) {
                    if (event.type == EXCEPTION && event.message && event.message.indexOf('Logged in?') != -1) {
                        $window.location.reload(); 
                    }
                    if (event.status) {
                        deferred.resolve(result);
                    } 
                    else {
                        btnSurveyAlert.addAlert(event.message+'<br>'+btnSurveyLabel.salesforceMessage, ERROR, true);
                        deferred.resolve(null);
                        return;
                    }
                };

                params.unshift(action);
                params.push(callback);
                params.push({escape: false});

                Visualforce.remoting.Manager.invokeAction.apply(Visualforce.remoting.Manager, params);
                return deferred.promise;
            },
            getTemplateByApiName = function(data){
                var template = {};

                return $http.get(btnSurveyConf.url.surveyTemplate.getTemplate)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getTemplateMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getTemplateByApiName.');
                    })
                    .then(function (response) {
                        angular.forEach(response.data, function(item){
                            if (item.template.templateApiName === data.templateApiName){
                                template = item.template;
                            }
                        });
                        return template;
                    });
            };

        this.getAllTemplates = function (data) {
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplatesInfo, [data]);
            }
            else {
                return $http.get(btnSurveyConf.url.surveyTemplate.getTemplatesInfo)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getAllTemplatesMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getAllTemplates.');
                    })
                    .then(function (response) {
                        return response.data;
                    });
            }
        };

        this.getTemplate = function (data) {
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplate, [data])
                    .then(function(response){
                        return response;
                    });
            }
            else {
                return getTemplateByApiName(data);
            }
        };

        this.saveSurvey = function (data) {
            var jsonData = angular.toJson(data);
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.saveSurvey, [jsonData])
                    .then(function(response){
                        return response;
                    });
            }
            else {
                return $http.get(btnSurveyConf.url.surveyTemplate.saveSurvey)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.saveSurveyMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully saveSurvey.');
                    })
                    .then(function (response) {
                        return response.data;
                    });
            }
        };

        this.getSurvey = function(data){
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getSurvey, [data]);
            }
            else {
                return $http.get(btnSurveyConf.url.surveyTemplate.getSurvey)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getSurveyMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getSurvey.');
                    })
                    .then(function (response) {
                        return response.data;
                    });
            }
            return null;
        };
    }]);
