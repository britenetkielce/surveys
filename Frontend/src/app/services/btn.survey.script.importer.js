angular.module('btn.survey').factory('btnSurveyScriptImporter', ["$document", "$q", "$rootScope", function($document, $q, $rootScope) {
    var _cache = {},
        _load = function(libSrc) {
            var d, scriptTag, s, onScriptLoad;

            if (angular.isDefined(_cache[libSrc])) {
                return _cache[libSrc];
            }

            onScriptLoad = function () {
                $rootScope.$apply(function() {
                    d.resolve();
                });
            };
            d = $q.defer();
            scriptTag = $document[0].createElement('script');

            scriptTag.type = 'text/javascript'; 
            scriptTag.async = true;
            scriptTag.src = libSrc;
            scriptTag.onreadystatechange = function () {
                if (this.readyState == 'complete') {
                    onScriptLoad();
                }
            };
            scriptTag.onload = onScriptLoad;

            s = $document[0].getElementsByTagName('body')[0];
            s.appendChild(scriptTag);
            _cache[libSrc] = d.promise;
            return _cache[libSrc];
        };

    return {
        load: function(libSrcs) {
            var i, p = [];
            if (angular.isArray(libSrcs)) {
                for (i = 0; i < libSrcs.length; i++) {
                    p.push(_load(libSrcs[i]));
                }
            } else {
                return _load(libSrcs);
            }
            return $q.all(p);
        }
    };
}]);