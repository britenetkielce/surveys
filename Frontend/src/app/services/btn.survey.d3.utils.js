angular.module('btn.survey').factory('btnSurveyD3Utils', [ function () {
    var d3;
    return {
        init : function(d3Inst) {
            d3 = d3Inst;
            return this;
        },
        createGraphBuilder : function(fixedNodes) {
            fixedNodes = angular.isDefined(fixedNodes) ? fixedNodes : false;
            return {
                fixedNodes : fixedNodes,
                nodeMap : {},
                linkMap : {},
                addNode : function(id, name, data) {
                    if (angular.isUndefined(this.nodeMap[id])) {
                        this.nodeMap[id] = angular.extend({id:id, name:name, fixed:fixedNodes, weight : 0, isVisible : true, dependent : {nodes : [], links : []}}, angular.isDefined(data) ? data : {});
                    } else {
                        angular.extend(this.nodeMap[id], {name:name}, angular.isDefined(data) ? data : {});
                    }
                    if (this.nodeMap[id].isRootNode) {
                        this.rootNode = this.nodeMap[id];
                    }
                    return this;
                },
                hasNode : function(id) {
                    return angular.isDefined(this.nodeMap[id]);
                },
                addLink : function(node1Id, node2Id, data, forceUpdate) {
                    var id = node1Id + node2Id;
                    if (angular.isUndefined(this.linkMap[id]) && angular.isUndefined(this.linkMap[node2Id + node1Id])) {
                        this.linkMap[id] = angular.extend({id : id, source:this.nodeMap[node1Id], target:this.nodeMap[node2Id], isVisible : true}, angular.isDefined(data) ? data : {});
                        this.nodeMap[node1Id].weight++;
                        this.nodeMap[node2Id].weight++;
                        this.nodeMap[node1Id].dependent.links.push(this.linkMap[id]);
                    } else if (forceUpdate) {
                        if (angular.isDefined(this.linkMap[id])) {
                            angular.extend(this.linkMap[id], angular.isDefined(data) ? data : {});
                        } else {
                            angular.extend(this.linkMap[node2Id + node1Id], angular.isDefined(data) ? data : {});
                        }
                    }
                    return this;
                },
                hideChilds : function(nodeId) {
                    this.changeVisibility(false, nodeId);
                    return this;
                },
                showChilds : function(nodeId) {
                    this.changeVisibility(true, nodeId);
                    return this;
                },
                getRootNode : function() {
                    return this.rootNode;
                },
                changeVisibility : function(visibility, nodeId) {
                    var updateNode = function(node) {
                            if (node.id != nodeId) {
                                node.isVisible = visibility;
                            }
                            angular.forEach(node.dependent.links, function(link) {
                                link.isVisible = visibility;
                            });
                            angular.forEach(node.dependent.nodes, function(node) {
                                if (node.isVisible != visibility) {
                                    updateNode(node, nodeId);
                                }
                            });
                        };
                    if (angular.isDefined(this.nodeMap[nodeId])) {
                        this.nodeMap[nodeId].isOpened = visibility;
                        updateNode(this.nodeMap[nodeId]);
                    }
                },
                getNodes : function() {
                    return this.nodeMap;
                },
                getNode : function(nodeId) {
                    return this.nodeMap[nodeId];
                },
                getLinks : function() {
                    var links = [];
                    angular.forEach(this.linkMap, function(v) {
                        links.push(v);
                    });
                    return links;
                },
                removeNode : function(nodeId){
                    delete this.nodeMap[nodeId];
                },
                removeLink : function(linkId){
                    delete this.linkMap[linkId];
                }
            };
        }
    };
}]);