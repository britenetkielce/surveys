angular.module('btn.survey')
    .service('btnSurveyService', ["btnSurveyConnectionBs", "btnSurveyProgressBar", "btnSurveyLabel", "btnSurveyAlert", "$location", function(btnSurveyConnectionBs, btnSurveyProgressBar, btnSurveyLabel, btnSurveyAlert, $location) {
        const WARNING = 'warning',
              SUCCESS = 'success';
        var validate = function(scope){
                var validated = true,
                    message = btnSurveyLabel.validateMessage + '<br/><ul>',
                    questions = [];

                scope.survey.answers.forEach(
                    function(item){
                        if (!item.value && item.required){
                            questions.push(scope.template.questions.filter(function(v){
                                return v.id === item.questionId;
                            })[0]);
                            validated = false;
                        }
                    }
                );

                questions.forEach(function(question){
                    message += '<li>' + question.name + '</li>';
                });
                message += '</ul>';
                if (!validated){
                    btnSurveyAlert.addAlert(message, WARNING);
                }
                return validated;
            },
            getTemplate = function(data){
                return btnSurveyConnectionBs.getTemplate(data);
            },
            getAllTemplates = function(scope){
                return btnSurveyConnectionBs.getAllTemplates({userId: scope.user});
            },
            createSurvey = function(scope){
                if (scope.survey === null) {
                    scope.survey = {};
                }
                scope.showSurvey = true;
            },
            fillSurvey = function(scope){
                var questionsMap = {},
                    answersMap = {},
                    items = [],
                    systemItems = [],
                    questions = [],
                    createAnswer = function(question) {
                        return {
                            questionId: question.id !== undefined ? question.id : '', 
                            value: '', 
                            $$text: '',
                            comment: question.comment !== undefined ? question.comment : '',
                            required: question.required 
                        };
                    };

                if (scope.template == null || scope.survey == null){
                    alert(btnSurveyLabel.surveyServiceAlert);
                    return;
                }

                if (scope.survey.answers == null || scope.survey.templateApiName != scope.template.templateApiName){
                    scope.survey.answers = [];
                }

                scope.survey.templateApiName = scope.template.templateApiName !== undefined ? scope.template.templateApiName : '';
                scope.survey.templateId = scope.template.id !== undefined ? scope.template.id : '';
                scope.survey.userId = scope.user;

                angular.forEach(scope.template.questions, function(question){
                    angular.forEach(question.options, function(option){
                        option.description = question.descriptions[option.id];
                    });
                    questionsMap[question.id] = question;
                });
                angular.forEach(scope.survey.answers, function(answer) {
                    if (questionsMap[answer.questionId] !== undefined){
                        answer.description = questionsMap[answer.questionId].descriptions[answer.value];
                        answer.required = questionsMap[answer.questionId].required;
                        answersMap[answer.questionId] = answer;
                    }
                });
                angular.forEach(questionsMap, function(question, id){
                    if (answersMap[id] === undefined){
                        scope.survey.answers.push(createAnswer(question));
                    }
                });
                angular.forEach(scope.survey.answers, function(answer){
                    var question = questionsMap[answer.questionId];
                    if (question !== undefined) {
                        if (question.isSystem) {
                            systemItems.push({question:question, answer:answer});
                        } else {
                            items.push({question:question, answer:answer});
                        }
                    }
                });
                questions['items'] = items;
                questions['systemItems'] = systemItems;
                return questions;
            };

        this.createSurvey = createSurvey;
        this.getTemplate = getTemplate;
        this.fillSurvey = fillSurvey;
        this.getAllTemplates = getAllTemplates;

        this.getSurvey = function(data){
            return btnSurveyConnectionBs.getSurvey(data);
        },
        this.onSave = function(scope) {
            if (validate(scope)) {
                btnSurveyProgressBar.start(scope.$parent.progressbar);
                btnSurveyConnectionBs.saveSurvey(scope.survey).then(function(savedSurvey){
                    if (savedSurvey){
                        scope.survey = savedSurvey;
                        scope.items = fillSurvey(scope);
                        btnSurveyAlert.addAlert(btnSurveyLabel.saveOkMessage, SUCCESS, 1000);
                        getAllTemplates({user:  scope.user}).then(function(updatetTemplates){
                            scope.templates = updatetTemplates;
                            for (var i = 0; i < updatetTemplates.length; i++){
                                if (updatetTemplates[i].templateApiName === scope.selectedTemplate.templateApiName){
                                    scope.selectedTemplate = updatetTemplates[i];
                                    break;
                                }
                            }
                            btnSurveyProgressBar.stop(scope.$parent.progressbar);
                            $location.path('/edit/' + scope.selectedTemplate.templateApiName + '/' + savedSurvey.surveyId);
                        });
                    }
                });
            }
        },
        this.onQuickSave = function(scope) {
            if (validate(scope)) {
                btnSurveyConnectionBs.saveSurvey(scope.survey).then(function(savedSurvey){
                    if (savedSurvey){
                        scope.survey = savedSurvey;
                        scope.items = fillSurvey(scope);
                        btnSurveyAlert.addAlert(btnSurveyLabel.saveOkMessage, SUCCESS, 1000);
                        getAllTemplates({user: scope.user}).then(function(updatetTemplates){
                           if ($location.$$path.match(/\/new\//g)) {
                                for (var i = 0; i < updatetTemplates.length; i++){
                                    if (updatetTemplates[i].templateApiName === scope.survey.templateApiName){
                                        scope.selectedTemplate = updatetTemplates[i];
                                        break;
                                    }
                                }
                                $location.path('/edit/' + scope.selectedTemplate.templateApiName + '/' + savedSurvey.surveyId);
                            }
                        });
                    }
                });
            }
        },
        this.newSurvey = function(scope){
            scope.showSurvey = false;
            scope.selected.value = '';
            getTemplate({userId: scope.user, templateApiName: scope.selectedTemplate.templateApiName}).then(function(response){
                scope.template = response;
                scope.survey = {};
                scope.$watch('survey', function(newVal){
                    if (newVal){
                        scope.showSurvey = true;
                    }
                });
            });
        },
        this.editSurvey = function(item, scope){
            scope.showSurvey = false;
            getTemplate({userId: scope.user, templateApiName: scope.selectedTemplate.templateApiName}).then(function(response){
                scope.template = response;
                var data = {
                    userId: scope.user,
                    templateApiName: response.templateApiName,
                    surveyId: item.surveyId
                };

                createSurvey(scope);
            });
            
        }
    }]
);