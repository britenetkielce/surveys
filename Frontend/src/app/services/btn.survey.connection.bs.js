'use strict';

angular
    .module('btn.survey')
    .service('btnSurveyConnectionBs', ["$http", "$q", "$window", "btnSurveyConf", "btnSurveyLabel", "btnSurveyAlert", function ($http, $q, $window, btnSurveyConf, btnSurveyLabel, btnSurveyAlert) {
        const ERROR = 'error';
        var getTemplateByApiName = function(data){
                var template = {};

                return $http.post(btnSurveyConf.url.surveyTemplate.getTemplate, data)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getTemplateMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getTemplateByApiName.');
                    })
                    .then(function (response) {
                        var data = angular.fromJson(response.data);
                        if (angular.isDefined(data[0]) && data[0].errorCode){
                            btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                        } else if(data.errorCode){
                            btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data.message)).message, ERROR, true);
                        } else {
                            return data;
                        }
                    });
            },
            getErrorMessage = function(message){
                return message.substring(message.indexOf('{'), message.indexOf('}')+1);
            };

        this.getAllTemplates = function (data) {
            return $http.post(btnSurveyConf.url.surveyTemplate.getTemplatesInfo, data)
                .error(function () {
                    btnSurveyAlert.addAlert(btnSurveyLabel.getAllTemplatesMessage, ERROR, true);
                    return;
                })
                .success(function () {
                    console.log('#Successfully getAllTemplates.');
                })
                .then(function (response) {
                    var data = angular.fromJson(response.data);
                    if (angular.isDefined(data[0]) && data[0].errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                    } else if(data.errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data.message)).message, ERROR, true);
                    } else {
                        return data;
                    }
                });
        };

        this.getTemplate = function (data) {
            return getTemplateByApiName(data);
        };

        this.saveSurvey = function (data) {
            var jsonData = angular.toJson(data);
            return $http.post(btnSurveyConf.url.surveyTemplate.saveSurvey, data)
                .error(function () {
                    btnSurveyAlert.addAlert(btnSurveyLabel.saveSurveyMessage, ERROR, true);
                    return;
                })
                .success(function () {
                    console.log('#Successfully saveSurvey.');
                })
                .then(function (response) {
                    var data = angular.fromJson(response.data);
                    if (angular.isDefined(data[0]) && data[0].errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                    } else if(data.errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data.message)).message, ERROR, true);
                    } else {
                        return data;
                    }
                });
        };

        this.getSurvey = function(data){
            return $http.post(btnSurveyConf.url.surveyTemplate.getSurvey, data)
                .error(function () {
                    btnSurveyAlert.addAlert(btnSurveyLabel.getSurveyMessage, ERROR, true);
                    return;
                })
                .success(function () {
                    console.log('#Successfully getSurvey.');
                })
                .then(function (response) {
                    var data = angular.fromJson(response.data);
                    if (angular.isDefined(data[0]) && data[0].errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                    } else if(data.errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data.message)).message, ERROR, true);
                    } else {
                        return data;
                    }
                });
        };
    }]);
