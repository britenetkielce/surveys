'use strict';

angular.module('btn.survey')
    .service('btnSurveyProgressBar', [ function() {
        this.start = function(progressBar){
            progressBar.start();
        }

        this.stop = function(progressBar){
            progressBar.set(100);
            progressBar.hide();
        }
    }]
);
