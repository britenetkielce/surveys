/**
 * Angular DialogBox service initialization.
 */
angular.module('btn.survey').factory('btnSurveyDialog', ["$uibModal", "btnSurveyLabel", function($uibModal, btnSurveyLabel) {
    const BTN_PRIMARY = 'btn-primary',
          BTN_DEFAULT = 'btn-default';
    return {
        alert : function(cfg) {
            var _cfg = angular.isObject(cfg) ? angular.extend({}, cfg) : {title:cfg};
            return this.custom({
                title: _cfg.title,
                titleIcon: _cfg.titleIcon,
                message: _cfg.message,
                answer: _cfg.answer,
                buttons:[
                    {
                        label : btnSurveyLabel.OK,
                        style : BTN_PRIMARY,
                        action : function(modalInstance) {
                            modalInstance.close(true);
                        }
                    }
                ]
            });
        },
        confirm : function(cfg) {
            var _cfg = angular.isObject(cfg) ? angular.extend({}, cfg) : {title:cfg};
            return this.custom({
                title:_cfg.title,
                titleIcon: _cfg.titleIcon,
                message:_cfg.message,
                answer: _cfg.answer,
                buttons:[
                    {
                        label : btnSurveyLabel.yes,
                        style : BTN_PRIMARY,
                        action : function(modalInstance) {
                            modalInstance.close(true);
                        }
                    },
                    {
                        label : btnSurveyLabel.no,
                        style : BTN_DEFAULT,
                        action : function(modalInstance) {
                            modalInstance.close(false);
                        }
                    }
                ]
            });
        },
        custom : function(cfg) {
            var _config = angular.extend({
                showMessage: angular.isDefined(cfg.message),
                showTitle: angular.isDefined(cfg.title),
                controller: 'BtnSurveyDialogController',
                templateUrl: 'app/templates/btn.survey.dialog.comment.html',
                controllerAs: 'modalDialogCtrl'
            }, cfg);
            return (cfg._modalInstance = $uibModal.open({
                controllerAs: _config.controllerAs,
                templateUrl : _config.templateUrl,
                windowClass: _config.windowClass,
                controller: _config.controller,
                size: _config.dialogSize,
                resolve: {
                    dialogModel : function() {
                        return _config;
                    }
                }
            })).result;
        }
    };
}]);