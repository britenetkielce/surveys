'use strict';

angular
    .module('btn.survey')
    .service('btnSurveyConnectionSfdc', ["$http", "$q", "$window","btnSurveySfdcConf", "btnSurveyAlert", "btnSurveyLabel", function ($http, $q, $window, btnSurveySfdcConf, btnSurveyAlert, btnSurveyLabel) {
        const ERROR = 'error',
            EXCEPTION = 'exception';
        var sendRemoteAction = function(action, params) {
            var deferred = $q.defer(),
                callback = function(result, event) {
                    if (event.type == EXCEPTION && event.message && event.message.indexOf('Logged in?') != -1) {
                        $window.location.reload(); 
                    }
                    if (event.status) {
                        deferred.resolve(result);
                    } 
                    else {
                        btnSurveyAlert.addAlert(event.message+'<br>'+btnSurveyLabel.salesforceMessage, ERROR, true);
                        deferred.resolve(null);
                        return;
                    }
                };

                params.unshift(action);
                params.push(callback);
                params.push({escape: false});

                Visualforce.remoting.Manager.invokeAction.apply(Visualforce.remoting.Manager, params);
                return deferred.promise;
            };

        this.getAllTemplates = function (data) {
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplatesInfo, [data]);
        };

        this.getTemplate = function (data) {
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplate, [data])
                .then(function(response){
                    return response;
                });
        };

        this.saveSurvey = function (data) {
            var jsonData = angular.toJson(data);
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.saveSurvey, [jsonData])
                .then(function(response){
                    return response;
                });
        };

        this.getSurvey = function(data){
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getSurvey, [data]);
        };
    }]);
