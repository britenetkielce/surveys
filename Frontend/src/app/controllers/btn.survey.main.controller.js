'use strict';

angular.module('btn.survey')
    .controller('MainController', ['$scope', 'btnSurveyConnection', 'ngProgressFactory', function($scope, btnSurveyConnection, ngProgressFactory) {
        $scope.survey = {};
        $scope.template = {};
        $scope.templates = {};
        $scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.setParent(document.getElementById('main-container'));
        $scope.progressbar.setColor('#337ab7');
        $scope.progressbar.setHeight('4px');
        angular.element($scope.progressbar.getDomElement()).addClass('container');
}]);