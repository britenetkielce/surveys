/**
 * Angular custom dialog window controller initialization.
 */
angular.module('btn.survey').controller('BtnSurveyDialogController', ['$scope', '$uibModalInstance', 'dialogModel', function($scope, $uibModalInstance, dialogModel) {
    
    this.value = null;
    this.dialogModel = dialogModel;
    this.modalInstance = $uibModalInstance;
     
    this.close = function() {
        $uibModalInstance.dismiss(false);
    };

    this.selectValue = function(value) {
        $uibModalInstance.close(value);
    };

    if (angular.isFunction(this.dialogModel.init)) {
        this.dialogModel.init();
    }
    
    $scope.getFieldValue = function(fieldName, record) {
        if (fieldName.indexOf('.') > 0) {
            angular.forEach(fieldName.split('.'), function(v) {
                if (angular.isDefined(record)) {
                    record = record[v];
                }
            });
            return record;
        }
        return record[fieldName];
    };
}]);