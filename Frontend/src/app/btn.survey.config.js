'use strict';

angular
    .module('btn.survey')
    .constant('btnSurveyConf',{
        url : {
            surveyTemplate : {
                getTemplatesInfo: 'app/mocks/questionsMap.json',
                getTemplate: 'app/mocks/extendentQuestion.json',
                getSurvey: '',
                saveSurvey: '',
                getUsers: 'app/mocks/contacts.json'
            }
        }
});

