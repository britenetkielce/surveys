'use strict';

angular
    .module('btn.survey')
    .constant('btnSurveyQuestionConf',{
        questionType : {
            error: true,
            info: true,
            text: true,
            select: true,
            picklist: true,
            percent: true,
            positiveInteger: true,
            email: true
        }
});

