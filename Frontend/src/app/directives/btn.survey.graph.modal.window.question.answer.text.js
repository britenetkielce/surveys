'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerText', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.text.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        scope.answer.$$text = newVal;
                    }
                );
            }
        }
    });
