'use strict';

angular.module('btn.survey')
    .filter('questionsFilter', function() {
        return function questionsFilter(questions, value) {
            var out = [];
            if (value == 'rated') {
                angular.forEach(questions, function(item) {
                    if (item.answer.value != '') { 
                        out.push(item);
                    }
                });
            } else if (value == 'notrated') {
                angular.forEach(questions, function(item) {
                    if (item.answer.value == '') { 
                        out.push(item);
                    }
                });
            } else if (value == 'ratedinselfsurvey') {
                angular.forEach(questions, function(item) {
                    if (item.answer.selfAnswer != null) { 
                        out.push(item);
                    }
                });
            } else if (value == 'ratedbycoworkers') {
                angular.forEach(questions, function(item) {
                    if (item.answer.otherEmployeeAvgValue != null || !angular.equals({}, item.answer.otherEmployeesSkillOrGroupValue)) { 
                        out.push(item);
                    }
                });
             } else if (value == 'ratedinselfsurveyorbycoworkers') {
                angular.forEach(questions, function(item) {
                    if (item.answer.otherEmployeeAvgValue != null || !angular.equals({}, item.answer.otherEmployeesSkillOrGroupValue) || item.answer.selfAnswer != null) { 
                        out.push(item);
                    }
                });
            } else {
                out = questions;
            }
            return out;
        };
    })
    .filter('categoryFilter', function() {
        return function categoryFilter(questions, value) {
            var out = [];
            var checked = [];
            angular.forEach(value, function(item) {
                checked.push(item.text);
            });
            if(checked.length > 0) {
                angular.forEach(questions, function(item) {
                    if (checked.indexOf(item.question.category) !== -1 || item.question.category === undefined || item.question.category === null) { 
                        out.push(item);
                    }
                });
            } else {
                out = questions;
            }

            return out;
        };
    })
    .directive('btnSurveyAchievementGrid', ["btnSurveyQuestionConf", "btnSurveyLabel", "$filter", function (btnSurveyQuestionConf, btnSurveyLabel, $filter) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            scope: true,
            templateUrl: 'app/templates/btn.survey.achievement.grid.html',
            link: function (scope){
               scope.btnSurveyLabel = btnSurveyLabel;
               scope.filterCheckbox = {};
               scope.allCategories = [];
               var tmp = [];
               angular.forEach(scope.items, function(item) {
                    if (item.question.category !== undefined && item.question.category !== null && tmp.indexOf(item.question.category) === -1) { 
                        tmp.push(item.question.category);
                        scope.allCategories.push({'text':item.question.category});
                    }
                });
               scope.loadCategories = function(query) {
                    return $filter('filter')(scope.allCategories, { text: query });
                };
            }
        };
}]);