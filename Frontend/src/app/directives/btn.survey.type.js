'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyType',["btnSurveyService", "btnSurveyAlert", "$location", "$route", "btnSurveyLabel", function(btnSurveyService, btnSurveyAlert, $location, $route, btnSurveyLabel) {
        var defaultTemplate = 0,
            hideSurvey = function(scope) {
                scope.showSurvey = false;
            },
            changePath = function(scope) {
                $location.path('/view/' + scope.selectedTemplate.templateApiName);
            }
            reload = function(scope){
                scope.showSurvey = false;
                scope.templateTmp = btnSurveyService.getTemplate({templateApiName: scope.selectedTemplate.templateApiName});
                scope.$watch('templateTmp', function(newVal){
                    if (newVal){
                        newVal.then(function(response){
                            scope.template = response;
                        });
                    }
                }, true);
            },
            reloadPage = function(href){
                if (href == $location.absUrl()){
                    $route.reload();
                }
            },
            onClear = function(scope) {
                scope.survey = {};
            },
            cfg = {
                readOnly: false,
                canSave: true,
                canClear: false,
                canEdit: false,
                showConfig: true
            }
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.type.html',
            transclude: true,
            scope: {
                templates: '=',
                survey: '=',
                mode: '=',
                canChangeMode: '=',
                config: '=',
                user: '@'
            },
            link: function(scope, element, attrs) {
                scope.btnSurveyLabel = btnSurveyLabel;
                if (!angular.isDefined(scope.selected)){
                    scope.selected = {value: null}
                }
                scope.reload = function(){
                    reload(scope);
                };
                scope.clear = function(){
                    onClear(scope);
                };
                scope.save = function(){
                    btnSurveyService.onSave(scope);
                };
                scope.onSelectTemplate = function() {
                    reload(scope);
                    scope.selected = {value: null};
                    hideSurvey(scope);
                    changePath(scope);
                };
                scope.onClickNew = function(event) {
                    scope.selected = {value: null};
                    reloadPage(event.target.href);
                };
                scope.onClickEdit = function(event) {
                    reloadPage(event.target.href);
                };
                scope.onSelectSurvey = function(event){
                    $location.path('/edit/' + scope.selectedTemplate.templateApiName + '/' + scope.selected.value.surveyId);
                }
            }
        }
    }]);