'use strict';

angular.module('btn.survey')
    .directive('btnSurveyButtons',["btnSurveyLabel", function (btnSurveyLabel) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            link: function (scope, element, attrs, surveyCtrl) {
                scope.config = surveyCtrl.getConfig();
                scope.btnSurveyLabel = btnSurveyLabel;
            },
            templateUrl: 'app/templates/btn.survey.buttons.html'
        };
    }]);
