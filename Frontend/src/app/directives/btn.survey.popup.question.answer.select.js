'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyPopupQuestionAnswerSelect',[ "btnSurveyLabel", "btnSurveyConf", function (btnSurveyLabel, btnSurveyConf) {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.popup.question.answer.select.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.getStars = function(num) {
                    return new Array(num+1);
                }
                scope.btnSurveySfdcConf = btnSurveyConf,
                scope.btnSurveyLabel = btnSurveyLabel,
                scope.checkedOption = function(value) {
                    if (scope.answer.value == '' || scope.answer.value != value) {
                        scope.answer.value = value;
                    } else if (scope.answer.value == value) {
                        scope.answer.value = '';
                    }
                },
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        var findTheSameValue = false;
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === newVal){
                                scope.answer.$$text = option.name;
                                findTheSameValue = true;
                            } else if (!findTheSameValue){
                                scope.answer.$$text = '';
                            }
                        });
                    }
                );
            }
        }
    }]);
