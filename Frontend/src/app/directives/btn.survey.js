'use strict';

angular.module('btn.survey')
    .directive('btnSurvey',["btnSurveyService", "btnSurveyLabel", function (btnSurveyService, btnSurveyLabel) {
        var modes = [{id: 'list', name: btnSurveyLabel.list}, {id: 'grid', name: btnSurveyLabel.grid}, {id: 'graph', name: btnSurveyLabel.graph}, {id: 'smallGrid', name: btnSurveyLabel.smallGrid}, {id: 'achievementGrid', name: btnSurveyLabel.achievementGrid}],
            defaulMode = modes[0],
            initializeMode = function (items, mode) {
                var returnedItem = null;
                angular.forEach(items, function (item) {
                    if (item.id === mode && returnedItem === null) {
                        returnedItem = item;
                    }
                });
                return returnedItem === null ? defaulMode : returnedItem;
            },
            showConfigurationPanel = function(){
                var display = angular.element('.configuration-panel').css('display');

                if (display === 'none'){
                    angular.element('.configuration-panel').css({display: 'block'});
                }
                else {
                    angular.element('.configuration-panel').css({display: 'none'});
                }
            };
        return {
            restrict: 'EA',
            scope: {
                mode: '=',
                canChangeMode: '=',
                config: '=',
                survey: '=',
                template: '=',
                user: '=',
                users: '=',
                save: '&',
                clear: '&',
                edit: '&'
            },
            controller: ['$scope', function($scope){
                var conf = $scope.config;
                var questionsItems= btnSurveyService.fillSurvey($scope);
                $scope.items = questionsItems['items'];
                $scope.systemItems = questionsItems['systemItems'];
                this.getConfig = function(){
                    return conf;
                };
                this.setConfig = function(extendConfig){
                    conf = angular.extend(conf, extendConfig);
                };
                this.geTemplate = function(){
                    return $scope.template;
                };
                this.getUsers = function() {
                    return $scope.users;
                }
                $scope.modes = modes;
                $scope.selectedMode = initializeMode(modes, $scope.mode);
            }],
            link: function (scope, element, attrs, controllers) {
                scope.cfg = controllers.getConfig();
                scope.btnSurveyLabel = btnSurveyLabel;
                scope.showConfigurationPanel = function(){
                    showConfigurationPanel();
                };
            },
            templateUrl: 'app/templates/btn.survey.html'
        };
    }]);
