'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerSelect', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.select.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        var findTheSameValue = false;
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === newVal){
                                scope.answer.$$text = option.name;
                                findTheSameValue = true;
                            } else if (!findTheSameValue){
                                scope.answer.$$text = '';
                            }
                        });
                    });
            }
        }
    });
