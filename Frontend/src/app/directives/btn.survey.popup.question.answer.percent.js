'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyPopupQuestionAnswerPercent', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.popup.question.answer.percent.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.value = {
                    percent: scope.answer.value * 100
                };
                if (scope.answer.value === ''){
                    scope.answer.value = 0;
                    scope.value.percent = 0;
                }
                scope.$watch(
                    function(){
                        return scope.value.percent;
                    }, function(newVal){
                        scope.answer.value = newVal / 100;
                        scope.answer.$$text = newVal+'%';
                    },
                    true
                );
            }
        }
    });
