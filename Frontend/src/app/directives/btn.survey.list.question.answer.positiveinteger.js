'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerPositiveinteger', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.list.question.answer.positiveinteger.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });
