'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerPicklist', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.answer.picklist.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });
