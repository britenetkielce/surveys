'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerEmail', [ function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.email.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    }]);
