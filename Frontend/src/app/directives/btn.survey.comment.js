'use strict';

angular.module('btn.survey')
    .directive('btnSurveyComment',["btnSurveyDialog", "btnSurveyLabel", function (btnSurveyDialog, btnSurveyLabel) {
        return {
            restrict: 'E',
            scope: {
                answer: '='
            },
            link: function (scope) {
                scope.onPostComment = function(){
                    btnSurveyDialog.alert({title: btnSurveyLabel.comment, message:btnSurveyLabel.commentMessage, answer: scope.answer});
                }
            },
            templateUrl: 'app/templates/btn.survey.comment.html'
        };
    }]);
