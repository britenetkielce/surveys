'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyPopupQuestionAnswerInfo', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.popup.question.answer.info.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });
