'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestion', ["btnSurveyDialog", "btnSurveyLabel", function (btnSurveyDialog, btnSurveyLabel) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.html',
            scope: {
                question: '=',
                answer: '=',
                users: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                var cfg = {
                    title: btnSurveyLabel.question, 
                    question: scope.question, 
                    answer: scope.answer,
                    config: surveyCtrl.getConfig(),
                    users: surveyCtrl.getUsers(),
                    templateUrl: 'app/templates/btn.survey.dialog.question.html',
                    buttons:[
                        {
                            label : btnSurveyLabel.OK,
                            style : 'btn-primary',
                            action : function(modalInstance) {
                                modalInstance.close(true);
                            }
                        }
                    ]
                };
                scope.config = surveyCtrl.getConfig();
                scope.onClickQuestion = function(){
                    btnSurveyDialog.custom(cfg);
                }
            }
        }
    }]);
