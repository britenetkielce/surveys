'use strict';

angular.module('btn.survey')
    .directive('btnSurveyGraph', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.graph.html'
        };
    });
