'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerPercent', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.list.question.answer.percent.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                if (scope.answer.value === ''){
                    scope.answer.value = 0;
                    scope.value = {
                        percent: 0
                    };
                } else {
                    scope.value = {
                        percent: scope.answer.value * 100
                    }
                }
                scope.$watch(
                    function(){
                        return scope.value.percent;
                    }, function(newVal){
                        scope.answer.value = newVal / 100;
                        scope.answer.$$text = newVal+'%';
                    },
                    true
                );
            }
        }
    });
