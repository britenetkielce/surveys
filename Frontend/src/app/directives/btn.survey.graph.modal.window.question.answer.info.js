'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerInfo', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.info.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });
