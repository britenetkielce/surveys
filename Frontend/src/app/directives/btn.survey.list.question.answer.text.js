'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerText', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.answer.text.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });
