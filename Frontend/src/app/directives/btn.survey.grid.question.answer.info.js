'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerInfo', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.info.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });
