'use strict';

angular.module('btn.survey')
    .directive('btnSurveyGraphModalWindow', ["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            scope: true,
            templateUrl: 'app/templates/btn.survey.graph.modal.window.html'
        };
}]);