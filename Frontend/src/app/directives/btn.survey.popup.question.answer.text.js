'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyPopupQuestionAnswerText', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.popup.question.answer.text.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        scope.answer.$$text = newVal;
                    }
                );
            }
        }
    });
