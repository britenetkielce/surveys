'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestion',["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.questionConfig = btnSurveyQuestionConf.questionType;
                scope.config = surveyCtrl.getConfig();
                scope.users = surveyCtrl.getUsers();
            }
        }
    }]);
