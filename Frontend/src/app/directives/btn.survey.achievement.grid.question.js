'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyAchievementGridQuestion', ["btnSurveyQuestionConf", "btnSurveyDialog", "btnSurveyLabel", "btnSurveyService", function (btnSurveyQuestionConf, btnSurveyDialog, btnSurveyLabel, btnSurveyService) {
        var EMAIL = 'email',
            SELECT = 'select',
            CHECKBOX = 'checkbox',
            PICKLIST = 'picklist',
            PERCENT = 'percent',
            FILLED_QUESTION_CLASS = 'filled-question',
            EMPTY_QUESTION_CLASS = 'empty-question',
            REQUIRED_QUESTION_CLASS = 'required-question',
            questionGridElement = document.getElementsByClassName("question-grid-element"),
            getAnswerText = function(scope){
                if (scope.question.type === EMAIL || scope.question.type === SELECT || scope.question.type === PICKLIST){
                    angular.forEach(scope.question.options, function(option){
                        if (option.id === scope.answer.value && option.id !== null){
                            scope.answer.$$text = option.name;
                        }
                    });
                    if (!scope.question.options){
                        scope.answer.$$text = scope.answer.value;
                    }
                    return scope.answer.$$text;
                } else if (scope.question.type === CHECKBOX) {
                    return scope.answer.value == btnSurveyLabel.yes ? scope.answer.$$text = btnSurveyLabel.have : scope.answer.$$text = "";
                } else if (scope.question.type === PERCENT){
                    return scope.answer.$$text = (scope.answer.value * 100) + '%';
                } else {
                    return angular.isObject(scope.answer.value) ? scope.answer.$$text = "" : scope.answer.$$text = scope.answer.value;
                }
            },
            initBorderShadowColor = function(scope, element) {
                if (scope.answer.value){
                    angular.element(questionGridElement).addClass(FILLED_QUESTION_CLASS);
                } else {
                    angular.element(questionGridElement).addClass(EMPTY_QUESTION_CLASS);
                }
            }
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.achievement.grid.question.html',
            scope: {
                question: '=',
                answer: '=',
                users: '='
            },
            link: function(scope, element, attrs, surveyCtrl) {
                scope.btnSurveyLabel = btnSurveyLabel;
                var cfg = {
                    title: btnSurveyLabel.question, 
                    question: scope.question, 
                    answer: scope.answer,
                    config: surveyCtrl.getConfig(),
                    users: surveyCtrl.getUsers(),
                    templateUrl: 'app/templates/btn.survey.dialog.question.html',
                    buttons:[
                        {
                            label : btnSurveyLabel.cancel,
                            class : 'btn btn-default pull-right',
                            action : function(modalInstance) {
                                modalInstance.close(false);
                            }
                        },
                        {
                            label : btnSurveyLabel.save,
                            class : 'btn btn-primary pull-left',
                            action : function(modalInstance) {
                                scope.answer.value = cfg.tmpAnswer.value;
                                scope.answer.$$text = getAnswerText(scope);
                                for (var i = 0; i < scope.$parent.survey.answers.length; i++) {
                                    if (scope.$parent.survey.answers[i].id === scope.answer.id) {
                                        scope.$parent.survey.answers[i].value = scope.answer.value;
                                        break;
                                    }
                                }
                                btnSurveyService.onQuickSave(scope.$parent);
                                modalInstance.close(false);
                            }
                        }
                    ]
                };
                scope.config = surveyCtrl.getConfig();
                scope.onClickQuestion = function(){
                    cfg.tmpAnswer = angular.copy(scope.answer);
                    btnSurveyDialog.custom(cfg);
                };
                scope.answerText = getAnswerText(scope);
                initBorderShadowColor(scope, element);
                scope.$watch(
                    function() {
                        return scope.answer.value;
                    }, function(newValue) {
                        if (newValue){
                            if (scope.answer.required){
                                angular.element(element[0].firstChild).removeClass(REQUIRED_QUESTION_CLASS);
                                angular.element(element[0].firstChild).addClass(FILLED_QUESTION_CLASS);
                            } else {
                                angular.element(element[0].firstChild).removeClass(EMPTY_QUESTION_CLASS);
                                angular.element(element[0].firstChild).addClass(FILLED_QUESTION_CLASS);
                            }
                        } else {
                            if (scope.answer.required){
                                angular.element(element[0].firstChild).removeClass(FILLED_QUESTION_CLASS);
                                angular.element(element[0].firstChild).addClass(REQUIRED_QUESTION_CLASS);
                            } else {
                                angular.element(element[0].firstChild).removeClass(FILLED_QUESTION_CLASS);
                                angular.element(element[0].firstChild).addClass(EMPTY_QUESTION_CLASS);
                            }
                        }
                }, true);
            }
        }
    }]);
