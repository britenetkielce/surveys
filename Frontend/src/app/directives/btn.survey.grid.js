'use strict';

angular.module('btn.survey')
    .directive('btnSurveyGrid',["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            scope: true,
            templateUrl: 'app/templates/btn.survey.grid.html'
        };
}]);
