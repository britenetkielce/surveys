'use strict';

angular.module('btn.survey')
    .directive('btnSurveyList', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.html'
        };
    });
