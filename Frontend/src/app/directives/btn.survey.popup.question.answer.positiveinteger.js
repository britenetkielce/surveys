'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyPopupQuestionAnswerPositiveinteger', ["btnSurveyLabel", function (btnSurveyLabel) {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.popup.question.answer.positiveinteger.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.btnSurveyLabel = btnSurveyLabel,
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        scope.answer.$$text = newVal;
                    }
                );
            }
        }
    }]);
