'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestion', ["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
                scope.questionConfig = btnSurveyQuestionConf.questionType;
                scope.users = surveyCtrl.getUsers();
            }
        }
    }]);
