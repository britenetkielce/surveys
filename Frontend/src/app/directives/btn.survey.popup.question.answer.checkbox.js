'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyPopupQuestionAnswerCheckbox',[ "btnSurveyLabel", "btnSurveyConf", function (btnSurveyLabel,btnSurveyConf) {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.popup.question.answer.checkbox.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.btnSurveyLabel = btnSurveyLabel,
                scope.btnSurveySfdcConf = btnSurveyConf,
                scope.checkedOption = function(value) {
                    if (scope.answer.value == '' || scope.answer.value != value) {
                        scope.answer.value = value;
                    } else if (scope.answer.value == value) {
                        scope.answer.value = '';
                    }
                },
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        var findTheSameValue = false;
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === newVal){
                                scope.answer.$$text = btnSurveyLabel.have;
                                findTheSameValue = true;
                            } else if (!findTheSameValue){
                                scope.answer.$$text = '';
                            }
                        });
                    }
                );
            }
        }
    }]);
