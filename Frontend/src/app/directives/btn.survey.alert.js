'use strict';

angular.module('btn.survey')
    .directive('btnSurveyAlert', ["btnSurveyAlert", "$timeout", "btnSurveyLabel", function (btnSurveyAlert, $timeout, btnSurveyLabel) {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.alert.html',
            link: function (scope) {
                scope.btnSurveyLabel = btnSurveyLabel;
                scope.$watch(
                    function(){
                        return btnSurveyAlert.alert;
                    }, 
                    function(newVal){
                        if (newVal) {
                            scope.alert = newVal;
                            $timeout(function() {
                                scope.alert = null;
                            }, 5000);
                        }
                    }
                );
                scope.closeAlert = function() {
                    scope.alert = null;
                };
            }
        };
    }]);
