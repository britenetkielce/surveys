'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerPositiveinteger', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.positiveinteger.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });
