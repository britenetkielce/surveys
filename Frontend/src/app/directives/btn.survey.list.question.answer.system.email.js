'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerSystemEmail', ["btnSurveyLabel", function (btnSurveyLabel) {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.list.question.answer.system.email.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope) {
                scope.btnSurveyLabel = btnSurveyLabel;
            }
        }
    }]);
