angular.module('btn.survey').directive('btnSurveyGraphMap', ['$location', '$window', 'btnSurveyScriptImporter', 'btnSurveyD3Utils', 'btnSurveyDialog', function ($location, $window, btnSurveyScriptImporter, btnSurveyD3Utils, btnSurveyDialog) {
    var _template = 'app/templates/btn.survey.graph.map.html',
        _addNodes = function(questions, graph){
            _removeNodes(graph);
            _removeLinks(graph);

            angular.forEach(questions, function(question){
                if (question.id === 'root'){
                    graph.addNode(question.id, question.name, {id: question.id, isRootNode: true, posX: 0.5, posY: 0.5});
                }
                else {
                    if (question.type === 'info') {
                        graph.addNode(question.id, question.name, {posX: question.x, posY: question.y, id: question.id, readOnly: question.readOnly, questionData: question, isVisible: true, parentIds: question.parentIds});
                    }
                    else {
                        graph.addNode(question.id, question.name, {posX: question.x, posY: question.y, id: question.id, readOnly: question.readOnly, questionData: question, isVisible: true, parentIds: question.parentIds});
                    }
                }
            });

            angular.forEach(questions, function(question){
                if (question.id !== 'root'){
                    angular.forEach(question.parentIds, function(parentId){
                        //if (question.type === 'info'){
                            graph.addLink(parentId, question.id);
                        //}
                    });
                }
            });
        },
        _hideAllWithoutOneNode = function(node, allNodes, scope){
            angular.forEach(allNodes, function(item){
                if (item !== node){
                    item.isVisible = false;
                    angular.forEach(item.dependent.links, function(link){
                        link.isVisible = false;
                    });
                }
                scope.render();
            });
        },
        _hideChildren = function(parentNode, scope){
            angular.forEach(parentNode.dependent.links, function(link){
                link.isVisible = false;
                link.target.isVisible = false;
            });
            scope.render();
        },
        _setCenterPositionForGraph = function(parentPosition, scope){
            var center = { x: 0, y: 0 };

            if (parentPosition.x >= scope.getGraphSize().w * 0.5 &&  parentPosition.y >= scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x - (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y - (scope.getGraphSize().h * 1.1);
            }
            else if (parentPosition.x < scope.getGraphSize().w * 0.5 &&  parentPosition.y >= scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x + (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y - (scope.getGraphSize().h * 1.1);
            }
            else if (parentPosition.x >= scope.getGraphSize().w * 0.5 &&  parentPosition.y < scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x - (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y + (scope.getGraphSize().h * 1.1);
            }
            else if (parentPosition.x < scope.getGraphSize().w * 0.5 &&  parentPosition.y < scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x + (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y + (scope.getGraphSize().h * 1.1);
            }
            return center;
        },
        _showChildren = function(parentNode, scope){
            angular.forEach(parentNode.dependent.links, function(link){
                link.isVisible = true;
                link.target.isVisible = true;
            });
            scope.render();
        },
        _showQuestionModal = function(d, scope, config){
            var question, answer, cfg;

            angular.forEach(scope.items, function(item){
                if (item.question.id === d.id){
                    question = item.question;
                    answer = item.answer;
                    return;
                }
            });
            cfg = {
                title: 'Question', 
                question: question, 
                answer: answer,
                config: config,
                templateUrl: 'app/templates/btn.survey.dialog.question.html',
                buttons:[
                    {
                        label : 'OK',
                        style : 'btn-primary',
                        action : function(modalInstance) {
                            modalInstance.close(true);
                        }
                    }
                ]
            };
            btnSurveyDialog.custom(cfg);
        },
        _removeLinks = function(graph){
            angular.forEach(graph.getLinks(), function(link){
                graph.removeLink(link.id);
            });
        },
        _removeNodes = function(graph){
            angular.forEach(graph.getNodes(), function(node){
                graph.removeNode(node.id);
            });
        },
        _link = function(scope, element, attr, surveyCtrl) {
            var graph = {},
                _config = surveyCtrl.getConfig(),
                _r = 35,
                _borderWidth = 3,
                _zoom = d3.behavior.zoom().scaleExtent([0.001, 10]),
                _dragStarted = function(d) {
                    d.fixed = true;
                },
                _dragged = function(d) {
                },
                _dragEnded = function(d) {
                    d.fixed = false;
                },
                _transformLink = function(d) {
                    var s = _updatePosition(d.source.posX, d.source.posY),
                        t = _updatePosition(d.target.posX, d.target.posY);

                    d.fixed = true;
                    return 'M' + s.x + ',' + s.y + 'L' + t.x + ',' + t.y;
                },
                _transformLink2 = function(d) {
                    var s = {x:d.source.x, y:d.source.y},
                        t = {x:d.target.x, y:d.target.y};

                    return 'M' + s.x + ',' + s.y + 'L' + t.x + ',' + t.y;
                },
                _transformNode = function(d){
                    var p = _updatePosition(d.posX, d.posY);

                    d.fixed = true;
                    return 'translate(' + p.x + ',' + p.y + ')';
                },
                _transformNode2 = function(d) {
                    var p = {x: d.x, y:d.y};

                    return 'translate(' + p.x + ',' + p.y + ')';
                },
                _updatePosition = function(posX, posY) {
                    var pos = {};

                    if (angular.isUndefined(posX) || angular.isUndefined(posY)){
                        return {x: 0, y: 0}
                    }
                    pos.x = scope.getGraphSize().w * posX,
                    pos.y = scope.getGraphSize().h * posY;
                    return pos;
                };

            scope.centerGraph = function(parentNode){
                var parentPosition = _updatePosition(parentNode.posX, parentNode.posY),
                    center = _setCenterPositionForGraph(parentPosition, scope);

                _zoom.translate([center.x, center.y]);
                graph.svg.attr("transform", "translate(" + center.x + "," + center.y + ")scale(1)");
            };
            scope.getGraphSize = function () {
                var h = angular.isDefined(scope.height) ? scope.height : element.children()[0].clientHeight,
                    w = angular.isDefined(scope.width) ? scope.width : element.children()[0].clientWidth;

                return { 'h': (h < 250 ? 250 : h), 'w': (w < 250 ? 250 : w)};
            };
            scope.home = function(){
                _zoom.scale(1);
                _zoom.translate([0,0]);
                graph.svg.attr("transform", "translate(0,0)scale(1)");
                scope.refresh();
            };
            scope.zoomIn = function(){
                var _scale = _zoom.scale() + 0.1;

                _zoom.scale(_scale);
                _zoom.translate([-0.1*scope.getGraphSize().w,-0.1*scope.getGraphSize().h]);
                graph.svg.attr("transform", "translate(" + -0.1 * scope.getGraphSize().w + "," + -0.1 * scope.getGraphSize().h+ ")scale(" + _scale + ")");
            };
            scope.zoomOut = function(){
                var _scale = _zoom.scale() - 0.1;

                _zoom.scale(_scale);
                _zoom.translate([-0.1*scope.getGraphSize().w,-0.1*scope.getGraphSize().h]);
                graph.svg.attr("transform", "translate(" + -0.1 * scope.getGraphSize().w + "," + -0.1 * scope.getGraphSize().h+ ")scale(" + _scale + ")");
            };
            scope.isReadOnlyMode = scope.readOnlyMode === 'true';
            scope.initialized = false;
            scope.newGraph = function(graph){
                graph.data = btnSurveyD3Utils.createGraphBuilder(scope.fixedNodes);
                scope.$watch('template', function(newVal){
                    if (newVal){
                        _addNodes(scope.template.questions, graph.data);
                    }
                }, true);
                var init = function() {
                    var d3 = window.d3, rootNode = graph.data.getRootNode();

                    btnSurveyD3Utils.init(d3);

                    graph.graphContainer = element.children();
                    graph.graph = graph.graphContainer.children();
                    graph.legendWindow = angular.element(graph.graph.children()[0]);
                    graph.legend = graph.legendWindow.children();
                    graph.contextMenuWindow = angular.element(graph.graph.children()[1]);
                    graph.contextMenu = graph.contextMenuWindow.children();
                    graph.svg = d3.select(graph.graph[0]).append('svg')
                        .attr('width', scope.getGraphSize().w)
                        .attr('height', scope.getGraphSize().h)
                        .append('svg:g')
                            .call(_zoom
                                .on("zoom", function(){
                                        graph.svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                                    }
                                )
                            )
                            .on("dblclick.zoom", null)
                            .on("touchstart.zoom", null)
                            .on("touchend.zoom", null)
                        .append('svg:g')

                    if (angular.isDefined(rootNode)) {
                        rootNode.x = scope.getGraphSize().w * 0.5;
                        rootNode.y = scope.getGraphSize().h * 0.5;
                    }

                    scope.initialized = true;

                    scope.$watch( function() {
                        scope.render();
                    }, true);
                    angular.element(graph.graphContainer).bind('resize', function () {
                        scope.$apply();
                    });
                    angular.element(angular.element($window)).bind('resize', function () {
                        scope.$apply();
                    });

                    scope.render = function() {
                        var force, drag, link, linkLabel, node, nodeIcon, nodeLabel, nodeButton, rootNodeButton;

                        graph.svg.selectAll('.graph-element').remove();
                        if (angular.isUndefined(graph.data)) return;

                        graph.svg
                            .attr('width', scope.getGraphSize().w)
                            .attr('height', scope.getGraphSize().h);

                        if (rootNode && rootNode.x === 0 && rootNode.y === 0) {
                            rootNode.x = scope.getGraphSize().w * 0.5;
                            rootNode.y = scope.getGraphSize().h * 0.5;
                        }

                        force = d3.layout.force()
                            .nodes(d3.values(graph.data.getNodes()))
                            .links(graph.data.getLinks())
                            .size([scope.getGraphSize().w, scope.getGraphSize().h])
                            .linkDistance(150)
                            .charge(-700)
                            .on('tick', function() {
                                node.attr('transform', _transformNode2);
                                link.attr('d', _transformLink2);
                                nodeLabel.attr('transform', _transformNode2);
                                nodeSmallLabel.attr('transform', _transformNode2);
                            })
                            .start();

                        drag = force.drag()
                            .on("dragstart", _dragStarted)
                            .on("drag", _dragged)
                            .on("dragend", _dragEnded);

                        link = graph.svg.selectAll('path')
                            .data(force.links())
                            .enter()
                            .append('path')
                            .attr('class', function() { return 'graph-element graph-link'; })
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })

                        node = graph.svg.selectAll('circle')
                            .data(force.nodes())
                            .enter()
                            .append('circle')
                            .attr('r', _r)
                            .attr('fill', function(d) { return d.isRootNode ? '#eee' : '#fff'; })
                            .attr('stroke', '#000')
                            .attr('stroke-width', _borderWidth)
                            .attr('class', function() { return 'graph-element node-shadow node'; })
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })
                            .on('dblclick', function(d) { 
                                    if (d.questionData.canOpen){
                                        scope.centerGraph(d);
                                        //_showChildren(d, scope); 
                                        _hideAllWithoutOneNode(d, force.nodes(), scope);
                                    }
                                })
                            .on('click', function(d) {
                                    console.log(d);
                                    if (d.questionData.canOpen && d.questionData.type !== 'info'){
                                        _showQuestionModal(d, scope, _config);
                                    }
                                })
                            .call(force.drag);

                        nodeLabel = graph.svg.selectAll('text.node-label')
                            .data(force.nodes())
                            .enter()
                            .append('text')
                            .attr("x", function(d){return -_r/2-_borderWidth;})
                            .attr("y", 0)
                            .attr('class', function(d) { return 'graph-element node-label graph-node-label ' + d.type; })
                            .text(function(d){return d.name;})
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })

                        nodeSmallLabel = graph.svg.selectAll('text.node-small-label')
                            .data(force.nodes())
                            .enter()
                            .append('text')
                            .attr("x", function(d){return -_r/2-_borderWidth;})
                            .attr("y", 15)
                            .attr('class', function(d) { return 'graph-element node-label graph-node-label ' + d.type; })
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })
                    };
                };

                if (scope.initialized) {
                    scope.render();
                } 
                else {
                    init();
                }
            };
            scope.refresh = function() {
                scope.newGraph(graph);
            };
            scope.refresh();
        };
    return {
        restrict : 'EA',
        require: '^btnSurvey',
        scope: {
            height: '@',
            width: '@',
            readOnlyMode: '@',
            survey: '=',
            template: '=',
            items: '='
        },
        templateUrl : _template,
        link: _link
    };
}]);
