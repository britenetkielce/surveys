'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerEmail', [ function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.email.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(){
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === scope.answer.value){
                                if (option.id !== null){
                                    scope.answer.$$text = option.name;
                                } else {
                                    scope.answer.$$text = '';
                                }
                            }
                        });
                    });
            }
        }
    }]);
