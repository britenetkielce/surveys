(function() {
    'use strict';

    angular
        .module('btn.survey')
        .config(config)
        .config(function(tagsInputConfigProvider) {
            tagsInputConfigProvider
                .setActiveInterpolation('tagsInput', { placeholder: true });
        });

    /** @ngInject */
    function config($logProvider) {
    // Enable log
        $logProvider.debugEnabled(true);

    }
})();
