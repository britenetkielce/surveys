(function(angular) {
    'use strict';

    angular
        .module('btn.survey', ['ui.bootstrap', 'ngSanitize', 'ui.select', 'ngProgress', 'ngAnimate', 'ngRoute', 'ngTagsInput']);
})(angular);
