(function() {
    'use strict';

    angular
        .module('btn.survey')
        .config(routerConfig);

        /** @ngInject */
        function routerConfig($routeProvider) {
            var ERROR = 'error',
                cfg = {
                    readOnly: false,
                    canSave: true,
                    canClear: false,
                    canEdit: false,
                    showConfig: true
                };
            $routeProvider
                .when('/', {
                    template: '',
                    controller: ["$scope", "$routeParams", "btnSurveyService", "btnSurveyProgressBar", function($scope, $routeParams, btnSurveyService, btnSurveyProgressBar) {
                        btnSurveyProgressBar.start($scope.$parent.$parent.progressbar);
                        $scope.$parent.cfg = angular.extend(cfg, $scope.$parent.config);
                        btnSurveyService.getAllTemplates({user:  $scope.$parent.user}).then(function(response){
                            $scope.$parent.templates = response;
                            $scope.$parent.selectedTemplate = response[0];
                            btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                        });
                    }]
                })
                .when('/edit/:templateApiName/:surveyId', {
                    template: '',
                    controller: ["$scope", "$routeParams", "btnSurveyService", "btnSurveyProgressBar", function($scope, $routeParams, btnSurveyService, btnSurveyProgressBar) {
                        btnSurveyProgressBar.start($scope.$parent.$parent.progressbar);
                        $scope.$parent.cfg = angular.extend(cfg, $scope.$parent.config);
                        if (!angular.isArray($scope.$parent.templates) || $scope.$parent.templates.length === 0){
                            btnSurveyService.getAllTemplates({user:  $scope.$parent.user}).then(function(response){
                                $scope.$parent.templates = response;
                                for (var i = 0; i < response.length; i++){
                                    if (response[i].templateApiName === $routeParams.templateApiName){
                                        $scope.$parent.selectedTemplate = response[i];
                                        btnSurveyService.getSurvey({userId:  $scope.$parent.user,templateApiName: $routeParams.templateApiName, surveyId: $routeParams.surveyId}).then(function(responseSurvey){
                                            $scope.$parent.survey = responseSurvey;
                                            $scope.$parent.selected = {value:{surveyId: $routeParams.surveyId, name: responseSurvey.name}}
                                            btnSurveyService.editSurvey($scope.$parent.survey, $scope.$parent);
                                            btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                                        });
                                        break;
                                    }
                                }
                            });
                        } else {
                            for (var i = 0; i < $scope.$parent.templates.length; i++){
                                if ($scope.$parent.templates[i].templateApiName === $routeParams.templateApiName){
                                    $scope.$parent.selectedTemplate = $scope.$parent.templates[i];
                                    btnSurveyService.getSurvey({userId:  $scope.$parent.user,templateApiName: $routeParams.templateApiName, surveyId: $routeParams.surveyId}).then(function(responseSurvey){
                                        $scope.$parent.survey = responseSurvey;
                                        $scope.$parent.selected = {value:{surveyId: $routeParams.surveyId, name: responseSurvey.name}}
                                        btnSurveyService.editSurvey($scope.$parent.survey, $scope.$parent);
                                        btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                                    });
                                    break;
                                }
                            }
                        }
                    }]
                })
                .when('/edit/:templateApiName', {
                    template: '',
                    controller: ["$scope", "$routeParams", "btnSurveyService", "btnSurveyProgressBar", function($scope, $routeParams, btnSurveyService, btnSurveyProgressBar) {
                        $scope.$parent.cfg = angular.extend(cfg, $scope.$parent.config);
                        if (!angular.isArray($scope.$parent.templates) || $scope.$parent.templates.length === 0){
                            btnSurveyProgressBar.start($scope.$parent.$parent.progressbar);
                            btnSurveyService.getAllTemplates({user:  $scope.$parent.user}).then(function(response){
                                $scope.$parent.templates = response;
                                for (var i = 0; i < response.length; i++){
                                    if (response[i].templateApiName === $routeParams.templateApiName){
                                        $scope.$parent.selectedTemplate = response[i];
                                        btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                                        break;
                                    }
                                }
                            });
                        }
                    }]
                })
                .when('/view/:templateApiName', {
                    template: '',
                    controller: ["$scope", "$routeParams", "btnSurveyService", "btnSurveyProgressBar", function($scope, $routeParams, btnSurveyService, btnSurveyProgressBar) {
                        $scope.$parent.cfg = angular.extend(cfg, $scope.$parent.config);
                        if (!angular.isArray($scope.$parent.templates) || $scope.$parent.templates.length === 0){
                            btnSurveyProgressBar.start($scope.$parent.$parent.progressbar);
                            btnSurveyService.getAllTemplates({user:  $scope.$parent.user}).then(function(response){
                                $scope.$parent.templates = response;
                                for (var i = 0; i < response.length; i++){
                                    if (response[i].templateApiName === $routeParams.templateApiName){
                                        $scope.$parent.selectedTemplate = response[i];
                                        btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                                        break;
                                    }
                                }
                            });
                        }
                    }]
                })
                .when('/new/:templateApiName', {
                    template: '',
                    controller: ["$scope", "$routeParams", "btnSurveyService", "btnSurveyAlert", "btnSurveyLabel", "btnSurveyProgressBar", function($scope, $routeParams, btnSurveyService, btnSurveyAlert, btnSurveyLabel,btnSurveyProgressBar) {
                        btnSurveyProgressBar.start($scope.$parent.$parent.progressbar);
                        $scope.$parent.cfg = angular.extend(cfg, $scope.$parent.config);
                        if (!angular.isArray($scope.$parent.templates) || $scope.$parent.templates.length === 0){
                            btnSurveyService.getAllTemplates({user:  $scope.$parent.user}).then(function(response){
                                $scope.$parent.templates = response;
                                for (var i = 0; i < response.length; i++){
                                    if (response[i].templateApiName === $routeParams.templateApiName){
                                        $scope.$parent.selectedTemplate = response[i];
                                        if ($scope.$parent.selectedTemplate.multiSurvey || $scope.$parent.selectedTemplate.surveys.length === 0){
                                            btnSurveyService.newSurvey($scope.$parent);
                                        } else {
                                            btnSurveyAlert.addAlert(btnSurveyLabel.cantCreateNewSurvey, ERROR, true);
                                        }
                                        btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                                        break;
                                    }
                                }
                            });
                        } else {
                            for (var i = 0; i < $scope.$parent.templates.length; i++){
                                if ($scope.$parent.templates[i].templateApiName === $routeParams.templateApiName){
                                    $scope.$parent.selectedTemplate = $scope.$parent.templates[i];
                                    if ($scope.$parent.selectedTemplate.multiSurvey || $scope.$parent.selectedTemplate.surveys.length === 0){
                                        btnSurveyService.newSurvey($scope.$parent);
                                    } else {
                                        btnSurveyAlert.addAlert(btnSurveyLabel.cantCreateNewSurvey, ERROR, true);
                                    }
                                    btnSurveyProgressBar.stop($scope.$parent.$parent.progressbar);
                                    break;
                                }
                            }
                        }
                    }]
                })
                .otherwise({ redirectTo: '/' });
        }
})();