(function() {
    'use strict';

    angular
        .module('btn.survey')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log) {
        $log.debug('runBlock start');
        $log.debug('runBlock end');
    }
})();
