(function(angular) {
    'use strict';

    angular
        .module('btn.survey', ['ui.bootstrap' , 'ngSanitize', 'ngProgress', 'ngAnimate', 'ngRoute']);
})(angular);

(function() {
  'use strict';

  angular
    .module('btn.survey')
    .controller('MainMenuController', MainMenuController);

  /** @ngInject */
  function MainMenuController() {
    var vm = this;

    vm.message = "sdasdsada s sas sdafas a sddasdss ss  adasdass as s s s s sss ss ss sss dasdsad sasd sdasdasd";
  }
})();

'use strict';

angular.module('btn.survey')
    .service('btnSurveyService', ["btnSurveyConnectionBs", "btnSurveyProgressBar", "btnSurveyLabel", "btnSurveyAlert", function(btnSurveyConnectionBs, btnSurveyProgressBar, btnSurveyLabel, btnSurveyAlert) {
        const WARNING = 'warning',
              SUCCESS = 'success';
        var validate = function(scope){
                var validated = true,
                    message = btnSurveyLabel.validateMessage + '<br/><ul>',
                    questions = [];

                scope.survey.answers.forEach(
                    function(item){
                        if (!item.value && item.required){
                            questions.push(scope.template.questions.filter(function(v){
                                return v.id === item.questionId;
                            })[0]);
                            validated = false;
                        }
                    }
                );

                questions.forEach(function(question){
                    message += '<li>' + question.name + '</li>';
                });
                message += '</ul>';
                if (!validated){
                    btnSurveyAlert.addAlert(message, WARNING);
                }
                return validated;
            },
            getTemplate = function(scope){
                if (scope.selectedTemplate){
                    var templateData = {
                        templateApiName: scope.selectedTemplate.templateApiName !== undefined ? scope.selectedTemplate.templateApiName : '',
                        userId: scope.user
                    }
                    return btnSurveyConnectionBs.getTemplate(templateData);
                }
                else {
                    return null;
                }
            },
            createSurvey = function(scope){
                if (scope.survey === null) {
                    scope.survey = {};
                }
                scope.showSurvey = true;
            },
            fillSurvey = function(scope){
                var questionsMap = {},
                    answersMap = {},
                    items = [],
                    createAnswer = function(question) {
                        return {
                            questionId: question.id !== undefined ? question.id : '', 
                            value: '', 
                            $$text: '',
                            comment: question.comment !== undefined ? question.comment : '',
                            required: question.required 
                        };
                    };

                if (scope.template == null || scope.survey == null){
                    alert(btnSurveyLabel.surveyServiceAlert);
                    return;
                }

                if (scope.survey.answers == null || scope.survey.templateApiName != scope.template.templateApiName){
                    scope.survey.answers = [];
                }

                scope.survey.templateApiName = scope.template.templateApiName !== undefined ? scope.template.templateApiName : '';
                scope.survey.templateId = scope.template.id !== undefined ? scope.template.id : '';
                scope.survey.userId = scope.user;

                angular.forEach(scope.template.questions, function(question){
                    angular.forEach(question.options, function(option){
                        option.description = question.descriptions[option.id];
                    });
                    questionsMap[question.id] = question;
                });
                angular.forEach(scope.survey.answers, function(answer) {
                    answer.description = questionsMap[answer.questionId].descriptions[answer.value];
                    if (questionsMap[answer.questionId] !== undefined){
                        answer.required = questionsMap[answer.questionId].required;
                        answersMap[answer.questionId] = answer;
                    }
                });
                angular.forEach(questionsMap, function(question, id){
                    if (answersMap[id] === undefined){
                        scope.survey.answers.push(createAnswer(question));
                    }
                });
                angular.forEach(scope.survey.answers, function(answer){
                    var question = questionsMap[answer.questionId];
                    items.push({question:question, answer:answer});
                });
                return items;
            };

        this.createSurvey = createSurvey;
        this.getTemplate = getTemplate;
        this.fillSurvey = fillSurvey;

        this.getAllTemplates = function(scope){
            return btnSurveyConnectionBs.getAllTemplates({userId: scope.user});
        },
        this.onSave = function(scope) {
            if (validate(scope)) {
                btnSurveyProgressBar.start(scope.$parent.progressbar);
                btnSurveyConnectionBs.saveSurvey(scope.survey).then(function(response){
                    if (response){
                        scope.survey = response;
                        scope.items = fillSurvey(scope);
                        btnSurveyAlert.addAlert(btnSurveyLabel.saveOkMessage, SUCCESS, 1000);
                        btnSurveyProgressBar.stop(scope.$parent.progressbar);
                    }
                });
            }
        },
        this.newSurvey = function(scope){
            scope.showSurvey = false;
            getTemplate(scope).then(function(response){
                scope.template = response;
                scope.survey = {};
                scope.$watch('survey', function(newVal){
                    if (newVal){
                        scope.showSurvey = true;
                    }
                });
            });
        },
        this.editSurvey = function(item, scope){
            btnSurveyProgressBar.start(scope.$parent.progressbar);
            scope.showSurvey = false;
            getTemplate(scope).then(function(response){
                scope.template = response;
                var data = {
                    userId: scope.user,
                    templateApiName: response.templateApiName,
                    surveyId: item.surveyId
                };

                btnSurveyConnectionBs.getSurvey(data)
                    .then(function(data){
                        scope.survey = data;
                        createSurvey(scope);
                        btnSurveyProgressBar.stop(scope.$parent.progressbar);
                    });
            });
            
        }
    }]
);

angular.module('btn.survey').factory('btnSurveyScriptImporter', ["$document", "$q", "$rootScope", function($document, $q, $rootScope) {
    var _cache = {},
        _load = function(libSrc) {
            var d, scriptTag, s, onScriptLoad;

            if (angular.isDefined(_cache[libSrc])) {
                return _cache[libSrc];
            }

            onScriptLoad = function () {
                $rootScope.$apply(function() {
                    d.resolve();
                });
            };
            d = $q.defer();
            scriptTag = $document[0].createElement('script');

            scriptTag.type = 'text/javascript'; 
            scriptTag.async = true;
            scriptTag.src = libSrc;
            scriptTag.onreadystatechange = function () {
                if (this.readyState == 'complete') {
                    onScriptLoad();
                }
            };
            scriptTag.onload = onScriptLoad;

            s = $document[0].getElementsByTagName('body')[0];
            s.appendChild(scriptTag);
            _cache[libSrc] = d.promise;
            return _cache[libSrc];
        };

    return {
        load: function(libSrcs) {
            var i, p = [];
            if (angular.isArray(libSrcs)) {
                for (i = 0; i < libSrcs.length; i++) {
                    p.push(_load(libSrcs[i]));
                }
            } else {
                return _load(libSrcs);
            }
            return $q.all(p);
        }
    };
}]);
'use strict';

angular.module('btn.survey')
    .service('btnSurveyProgressBar', [ function() {
        this.start = function(progressBar){
            progressBar.start();
        }

        this.stop = function(progressBar){
            progressBar.set(100);
            progressBar.hide();
        }
    }]
);

/**
 * Angular DialogBox service initialization.
 */
angular.module('btn.survey').factory('btnSurveyDialog', ["$uibModal", "btnSurveyLabel", function($uibModal, btnSurveyLabel) {
    const BTN_PRIMARY = 'btn-primary',
        BTN_DEFAULT = 'btn-default';
    return {
        alert : function(cfg) {
            var _cfg = angular.isObject(cfg) ? angular.extend({}, cfg) : {title:cfg};
            return this.custom({
                title: _cfg.title,
                titleIcon: _cfg.titleIcon,
                message: _cfg.message,
                answer: _cfg.answer,
                buttons:[
                    {
                        label : btnSurveyLabel.OK,
                        style : BTN_PRIMARY,
                        action : function(modalInstance) {
                            modalInstance.close(true);
                        }
                    }
                ]
            });
        },
        confirm : function(cfg) {
            var _cfg = angular.isObject(cfg) ? angular.extend({}, cfg) : {title:cfg};
            return this.custom({
                title:_cfg.title,
                titleIcon: _cfg.titleIcon,
                message:_cfg.message,
                answer: _cfg.answer,
                buttons:[
                    {
                        label : btnSurveyLabel.yes,
                        style : BTN_PRIMARY,
                        action : function(modalInstance) {
                            modalInstance.close(true);
                        }
                    },
                    {
                        label : btnSurveyLabel.no,
                        style : BTN_DEFAULT,
                        action : function(modalInstance) {
                            modalInstance.close(false);
                        }
                    }
                ]
            });
        },
        custom : function(cfg) {
            var _config = angular.extend({
                showMessage: angular.isDefined(cfg.message),
                showTitle: angular.isDefined(cfg.title),
                controller: 'BtnSurveyDialogController',
                templateUrl: 'app/templates/btn.survey.dialog.comment.html',
                controllerAs: 'modalDialogCtrl'
            }, cfg);
            return (cfg._modalInstance = $uibModal.open({
                controllerAs: _config.controllerAs,
                templateUrl : _config.templateUrl,
                windowClass: _config.windowClass,
                controller: _config.controller,
                size: _config.dialogSize,
                resolve: {
                    dialogModel : function() {
                        return _config;
                    }
                }
            })).result;
        }
    };
}]);
angular.module('btn.survey').factory('btnSurveyD3Utils', [ function () {
    var d3;
    return {
        init : function(d3Inst) {
            d3 = d3Inst;
            return this;
        },
        createGraphBuilder : function(fixedNodes) {
            fixedNodes = angular.isDefined(fixedNodes) ? fixedNodes : false;
            return {
                fixedNodes : fixedNodes,
                nodeMap : {},
                linkMap : {},
                addNode : function(id, name, data) {
                    if (angular.isUndefined(this.nodeMap[id])) {
                        this.nodeMap[id] = angular.extend({id:id, name:name, fixed:fixedNodes, weight : 0, isVisible : true, dependent : {nodes : [], links : []}}, angular.isDefined(data) ? data : {});
                    } else {
                        angular.extend(this.nodeMap[id], {name:name}, angular.isDefined(data) ? data : {});
                    }
                    if (this.nodeMap[id].isRootNode) {
                        this.rootNode = this.nodeMap[id];
                    }
                    return this;
                },
                hasNode : function(id) {
                    return angular.isDefined(this.nodeMap[id]);
                },
                addLink : function(node1Id, node2Id, data, forceUpdate) {
                    var id = node1Id + node2Id;
                    if (angular.isUndefined(this.linkMap[id]) && angular.isUndefined(this.linkMap[node2Id + node1Id])) {
                        this.linkMap[id] = angular.extend({id : id, source:this.nodeMap[node1Id], target:this.nodeMap[node2Id], isVisible : true}, angular.isDefined(data) ? data : {});
                        this.nodeMap[node1Id].weight++;
                        this.nodeMap[node2Id].weight++;
                        this.nodeMap[node1Id].dependent.links.push(this.linkMap[id]);
                    } else if (forceUpdate) {
                        if (angular.isDefined(this.linkMap[id])) {
                            angular.extend(this.linkMap[id], angular.isDefined(data) ? data : {});
                        } else {
                            angular.extend(this.linkMap[node2Id + node1Id], angular.isDefined(data) ? data : {});
                        }
                    }
                    return this;
                },
                hideChilds : function(nodeId) {
                    this.changeVisibility(false, nodeId);
                    return this;
                },
                showChilds : function(nodeId) {
                    this.changeVisibility(true, nodeId);
                    return this;
                },
                getRootNode : function() {
                    return this.rootNode;
                },
                changeVisibility : function(visibility, nodeId) {
                    var updateNode = function(node) {
                            if (node.id != nodeId) {
                                node.isVisible = visibility;
                            }
                            angular.forEach(node.dependent.links, function(link) {
                                link.isVisible = visibility;
                            });
                            angular.forEach(node.dependent.nodes, function(node) {
                                if (node.isVisible != visibility) {
                                    updateNode(node, nodeId);
                                }
                            });
                        };
                    if (angular.isDefined(this.nodeMap[nodeId])) {
                        this.nodeMap[nodeId].isOpened = visibility;
                        updateNode(this.nodeMap[nodeId]);
                    }
                },
                getNodes : function() {
                    return this.nodeMap;
                },
                getNode : function(nodeId) {
                    return this.nodeMap[nodeId];
                },
                getLinks : function() {
                    var links = [];
                    angular.forEach(this.linkMap, function(v) {
                        links.push(v);
                    });
                    return links;
                },
                removeNode : function(nodeId){
                    delete this.nodeMap[nodeId];
                },
                removeLink : function(linkId){
                    delete this.linkMap[linkId];
                }
            };
        }
    };
}]);
'use strict';

angular
    .module('btn.survey')
    .service('btnSurveyConnectionSfdc', ["$http", "$q", "$window","btnSurveySfdcConf", "btnSurveyAlert", "btnSurveyLabel", function ($http, $q, $window, btnSurveySfdcConf, btnSurveyAlert, btnSurveyLabel) {
        const ERROR = 'error',
            EXCEPTION = 'exception';
        var    sendRemoteAction = function(action, params) {
                var deferred = $q.defer(),
                    callback = function(result, event) {
                        if (event.type == EXCEPTION && event.message && event.message.indexOf('Logged in?') != -1) {
                            $window.location.reload(); 
                        }
                        if (event.status) {
                            deferred.resolve(result);
                        } 
                        else {
                            btnSurveyAlert.addAlert(event.message+'<br>'+btnSurveyLabel.salesforceMessage, ERROR, true);
                            deferred.resolve(null);
                            return;
                        }
                    };

                    params.unshift(action);
                    params.push(callback);
                    params.push({escape: false});

                    Visualforce.remoting.Manager.invokeAction.apply(Visualforce.remoting.Manager, params);
                    return deferred.promise;
                };

        this.getAllTemplates = function (data) {
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplatesInfo, [data]);
        };

        this.getTemplate = function (data) {
            return getTemplateByApiName(data);
        };

        this.saveSurvey = function (data) {
            var jsonData = angular.toJson(data);
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.saveSurvey, [jsonData])
                .then(function(response){
                    return response;
                });
        };

        this.getSurvey = function(data){
            return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getSurvey, [data]);
        };
    }]);

'use strict';

angular
    .module('btn.survey')
    .service('btnSurveyConnection', ["$http", "$q", "$window","btnSurveySfdcConf", "btnSurveyConf", "btnSurveyAlert", "btnSurveyLabel", function ($http, $q, $window, btnSurveySfdcConf, btnSurveyConf, btnSurveyAlert, btnSurveyLabel) {
        const ERROR = 'error',
            EXCEPTION = 'exception';
        var sendRemoteAction = function(action, params) {
                var deferred = $q.defer(),
                callback = function(result, event) {
                    if (event.type == EXCEPTION && event.message && event.message.indexOf('Logged in?') != -1) {
                        $window.location.reload(); 
                    }
                    if (event.status) {
                        deferred.resolve(result);
                    } 
                    else {
                        btnSurveyAlert.addAlert(event.message+'<br>'+btnSurveyLabel.salesforceMessage, ERROR, true);
                        deferred.resolve(null);
                        return;
                    }
                };

                params.unshift(action);
                params.push(callback);
                params.push({escape: false});

                Visualforce.remoting.Manager.invokeAction.apply(Visualforce.remoting.Manager, params);
                return deferred.promise;
            },
            getTemplateByApiName = function(data){
                var template = {};

                return $http.get(btnSurveyConf.url.surveyTemplate.getTemplate)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getTemplateMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getTemplateByApiName.');
                    })
                    .then(function (response) {
                        angular.forEach(response.data, function(item){
                            if (item.template.templateApiName === data.templateApiName){
                                template = item.template;
                            }
                        });
                        return template;
                    });
            };

        this.getAllTemplates = function (data) {
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplatesInfo, [data]);
            }
            else {
                return $http.get(btnSurveyConf.url.surveyTemplate.getTemplatesInfo)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getAllTemplatesMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getAllTemplates.');
                    })
                    .then(function (response) {
                        return response.data;
                    });
            }
        };

        this.getTemplate = function (data) {
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getTemplate, [data])
                    .then(function(response){
                        return response;
                    });
            }
            else {
                return getTemplateByApiName(data);
            }
        };

        this.saveSurvey = function (data) {
            var jsonData = angular.toJson(data);
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.saveSurvey, [jsonData])
                    .then(function(response){
                        return response;
                    });
            }
            else {
                return $http.get(btnSurveyConf.url.surveyTemplate.saveSurvey)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.saveSurveyMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully saveSurvey.');
                    })
                    .then(function (response) {
                        return response.data;
                    });
            }
        };

        this.getSurvey = function(data){
            if (btnSurveySfdcConf){
                return sendRemoteAction(btnSurveySfdcConf.url.surveyTemplate.getSurvey, [data]);
            }
            else {
                return $http.get(btnSurveyConf.url.surveyTemplate.getSurvey)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getSurveyMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getSurvey.');
                    })
                    .then(function (response) {
                        return response.data;
                    });
            }
            return null;
        };
    }]);

'use strict';

angular
    .module('btn.survey')
    .service('btnSurveyConnectionBs', ["$http", "$q", "$window", "btnSurveyConf", "btnSurveyLabel", "btnSurveyAlert", function ($http, $q, $window, btnSurveyConf, btnSurveyLabel, btnSurveyAlert) {
        const ERROR = 'error';
        var getTemplateByApiName = function(data){
                var template = {};

                return $http.post(btnSurveyConf.url.surveyTemplate.getTemplate, data)
                    .error(function () {
                        btnSurveyAlert.addAlert(btnSurveyLabel.getTemplateMessage, ERROR, true);
                        return;
                    })
                    .success(function () {
                        console.log('#Successfully getTemplateByApiName.');
                    })
                    .then(function (response) {
                        var data = angular.fromJson(response.data);
                        if (angular.isDefined(data[0]) && data[0].errorCode){
                            btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                        } else {
                            return data;
                        }
                    });
            },
            getErrorMessage = function(message){
                return message.substring(message.indexOf('{'), message.indexOf('}')+1);
            };

        this.getAllTemplates = function (data) {
            return $http.post(btnSurveyConf.url.surveyTemplate.getTemplatesInfo, data)
                .error(function () {
                    btnSurveyAlert.addAlert(btnSurveyLabel.getAllTemplatesMessage, ERROR, true);
                    return;
                })
                .success(function () {
                    console.log('#Successfully getAllTemplates.');
                })
                .then(function (response) {
                    var data = angular.fromJson(response.data);
                    if (angular.isDefined(data[0]) && data[0].errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                    } else {
                        return data;
                    }
                });
        };

        this.getTemplate = function (data) {
            return getTemplateByApiName(data);
        };

        this.saveSurvey = function (data) {
            var jsonData = angular.toJson(data);
            return $http.post(btnSurveyConf.url.surveyTemplate.saveSurvey, data)
                .error(function () {
                    btnSurveyAlert.addAlert(btnSurveyLabel.saveSurveyMessage, ERROR, true);
                    return;
                })
                .success(function () {
                    console.log('#Successfully saveSurvey.');
                })
                .then(function (response) {
                    var data = angular.fromJson(response.data);
                    if (angular.isDefined(data[0]) && data[0].errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                    } else {
                        return data;
                    }
                });
        };

        this.getSurvey = function(data){
            return $http.post(btnSurveyConf.url.surveyTemplate.getSurvey, data)
                .error(function () {
                    btnSurveyAlert.addAlert(btnSurveyLabel.getSurveyMessage, ERROR, true);
                    return;
                })
                .success(function () {
                    console.log('#Successfully getSurvey.');
                })
                .then(function (response) {
                    var data = angular.fromJson(response.data);
                    if (angular.isDefined(data[0]) && data[0].errorCode){
                        btnSurveyAlert.addAlert(angular.fromJson(getErrorMessage(data[0].message)).message, ERROR, true);
                    } else {
                        return data;
                    }
                });
        };
    }]);

'use strict';

angular.module('btn.survey')
    .service('btnSurveyAlert', ["$window", function($window) {
        this.alert = {};
        this.addAlert = function(message, type, timeout, closable){
            $window.scrollTo(0, 0);
            this.alert = {
                message: message,
                type: type,
                closable: closable !== undefined ? closable : true
            };
        };
        this.removeAlert = function(){
            this.alert = null;
        }
    }]);

// 'use strict';

// angular.module('btn.survey')
//     .provider('btnSurveyProvider', [ function() {
//         var injector = angular.injector(['btn.survey']);
//         console.log(injector);
//         //var socketio = injector.get('btnSurveyService');

//         this.$get = function() {
//             return 'powinno działać';
//         };
//     }
// ]);
'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyType',["btnSurveyProgressBar","btnSurveyService", "btnSurveyAlert", function (btnSurveyProgressBar, btnSurveyService, btnSurveyAlert) {
        var defaultTemplate = 0,
            initializeTemplate = function(items, defaultTemplate){
                var returnedItem = null;
                if (items[defaultTemplate]){
                    returnedItem = items[defaultTemplate];
                }
                return returnedItem === null ? null : returnedItem;
            },
            hideSurvey = function(scope) {
                scope.showSurvey = false;
            },
            reload = function(scope){
                scope.showSurvey = false;
                scope.templateTmp = btnSurveyService.getTemplate(scope);
                scope.$watch('templateTmp', function(newVal){
                    if (newVal){
                        newVal.then(function(response){
                            scope.template = response;
                        });
                    }
                }, true);
            },
            onClear = function(scope) {
                scope.survey = {};
            },
            cfg = {
                readOnly: false,
                canSave: true,
                canClear: false,
                canEdit: false,
                showConfig: true
            }
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.type.html',
            transclude: true,
            scope: {
                templates: '=',
                survey: '=',
                mode: '=',
                canChangeMode: '=',
                config: '=',
                user: '@'
            },
            link: function(scope) {
                btnSurveyProgressBar.start(scope.$parent.progressbar);
                btnSurveyService.getAllTemplates(scope).then(function(data){
                    scope.templates = data;
                    btnSurveyProgressBar.stop(scope.$parent.progressbar);
                });
                scope.cfg = angular.extend(cfg, scope.config);
                scope.$watch('templates', function(newVal){
                    if (newVal){
                        scope.selectedTemplate = initializeTemplate(newVal, defaultTemplate);
                        reload(scope);
                    }
                }, true);
                scope.reload = function(){
                    reload(scope);
                };
                scope.clear = function(){
                    onClear(scope);
                };
                scope.save = function(){
                    btnSurveyService.onSave(scope);
                };
                scope.newSurvey = function(){
                    btnSurveyService.newSurvey(scope);
                };
                scope.editSurvey = function(item){
                    btnSurveyService.editSurvey(item, scope);
                };
                scope.hideSurvey = function(){
                    hideSurvey(scope);
                };
            }
        }
    }]);
'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestion',["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.questionConfig = btnSurveyQuestionConf.questionType;
                scope.config = surveyCtrl.getConfig();
                scope.users = surveyCtrl.getUsers();
            }
        }
    }]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerText', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.answer.text.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerSelect', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.answer.select.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerPositiveinteger', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.list.question.answer.positiveinteger.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerPicklist', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.answer.picklist.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerPercent', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.list.question.answer.percent.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                if (scope.answer.value === ''){
                    scope.answer.value = 0;
                    scope.value = {
                        percent: 0
                    };
                } else {
                    scope.value = {
                        percent: scope.answer.value * 100
                    }
                }
                scope.$watch(
                    function(){
                        return scope.value.percent;
                    }, function(newVal){
                        scope.answer.value = newVal / 100;
                        scope.answer.$$text = newVal+'%';
                    },
                    true
                );
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerInfo', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.question.answer.info.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyListQuestionAnswerEmail', [ function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.email.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    }]);

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyList', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.list.html'
        };
    });

'use strict';

angular.module('btn.survey')
    .directive('btnSurvey',["btnSurveyService", "btnSurveyLabel", function (btnSurveyService, btnSurveyLabel) {
        var modes = [{id: 'list', name: btnSurveyLabel.list}, {id: 'grid', name: btnSurveyLabel.grid}, {id: 'graph', name: btnSurveyLabel.graph}, {id: 'smallGrid', name: btnSurveyLabel.smallGrid}, {id: 'achievementGrid', name: btnSurveyLabel.achievementGrid}],
            defaulMode = modes[0],
            initializeMode = function (items, mode) {
                var returnedItem = null;
                angular.forEach(items, function (item) {
                    if (item.id === mode && returnedItem === null) {
                        returnedItem = item;
                    }
                });
                return returnedItem === null ? defaulMode : returnedItem;
            },
            showConfigurationPanel = function(){
                var display = angular.element('.configuration-panel').css('display');

                if (display === 'none'){
                    angular.element('.configuration-panel').css({display: 'block'});
                }
                else {
                    angular.element('.configuration-panel').css({display: 'none'});
                }
            };
        return {
            restrict: 'EA',
            scope: {
                mode: '=',
                canChangeMode: '=',
                config: '=',
                survey: '=',
                template: '=',
                user: '=',
                users: '=',
                save: '&',
                clear: '&',
                edit: '&'
            },
            controller: ['$scope', function($scope){
                var conf = $scope.config;
                $scope.items = btnSurveyService.fillSurvey($scope);
                this.getConfig = function(){
                    return conf;
                };
                this.setConfig = function(extendConfig){
                    conf = angular.extend(conf, extendConfig);
                };
                this.geTemplate = function(){
                    return $scope.template;
                };
                this.getUsers = function() {
                    return $scope.users;
                }
                $scope.modes = modes;
                $scope.selectedMode = initializeMode(modes, $scope.mode);
            }],
            link: function (scope, element, attrs, controllers) {
                scope.cfg = controllers.getConfig();
                scope.showConfigurationPanel = function(){
                    showConfigurationPanel();
                };
            },
            templateUrl: 'app/templates/btn.survey.html'
        };
    }]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestion', ["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
                scope.questionConfig = btnSurveyQuestionConf.questionType;
                scope.users = surveyCtrl.getUsers();
            }
        }
    }]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerText', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.text.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerSelect', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.select.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerPositiveinteger', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.positiveinteger.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerPicklist', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.picklist.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerPercent', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.percent.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.value = {
                    percent: scope.answer.value * 100
                };
                if (scope.answer.value === ''){
                    scope.answer.value = 0;
                    scope.value.percent = 0;
                }
                scope.$watch(
                    function(){
                        return scope.value.percent;
                    }, function(newVal){
                        scope.answer.value = newVal / 100;
                        scope.answer.$$text = newVal+'%';
                    },
                    true
                );
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerInfo', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.grid.question.answer.info.html',
            scope: {
                question: '=',
                answer: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                scope.config = surveyCtrl.getConfig();
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGridQuestionAnswerEmail', [ function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.email.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    }]);

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyGrid',["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            scope: true,
            templateUrl: 'app/templates/btn.survey.grid.html'
        };
}]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestion', ["btnSurveyDialog", "btnSurveyLabel", function (btnSurveyDialog, btnSurveyLabel) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.html',
            scope: {
                question: '=',
                answer: '=',
                users: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                var cfg = {
                    title: btnSurveyLabel.question, 
                    question: scope.question, 
                    answer: scope.answer,
                    config: surveyCtrl.getConfig(),
                    users: surveyCtrl.getUsers(),
                    templateUrl: 'app/templates/btn.survey.dialog.question.html',
                    buttons:[
                        {
                            label : btnSurveyLabel.OK,
                            style : 'btn-primary',
                            action : function(modalInstance) {
                                modalInstance.close(true);
                            }
                        }
                    ]
                };
                scope.config = surveyCtrl.getConfig();
                scope.onClickQuestion = function(){
                    btnSurveyDialog.custom(cfg);
                }
            }
        }
    }]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerText', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.text.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        scope.answer.$$text = newVal;
                    }
                );
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerSelect', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.select.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        var findTheSameValue = false;
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === newVal){
                                scope.answer.$$text = option.name;
                                findTheSameValue = true;
                            } else if (!findTheSameValue){
                                scope.answer.$$text = '';
                            }
                        });
                    });
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerPositiveinteger', [ function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.positiveinteger.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(newVal){
                        scope.answer.$$text = newVal;
                    }
                );
            }
        }
    }]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerPicklist', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.picklist.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(){
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === scope.answer.value){
                                if (option.id !== null){
                                    scope.answer.$$text = option.name;
                                } else {
                                    scope.answer.$$text = '';
                                }
                            }
                        });
                    });
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerPercent', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.percent.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.value = {
                    percent: scope.answer.value * 100
                };
                if (scope.answer.value === ''){
                    scope.answer.value = 0;
                    scope.value.percent = 0;
                }
                scope.$watch(
                    function(){
                        return scope.value.percent;
                    }, function(newVal){
                        scope.answer.value = newVal / 100;
                        scope.answer.$$text = newVal+'%';
                    },
                    true
                );
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerInfo', function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.info.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            }
        }
    });

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyGraphModalWindowQuestionAnswerEmail', [ function () {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.graph.modal.window.question.answer.email.html',
            scope: {
                question: '=',
                answer: '=',
                config: '='
            },
            link: function(scope){
                scope.$watch(
                    function(){
                        return scope.answer.value;
                    },
                    function(){
                        angular.forEach(scope.question.options, function(option){
                            if (option.id === scope.answer.value){
                                if (option.id !== null){
                                    scope.answer.$$text = option.name;
                                } else {
                                    scope.answer.$$text = '';
                                }
                            }
                        });
                    });
            }
        }
    }]);

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyGraphModalWindow', ["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            scope: true,
            templateUrl: 'app/templates/btn.survey.graph.modal.window.html'
        };
}]);
angular.module('btn.survey').directive('btnSurveyGraphMap', ['$location', '$window', 'btnSurveyScriptImporter', 'btnSurveyD3Utils', 'btnSurveyDialog', function ($location, $window, btnSurveyScriptImporter, btnSurveyD3Utils, btnSurveyDialog) {
    var _template = 'app/templates/btn.survey.graph.map.html',
        _addNodes = function(questions, graph){
            _removeNodes(graph);
            _removeLinks(graph);

            angular.forEach(questions, function(question){
                if (question.id === 'root'){
                    graph.addNode(question.id, question.name, {id: question.id, isRootNode: true, posX: 0.5, posY: 0.5});
                }
                else {
                    if (question.type === 'info') {
                        graph.addNode(question.id, question.name, {posX: question.x, posY: question.y, id: question.id, readOnly: question.readOnly, questionData: question, isVisible: true, parentIds: question.parentIds});
                    }
                    else {
                        graph.addNode(question.id, question.name, {posX: question.x, posY: question.y, id: question.id, readOnly: question.readOnly, questionData: question, isVisible: true, parentIds: question.parentIds});
                    }
                }
            });

            angular.forEach(questions, function(question){
                if (question.id !== 'root'){
                    angular.forEach(question.parentIds, function(parentId){
                        //if (question.type === 'info'){
                            graph.addLink(parentId, question.id);
                        //}
                    });
                }
            });
        },
        _hideAllWithoutOneNode = function(node, allNodes, scope){
            angular.forEach(allNodes, function(item){
                if (item !== node){
                    item.isVisible = false;
                    angular.forEach(item.dependent.links, function(link){
                        link.isVisible = false;
                    });
                }
                scope.render();
            });
        },
        _hideChildren = function(parentNode, scope){
            angular.forEach(parentNode.dependent.links, function(link){
                link.isVisible = false;
                link.target.isVisible = false;
            });
            scope.render();
        },
        _setCenterPositionForGraph = function(parentPosition, scope){
            var center = { x: 0, y: 0 };

            if (parentPosition.x >= scope.getGraphSize().w * 0.5 &&  parentPosition.y >= scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x - (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y - (scope.getGraphSize().h * 1.1);
            }
            else if (parentPosition.x < scope.getGraphSize().w * 0.5 &&  parentPosition.y >= scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x + (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y - (scope.getGraphSize().h * 1.1);
            }
            else if (parentPosition.x >= scope.getGraphSize().w * 0.5 &&  parentPosition.y < scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x - (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y + (scope.getGraphSize().h * 1.1);
            }
            else if (parentPosition.x < scope.getGraphSize().w * 0.5 &&  parentPosition.y < scope.getGraphSize().h * 0.5){
                center.x = parentPosition.x + (scope.getGraphSize().w * 1.1);
                center.y = parentPosition.y + (scope.getGraphSize().h * 1.1);
            }
            return center;
        },
        _showChildren = function(parentNode, scope){
            angular.forEach(parentNode.dependent.links, function(link){
                link.isVisible = true;
                link.target.isVisible = true;
            });
            scope.render();
        },
        _showQuestionModal = function(d, scope, config){
            var question, answer, cfg;

            angular.forEach(scope.items, function(item){
                if (item.question.id === d.id){
                    question = item.question;
                    answer = item.answer;
                    return;
                }
            });
            cfg = {
                title: 'Question', 
                question: question, 
                answer: answer,
                config: config,
                templateUrl: 'app/templates/btn.survey.dialog.question.html',
                buttons:[
                    {
                        label : 'OK',
                        style : 'btn-primary',
                        action : function(modalInstance) {
                            modalInstance.close(true);
                        }
                    }
                ]
            };
            btnSurveyDialog.custom(cfg);
        },
        _removeLinks = function(graph){
            angular.forEach(graph.getLinks(), function(link){
                graph.removeLink(link.id);
            });
        },
        _removeNodes = function(graph){
            angular.forEach(graph.getNodes(), function(node){
                graph.removeNode(node.id);
            });
        },
        _link = function(scope, element, attr, surveyCtrl) {
            var graph = {},
                _config = surveyCtrl.getConfig(),
                _r = 35,
                _borderWidth = 3,
                _zoom = d3.behavior.zoom().scaleExtent([0.001, 10]),
                _dragStarted = function(d) {
                    d.fixed = true;
                },
                _dragged = function(d) {
                },
                _dragEnded = function(d) {
                    d.fixed = false;
                },
                _transformLink = function(d) {
                    var s = _updatePosition(d.source.posX, d.source.posY),
                        t = _updatePosition(d.target.posX, d.target.posY);

                    d.fixed = true;
                    return 'M' + s.x + ',' + s.y + 'L' + t.x + ',' + t.y;
                },
                _transformLink2 = function(d) {
                    var s = {x:d.source.x, y:d.source.y},
                        t = {x:d.target.x, y:d.target.y};

                    return 'M' + s.x + ',' + s.y + 'L' + t.x + ',' + t.y;
                },
                _transformNode = function(d){
                    var p = _updatePosition(d.posX, d.posY);

                    d.fixed = true;
                    return 'translate(' + p.x + ',' + p.y + ')';
                },
                _transformNode2 = function(d) {
                    var p = {x: d.x, y:d.y};

                    return 'translate(' + p.x + ',' + p.y + ')';
                },
                _updatePosition = function(posX, posY) {
                    var pos = {};

                    if (angular.isUndefined(posX) || angular.isUndefined(posY)){
                        return {x: 0, y: 0}
                    }
                    pos.x = scope.getGraphSize().w * posX,
                    pos.y = scope.getGraphSize().h * posY;
                    return pos;
                };

            scope.centerGraph = function(parentNode){
                var parentPosition = _updatePosition(parentNode.posX, parentNode.posY),
                    center = _setCenterPositionForGraph(parentPosition, scope);

                _zoom.translate([center.x, center.y]);
                graph.svg.attr("transform", "translate(" + center.x + "," + center.y + ")scale(1)");
            };
            scope.getGraphSize = function () {
                var h = angular.isDefined(scope.height) ? scope.height : element.children()[0].clientHeight,
                    w = angular.isDefined(scope.width) ? scope.width : element.children()[0].clientWidth;

                return { 'h': (h < 250 ? 250 : h), 'w': (w < 250 ? 250 : w)};
            };
            scope.home = function(){
                _zoom.scale(1);
                _zoom.translate([0,0]);
                graph.svg.attr("transform", "translate(0,0)scale(1)");
                scope.refresh();
            };
            scope.zoomIn = function(){
                var _scale = _zoom.scale() + 0.1;

                _zoom.scale(_scale);
                _zoom.translate([-0.1*scope.getGraphSize().w,-0.1*scope.getGraphSize().h]);
                graph.svg.attr("transform", "translate(" + -0.1 * scope.getGraphSize().w + "," + -0.1 * scope.getGraphSize().h+ ")scale(" + _scale + ")");
            };
            scope.zoomOut = function(){
                var _scale = _zoom.scale() - 0.1;

                _zoom.scale(_scale);
                _zoom.translate([-0.1*scope.getGraphSize().w,-0.1*scope.getGraphSize().h]);
                graph.svg.attr("transform", "translate(" + -0.1 * scope.getGraphSize().w + "," + -0.1 * scope.getGraphSize().h+ ")scale(" + _scale + ")");
            };
            scope.isReadOnlyMode = scope.readOnlyMode === 'true';
            scope.initialized = false;
            scope.newGraph = function(graph){
                graph.data = btnSurveyD3Utils.createGraphBuilder(scope.fixedNodes);
                scope.$watch('template', function(newVal){
                    if (newVal){
                        _addNodes(scope.template.questions, graph.data);
                    }
                }, true);
                var init = function() {
                    var d3 = window.d3, rootNode = graph.data.getRootNode();

                    btnSurveyD3Utils.init(d3);

                    graph.graphContainer = element.children();
                    graph.graph = graph.graphContainer.children();
                    graph.legendWindow = angular.element(graph.graph.children()[0]);
                    graph.legend = graph.legendWindow.children();
                    graph.contextMenuWindow = angular.element(graph.graph.children()[1]);
                    graph.contextMenu = graph.contextMenuWindow.children();
                    graph.svg = d3.select(graph.graph[0]).append('svg')
                        .attr('width', scope.getGraphSize().w)
                        .attr('height', scope.getGraphSize().h)
                        .append('svg:g')
                            .call(_zoom
                                .on("zoom", function(){
                                        graph.svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                                    }
                                )
                            )
                            .on("dblclick.zoom", null)
                            .on("touchstart.zoom", null)
                            .on("touchend.zoom", null)
                        .append('svg:g')

                    if (angular.isDefined(rootNode)) {
                        rootNode.x = scope.getGraphSize().w * 0.5;
                        rootNode.y = scope.getGraphSize().h * 0.5;
                    }

                    scope.initialized = true;

                    scope.$watch( function() {
                        scope.render();
                    }, true);
                    angular.element(graph.graphContainer).bind('resize', function () {
                        scope.$apply();
                    });
                    angular.element(angular.element($window)).bind('resize', function () {
                        scope.$apply();
                    });

                    scope.render = function() {
                        var force, drag, link, linkLabel, node, nodeIcon, nodeLabel, nodeButton, rootNodeButton;

                        graph.svg.selectAll('.graph-element').remove();
                        if (angular.isUndefined(graph.data)) return;

                        graph.svg
                            .attr('width', scope.getGraphSize().w)
                            .attr('height', scope.getGraphSize().h);

                        if (rootNode && rootNode.x === 0 && rootNode.y === 0) {
                            rootNode.x = scope.getGraphSize().w * 0.5;
                            rootNode.y = scope.getGraphSize().h * 0.5;
                        }

                        force = d3.layout.force()
                            .nodes(d3.values(graph.data.getNodes()))
                            .links(graph.data.getLinks())
                            .size([scope.getGraphSize().w, scope.getGraphSize().h])
                            .linkDistance(150)
                            .charge(-700)
                            .on('tick', function() {
                                node.attr('transform', _transformNode2);
                                link.attr('d', _transformLink2);
                                nodeLabel.attr('transform', _transformNode2);
                                nodeSmallLabel.attr('transform', _transformNode2);
                            })
                            .start();

                        drag = force.drag()
                            .on("dragstart", _dragStarted)
                            .on("drag", _dragged)
                            .on("dragend", _dragEnded);

                        link = graph.svg.selectAll('path')
                            .data(force.links())
                            .enter()
                            .append('path')
                            .attr('class', function() { return 'graph-element graph-link'; })
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })

                        node = graph.svg.selectAll('circle')
                            .data(force.nodes())
                            .enter()
                            .append('circle')
                            .attr('r', _r)
                            .attr('fill', function(d) { return d.isRootNode ? '#eee' : '#fff'; })
                            .attr('stroke', '#000')
                            .attr('stroke-width', _borderWidth)
                            .attr('class', function() { return 'graph-element node-shadow node'; })
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })
                            .on('dblclick', function(d) { 
                                    if (d.questionData.canOpen){
                                        scope.centerGraph(d);
                                        //_showChildren(d, scope); 
                                        _hideAllWithoutOneNode(d, force.nodes(), scope);
                                    }
                                })
                            .on('click', function(d) {
                                    console.log(d);
                                    if (d.questionData.canOpen && d.questionData.type !== 'info'){
                                        _showQuestionModal(d, scope, _config);
                                    }
                                })
                            .call(force.drag);

                        nodeLabel = graph.svg.selectAll('text.node-label')
                            .data(force.nodes())
                            .enter()
                            .append('text')
                            .attr("x", function(d){return -_r/2-_borderWidth;})
                            .attr("y", 0)
                            .attr('class', function(d) { return 'graph-element node-label graph-node-label ' + d.type; })
                            .text(function(d){return d.name;})
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })

                        nodeSmallLabel = graph.svg.selectAll('text.node-small-label')
                            .data(force.nodes())
                            .enter()
                            .append('text')
                            .attr("x", function(d){return -_r/2-_borderWidth;})
                            .attr("y", 15)
                            .attr('class', function(d) { return 'graph-element node-label graph-node-label ' + d.type; })
                            .style("display", function(d) { return d.isVisible ? 'block' : 'none'; })
                    };
                };

                if (scope.initialized) {
                    scope.render();
                } 
                else {
                    init();
                }
            };
            scope.refresh = function() {
                scope.newGraph(graph);
            };
            scope.refresh();
        };
    return {
        restrict : 'EA',
        require: '^btnSurvey',
        scope: {
            height: '@',
            width: '@',
            readOnlyMode: '@',
            survey: '=',
            template: '=',
            items: '='
        },
        templateUrl : _template,
        link: _link
    };
}]);

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyGraph', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.graph.html'
        };
    });

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyComment',["btnSurveyDialog", "btnSurveyLabel", function (btnSurveyDialog, btnSurveyLabel) {
        return {
            restrict: 'E',
            scope: {
                answer: '='
            },
            link: function (scope) {
                scope.onPostComment = function(){
                    btnSurveyDialog.alert({title: btnSurveyLabel.comment, message:btnSurveyLabel.commentMessage, answer: scope.answer});
                }
            },
            templateUrl: 'app/templates/btn.survey.comment.html'
        };
    }]);

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyButtons', function () {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            link: function (scope, element, attrs, surveyCtrl) {
                scope.config = surveyCtrl.getConfig();
            },
            templateUrl: 'app/templates/btn.survey.buttons.html'
        };
    });

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyAlert', ["btnSurveyAlert", "$timeout", function (btnSurveyAlert, $timeout) {
        return {
            restrict: 'E',
            templateUrl: 'app/templates/btn.survey.alert.html',
            link: function (scope) {
                scope.$watch(
                    function(){
                        return btnSurveyAlert.alert;
                    }, 
                    function(newVal){
                        if (newVal) {
                            scope.alert = newVal;
                            $timeout(function() {
                                scope.alert = null;
                            }, 5000);
                        }
                    }
                );
                scope.closeAlert = function() {
                    scope.alert = null;
                };
            }
        };
    }]);

'use strict';

angular
    .module('btn.survey')
    .directive('btnSurveyAchievementGridQuestion', ["btnSurveyQuestionConf", "btnSurveyDialog", "btnSurveyLabel", function (btnSurveyQuestionConf, btnSurveyDialog, btnSurveyLabel) {
        const EMAIL = 'email',
            SELECT = 'select',
            PICKLIST = 'picklist',
            PERCENT = 'percent',
            FILLED_QUESTION_CLASS = 'filled-question',
            EMPTY_QUESTION_CLASS = 'empty-question';
        var questionGridElement = document.getElementsByClassName("question-grid-element"),
            getAnswerText = function(scope){
                if (scope.question.type === EMAIL || scope.question.type === SELECT || scope.question.type === PICKLIST){
                    angular.forEach(scope.question.options, function(option){
                        if (option.id === scope.answer.value && option.id !== null){
                            scope.answer.$$text = option.name;
                        }
                    });
                    if (!scope.question.options){
                        scope.answer.$$text = scope.answer.value;
                    }
                    return scope.answer.$$text;
                } else if (scope.question.type === PERCENT){
                    return scope.answer.$$text = (scope.answer.value * 100) + '%';
                } else {
                    return angular.isObject(scope.answer.value) ? scope.answer.$$text = "" : scope.answer.$$text = scope.answer.value;
                }
            },
            initBorderShadowColor = function(scope, element){
                if (scope.answer.value){
                    angular.element(questionGridElement).addClass(FILLED_QUESTION_CLASS);
                } else {
                    angular.element(questionGridElement).addClass(EMPTY_QUESTION_CLASS);
                }
            }
        return {
            restrict: 'E',
            require: '^btnSurvey',
            templateUrl: 'app/templates/btn.survey.achievement.grid.question.html',
            scope: {
                question: '=',
                answer: '=',
                users: '='
            },
            link: function(scope, element, attrs, surveyCtrl){
                var cfg = {
                    title: btnSurveyLabel.question, 
                    question: scope.question, 
                    answer: scope.answer,
                    config: surveyCtrl.getConfig(),
                    users: surveyCtrl.getUsers(),
                    templateUrl: 'app/templates/btn.survey.dialog.question.html',
                    buttons:[
                        {
                            label : btnSurveyLabel.OK,
                            style : 'btn-primary',
                            action : function(modalInstance) {
                                modalInstance.close(true);
                            }
                        }
                    ]
                };
                scope.config = surveyCtrl.getConfig();
                scope.onClickQuestion = function(){
                    btnSurveyDialog.custom(cfg);
                };
                scope.answerText = getAnswerText(scope);
                initBorderShadowColor(scope, element);
                scope.$watch(
                    function() {
                        return scope.answer.value;
                    }, function(newValue) {
                        if (newValue){
                            angular.element(element[0].firstChild).removeClass(EMPTY_QUESTION_CLASS);
                            angular.element(element[0].firstChild).addClass(FILLED_QUESTION_CLASS);
                        } else {
                            angular.element(element[0].firstChild).removeClass(FILLED_QUESTION_CLASS);
                            angular.element(element[0].firstChild).addClass(EMPTY_QUESTION_CLASS);
                        }
                }, true);
            }
        }
    }]);

'use strict';

angular.module('btn.survey')
    .directive('btnSurveyAchievementGrid', ["btnSurveyQuestionConf", function (btnSurveyQuestionConf) {
        return {
            restrict: 'E',
            require: '^btnSurvey',
            scope: true,
            templateUrl: 'app/templates/btn.survey.achievement.grid.html'
        };
}]);
'use strict';

angular.module('btn.survey')
    .controller('MainController', ['$scope', 'btnSurveyConnection', 'ngProgressFactory', function($scope, btnSurveyConnection, ngProgressFactory) {
        $scope.survey = {};
        $scope.template = {};
        $scope.templates = {};
        $scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.setParent(document.getElementById('main-container'));
        $scope.progressbar.setColor('#337ab7');
        $scope.progressbar.setHeight('4px');
        angular.element($scope.progressbar.getDomElement()).addClass('container');
}]);
/**
 * Angular custom dialog window controller initialization.
 */
angular.module('btn.survey').controller('BtnSurveyDialogController', ['$scope', '$uibModalInstance', 'dialogModel', function($scope, $uibModalInstance, dialogModel) {
    
    this.value = null;
    this.dialogModel = dialogModel;
    this.modalInstance = $uibModalInstance;
     
    this.close = function() {
        $uibModalInstance.dismiss(false);
    };

    this.selectValue = function(value) {
        $uibModalInstance.close(value);
    };

    if (angular.isFunction(this.dialogModel.init)) {
        this.dialogModel.init();
    }
    
    $scope.getFieldValue = function(fieldName, record) {
        if (fieldName.indexOf('.') > 0) {
            angular.forEach(fieldName.split('.'), function(v) {
                if (angular.isDefined(record)) {
                    record = record[v];
                }
            });
            return record;
        }
        return record[fieldName];
    };
}]);
(function() {
    'use strict';

    runBlock.$inject = ["$log"];
    angular
        .module('btn.survey')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log) {
        $log.debug('runBlock start');
        $log.debug('runBlock end');
    }
})();

(function() {
    'use strict';

        routerConfig.$inject = ["$routeProvider", "$locationProvider", "$provide", "$injector"];
    angular
        .module('btn.survey')
        .config(routerConfig);

        /** @ngInject */
        function routerConfig($routeProvider, $locationProvider, $provide, $injector) {
            //console.log($injector);
            //console.log($injector.instantiate('btnSurveyService'));
            //$locationProvider.html5Mode(true);
        //     console.log($injector);
        //     $routeProvider
        //         .when('/edit/:templateApiName/:surveyId', {
        //             template: '',
        //             controller: function($scope, $routeParams) {
        //                 console.log('edit/'+$routeParams.templateApiName+'/'+$routeParams.surveyId);
        //                 console.log($scope);
        //                 var item = {
        //                     surveyId: $routeParams.surveyId
        //                 };
        //                 //console.log(btnSurveyService);
        //             }
        //         })
        //         .when('/new/:templateApiName', {
        //             template: '',
        //             controller: function($scope, $routeParams) {
        //                 console.log('new/'+$routeParams.templateApiName);
        //             }
        //         })
        //         .when('/template/:templateApiName', {
        //             template: '',
        //             controller: function($scope, $routeParams) {
        //                 console.log('template/'+$routeParams.templateApiName);
        //             }
        //         })
        //         .when('/survey/:surveyId', {
        //             template: '',
        //             controller: function($scope, $routeParams) {
        //                 console.log('survey/'+$routeParams.surveyId);
        //             }
        //         });
        }
})();
(function() {
    'use strict';

    angular
        .module('btn.survey');
        //.constant('constName', false)

})();

(function() {
    'use strict';

    config.$inject = ["$logProvider"];
    angular
        .module('btn.survey')
        .config(config);

    /** @ngInject */
    function config($logProvider) {
    // Enable log
        $logProvider.debugEnabled(true);

    }
})();

'use strict';

angular
    .module('btn.survey')
    .constant('btnSurveyQuestionConf',{
        questionType : {
            error: true,
            info: true,
            text: true,
            select: true,
            picklist: true,
            percent: true,
            positiveInteger: true,
            email: true
        }
});


'use strict';

angular
    .module('btn.survey')
    .constant('btnSurveyLabel', {
        OK: 'OK',
        yes: 'Yes',
        no: 'No',
        comment: 'Comment',
        commentMessage: 'Type your attention to the question.',
        question: 'Question',
        list: 'List',
        grid: 'Grid',
        graph: 'Graph',
        smallGrid: 'Small grid with modal answers',
        achievementGrid: 'Achievement Grid',
        validateMessage: 'You did not answer all the required questions!',
        saveOkMessage: 'Save OK!',
        getTemplateMessage: 'We have problem with getting selected template. Please contact with your administrator!',
        getAllTemplatesMessage: 'We have problem with getting set of your templates. Please contact with your administrator!',
        saveSurveyMessage: 'We have problem with saving survey. Please contact with your administrator!',
        getSurveyMessage: 'We have problem with getting survey. Please contact with your administrator!',
        salesforceMessage: 'Please contact with your administrator!',
        surveyServiceAlert: 'Template or Survey cannot be null!!!'
});
'use strict';

angular
    .module('btn.survey')
    .constant('btnSurveyConf',{
        url : {
            surveyTemplate : {
                getTemplatesInfo: 'app/mocks/questionsMap.json',
                getTemplate: 'app/mocks/extendentQuestion.json',
                getSurvey: '',
                saveSurvey: '',
                getUsers: 'app/mocks/contacts.json'
            }
        }
});


angular.module("btn.survey").run(["$templateCache", function($templateCache) {$templateCache.put("app/templates/btn.survey.achievement.grid.html","<div class=\"panel panel-default\"><div class=\"panel-heading\" ng-bind-html=\"template.name\"></div><div class=\"panel-body\"><small ng-bind=\"template.description\"></small><div class=\"wrapper text-center button-block-padding-bottom row\"><btn-survey-buttons></btn-survey-buttons></div><input type=\"search\" ng-model=\"name\" placeholder=\"Find...\" aria-label=\"Find...\" class=\"form-control input-search\"><ul class=\"row flex-container list-serach\"><div ng-repeat=\"item in items | orderBy: \'question.order\' | filter:name as results\" class=\"col-xs-12 col-sm-6 col-md-3 col-lg-2 slide-left\" ng-animate=\"\'animate\'\"><btn-survey-achievement-grid-question answer=\"item.answer\" question=\"item.question\"></btn-survey-achievement-grid-question></div></ul><div class=\"wrapper text-center row\"><btn-survey-buttons></btn-survey-buttons></div></div></div>");
$templateCache.put("app/templates/btn.survey.achievement.grid.question.html","<li class=\"list-serach-element\"><div id=\"{{answer.questionId}}\" class=\"question-padding-15 panel panel-success achievement-question question-grid-element\" ng-click=\"onClickQuestion()\"><div class=\"question-padding-bottom small-grid-question-tex achievement-question\" ng-bind-html=\"question.name\"></div><div class=\"question-padding-bottom small-grid-question-text achievement-question\"><small ng-bind-html=\"answer.$$text\"></small></div></div></li>");
$templateCache.put("app/templates/btn.survey.alert.html","<div ng-if=\"alert\" class=\"slide-down\" ng-animate=\"\'animate\'\"><div ng-if=\"alert.type == \'error\'\"><div class=\"alert alert-danger fade in\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" ng-click=\"closeAlert()\" ng-if=\"alert.closable\"><span aria-hidden=\"true\">&times;</span></button> <strong>Error!</strong>&nbsp;<span ng-bind-html=\"alert.message\"></span></div></div><div ng-if=\"alert.type == \'success\'\"><div class=\"alert alert-success fade in\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" ng-click=\"closeAlert()\" ng-if=\"alert.closable\"><span aria-hidden=\"true\">&times;</span></button> <strong>Success!</strong>&nbsp;<span ng-bind-html=\"alert.message\"></span></div></div><div ng-if=\"alert.type == \'warning\'\"><div class=\"alert alert-warning fade in\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" ng-click=\"closeAlert()\" ng-if=\"alert.closable\"><span aria-hidden=\"true\">&times;</span></button> <strong>Warning!</strong>&nbsp;<span ng-bind-html=\"alert.message\"></span></div></div><div ng-if=\"alert.type == \'info\'\"><div class=\"alert alert-info fade in\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" ng-click=\"closeAlert()\" ng-if=\"alert.closable\"><span aria-hidden=\"true\">&times;</span></button> <strong>Info!</strong>&nbsp;<span ng-bind-html=\"alert.message\"></span></div></div></div>");
$templateCache.put("app/templates/btn.survey.buttons.html","<button ng-show=\"config.canSave\" class=\"btn btn-primary btn btn-secondary\" ng-click=\"save()\">&#9745; Save</button> <button ng-show=\"config.canClear\" class=\"btn btn-primary btn btn-secondary\" ng-click=\"onClear()\">&#9746; Clear</button> <button ng-show=\"config.canEdit\" class=\"btn btn-primary btn btn-secondary\" ng-click=\"onEdit()\">&#9744; Edit</button>");
$templateCache.put("app/templates/btn.survey.comment.html","<div class=\"question-comment\"><div><div class=\"wrapper text-right\"><button ng-click=\"onPostComment()\" class=\"btn btn-default\">Post comment</button></div></div></div>");
$templateCache.put("app/templates/btn.survey.dialog.comment.html","<div class=\"modal-header\" show=\"modalDialogCtrl.dialogModel.showTitle\"><button type=\"button\" class=\"close\" ng-click=\"modalDialogCtrl.close()\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\" ng-bind-html=\"modalDialogCtrl.dialogModel.title\"></h4></div><div class=\"modal-body\" show=\"modalDialogCtrl.dialogModel.showMessage\"><p ng-bind-html=\"modalDialogCtrl.dialogModel.message\"></p><textarea placeholder=\"{{modalDialogCtrl.dialogModel.customLabel}}\" class=\"form-control\" rows=\"1\" ng-model=\"modalDialogCtrl.dialogModel.answer.comment\"></textarea></div><div class=\"modal-footer\"><div ng-repeat=\"button in modalDialogCtrl.dialogModel.buttons\"><button type=\"button\" ng-click=\"button.action(modalDialogCtrl.modalInstance, modalDialogCtrl.value)\" class=\"btn btn-default\" data-dismiss=\"modal\" ng-bind-html=\"button.label\"></button></div></div>");
$templateCache.put("app/templates/btn.survey.dialog.question.html","<div class=\"modal-header\" show=\"modalDialogCtrl.dialogModel.showTitle\"><button type=\"button\" class=\"close\" ng-click=\"modalDialogCtrl.close()\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><h4 class=\"modal-title\" ng-bind-html=\"modalDialogCtrl.dialogModel.title\"></h4></div><div class=\"modal-body\" show=\"modalDialogCtrl.dialogModel.showMessage\"><p ng-bind-html=\"modalDialogCtrl.dialogModel.question.name\"></p><p><small ng-bind-html=\"modalDialogCtrl.dialogModel.question.descriptions.Default\"></small></p><p><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'text\'\"><btn-survey-graph-modal-window-question-answer-text question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\"></btn-survey-graph-modal-window-question-answer-text></div><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'select\'\"><btn-survey-graph-modal-window-question-answer-select question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\"></btn-survey-graph-modal-window-question-answer-select></div><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'picklist\'\"><btn-survey-graph-modal-window-question-answer-picklist question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\"></btn-survey-graph-modal-window-question-answer-picklist></div><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'percent\'\"><btn-survey-graph-modal-window-question-answer-percent question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\"></btn-survey-graph-modal-window-question-answer-percent></div><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'positiveInteger\'\"><btn-survey-graph-modal-window-question-answer-positiveinteger question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\"></btn-survey-graph-modal-window-question-answer-positiveinteger></div><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'email\'\"><btn-survey-graph-modal-window-question-answer-email question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\" users=\"modalDialogCtrl.dialogModel.users\"></btn-survey-graph-modal-window-question-answer-email></div><div ng-if=\"modalDialogCtrl.dialogModel.question.type === \'info\' || modalDialogCtrl.dialogModel.question.type === \'error\'\"><btn-survey-graph-modal-window-question-answer-info question=\"modalDialogCtrl.dialogModel.question\" answer=\"modalDialogCtrl.dialogModel.answer\" config=\"modalDialogCtrl.dialogModel.config\"></btn-survey-graph-modal-window-question-answer-info></div></p></div><div class=\"modal-footer\"><div ng-repeat=\"button in modalDialogCtrl.dialogModel.buttons\"><button type=\"button\" ng-click=\"button.action(modalDialogCtrl.modalInstance, modalDialogCtrl.value)\" class=\"btn btn-default\" data-dismiss=\"modal\" ng-bind-html=\"button.label\"></button></div></div>");
$templateCache.put("app/templates/btn.survey.graph.html","<div class=\"panel panel-default\"><div class=\"panel-heading\" ng-bind-html=\"template.name\"></div><div class=\"panel-body\"><small ng-bind-html=\"template.description\"></small><btn-survey-graph-map template=\"template\" survey=\"survey\" height=\"600\" items=\"items\"></btn-survey-graph-map></div></div>");
$templateCache.put("app/templates/btn.survey.graph.map.html","<div class=\"graph-container\"><div class=\"graph\"></div><div class=\"disable-selection\"><button type=\"button\" class=\"btn btn-default\" ng-click=\"home()\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Default settings\">&#8962;</button> <button type=\"button\" class=\"btn btn-default\" ng-click=\"zoomIn()\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Zoom In\">&#43;</button> <button type=\"button\" class=\"btn btn-default\" ng-click=\"zoomOut()\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Zoom Out\">&#8722;</button></div></div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.html","<div class=\"panel panel-default\"><div class=\"panel-heading\" ng-bind-html=\"template.name\"></div><div class=\"panel-body\"><small ng-bind=\"template.description\"></small><div class=\"wrapper text-center button-block-padding-bottom row\"><btn-survey-buttons></btn-survey-buttons></div><input type=\"search\" ng-model=\"name\" placeholder=\"Find...\" aria-label=\"Find...\" class=\"form-control input-search\"><ul class=\"row list-serach\"><div ng-repeat=\"item in items | orderBy: \'question.order\' | filter:name as results\"><btn-survey-graph-modal-window-question answer=\"item.answer\" question=\"item.question\" class=\"col-xs-12 col-sm-6 col-md-3 col-lg-2 slide-left\" ng-animate=\"\'animate\'\"></btn-survey-graph-modal-window-question></div></ul><div class=\"wrapper text-center row\"><btn-survey-buttons></btn-survey-buttons></div></div></div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.email.html","<select ng-if=\"question.readOnly == false\" ng-init=\"answer.value = answer.value || question.options[0].id\" ng-model=\"answer.value\" class=\"form-control\" ng-options=\"item.id as item.name for item in question.options\"></select><p ng-if=\"question.readOnly\">{{answer.value}}</p>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.info.html","<span ng-bind-html=\"answer.value\"></span>&nbsp; &nbsp;<div ng-if=\"answer.value\" class=\"question-select-description\"><span ng-if=\"answer.description\" class=\"tooltiptext\" ng-bind-html=\"answer.description\"></span> <span ng-if=\"!answer.description\" class=\"tooltiptext\" ng-bind-html=\"\'Brak opisu. <br> Poproś admina o uzupełenienie!\'\"></span></div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.percent.html","<label>{{value.percent}}%</label><div ng-if=\"question.readOnly == false\"><input type=\"range\" id=\"myRange\" ng-model=\"value.percent\"></div><div ng-if=\"question.readOnly\"><input type=\"range\" id=\"myRange\" ng-model=\"value.percent\" disabled=\"true\"></div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.picklist.html","<select ng-if=\"question.readOnly == false\" ng-init=\"answer.value = answer.value || question.options[0].id\" ng-model=\"answer.value\" class=\"form-control\" ng-options=\"option.id as option.name for option in question.options\"></select><p ng-if=\"question.readOnly\">{{answer.value}}</p><p class=\"question-picklist-description\"><small ng-if=\"question.descriptions[answer.value]\" ng-bind-html=\"question.descriptions[answer.value]\"></small></p>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.positiveinteger.html","<div ng-if=\"question.readOnly == false\"><input type=\"numeric\" class=\"form-control\" onkeypress=\"return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57\" min=\"0\" ng-model=\"answer.value\"></div><div ng-if=\"question.readOnly\">{{answer.value}}</div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.select.html","<div ng-if=\"question.readOnly == false\" ng-repeat=\"option in question.options\" class=\"radio\"><label><input type=\"radio\" value=\"{{option.id}}\" ng-model=\"answer.value\"> {{option.name}} &nbsp;&nbsp;<div ng-if=\"option.description\" class=\"question-select-description\"><span class=\"tooltiptext\" ng-bind-html=\"option.description\"></span></div></label></div><div ng-if=\"question.readOnly\">{{answer.value}}</div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.answer.text.html","<div ng-if=\"question.readOnly == false\"><textarea class=\"form-control\" rows=\"5\" ng-model=\"answer.value\"></textarea></div><div ng-if=\"question.readOnly\" ng-bind-html=\"answer.value\"></div>");
$templateCache.put("app/templates/btn.survey.graph.modal.window.question.html","<li class=\"list-serach-element question-padding-15 panel panel-success\" ng-click=\"onClickQuestion()\"><div class=\"question-padding-bottom small-grid-question-text\" ng-bind-html=\"question.name\"></div></li>");
$templateCache.put("app/templates/btn.survey.grid.html","<div class=\"panel panel-default\"><div class=\"panel-heading\" ng-bind-html=\"template.name\"></div><div class=\"panel-body\"><small ng-bind-html=\"template.description\"></small><div class=\"text-center button-block-padding-bottom row\"><btn-survey-buttons></btn-survey-buttons></div><input type=\"search\" ng-model=\"name\" placeholder=\"Find...\" aria-label=\"Find...\" class=\"form-control input-search\"><ul class=\"row flex-container list-serach\"><div ng-repeat=\"item in items | orderBy: \'question.order\' | filter:name as results\" class=\"col-xs-12 col-sm-6 col-md-4 col-lg-3 slide-left\" ng-animate=\"\'animate\'\"><btn-survey-grid-question answer=\"item.answer\" question=\"item.question\"></btn-survey-grid-question></div></ul><div class=\"text-center row\"><btn-survey-buttons></btn-survey-buttons></div></div></div>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.email.html","<select ng-if=\"question.readOnly == false\" ng-init=\"answer.value = answer.value || question.options[0].id\" ng-model=\"answer.value\" class=\"form-control\" ng-options=\"item.id as item.name for item in question.options\"></select><p ng-if=\"question.readOnly\">{{answer.value}}</p>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.info.html","<span ng-bind-html=\"answer.value\"></span>&nbsp; &nbsp;<div ng-if=\"answer.value\" class=\"question-select-description\"><span ng-if=\"answer.description\" class=\"tooltiptext\" ng-bind-html=\"answer.description\"></span> <span ng-if=\"!answer.description\" class=\"tooltiptext\" ng-bind-html=\"\'Brak opisu. <br> Poproś admina o uzupełenienie!\'\"></span></div>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.percent.html","<label>{{value.percent}}%</label><div ng-if=\"question.readOnly == false\"><input type=\"range\" id=\"myRange\" ng-model=\"value.percent\"></div><div ng-if=\"question.readOnly\"><input type=\"range\" id=\"myRange\" ng-model=\"value.percent\" disabled=\"true\"></div>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.picklist.html","<select ng-if=\"question.readOnly == false\" ng-init=\"answer.value = answer.value || question.options[0].id\" ng-model=\"answer.value\" class=\"form-control\" ng-options=\"option.id as option.name for option in question.options\"></select><p ng-if=\"question.readOnly\">{{answer.value}}</p><p class=\"question-picklist-description\"><small ng-if=\"question.descriptions[answer.value]\" ng-bind-html=\"question.descriptions[answer.value]\"></small></p>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.positiveinteger.html","<div ng-if=\"question.readOnly == false\"><input type=\"numeric\" class=\"form-control\" onkeypress=\"return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57\" min=\"0\" ng-model=\"answer.value\"></div><div ng-if=\"question.readOnly\">{{answer.value}}</div>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.select.html","<div ng-if=\"question.readOnly == false\" ng-repeat=\"option in question.options\" class=\"radio\"><label><input type=\"radio\" value=\"{{option.id}}\" ng-model=\"answer.value\"> {{option.name}} &nbsp;&nbsp;<div ng-if=\"option.description\" class=\"question-select-description\"><span class=\"tooltiptext\" ng-bind-html=\"option.description\"></span></div></label></div><div ng-if=\"question.readOnly\">{{answer.value}}</div>");
$templateCache.put("app/templates/btn.survey.grid.question.answer.text.html","<div ng-if=\"question.readOnly == false\"><textarea class=\"form-control\" rows=\"5\" ng-model=\"answer.value\"></textarea></div><div ng-if=\"question.readOnly\" ng-bind-html=\"answer.value\"></div>");
$templateCache.put("app/templates/btn.survey.grid.question.html","<li class=\"list-serach-element question-padding-15 panel panel-success\"><div class=\"question-padding-bottom\"><p ng-bind-html=\"question.name\"></p><p><small ng-bind-html=\"question.descriptions.default\"></small></p></div><div ng-if=\"question.type === \'text\'\"><btn-survey-grid-question-answer-text question=\"question\" answer=\"answer\"></btn-survey-grid-question-answer-text></div><div ng-if=\"question.type === \'select\'\"><btn-survey-grid-question-answer-select question=\"question\" answer=\"answer\"></btn-survey-grid-question-answer-select></div><div ng-if=\"question.type === \'picklist\'\"><btn-survey-grid-question-answer-picklist question=\"question\" answer=\"answer\"></btn-survey-grid-question-answer-picklist></div><div ng-if=\"question.type === \'percent\'\"><btn-survey-grid-question-answer-percent question=\"question\" answer=\"answer\"></btn-survey-grid-question-answer-percent></div><div ng-if=\"question.type === \'positiveInteger\'\"><btn-survey-grid-question-answer-positiveinteger question=\"question\" answer=\"answer\" config=\"config\"></btn-survey-grid-question-answer-positiveinteger></div><div ng-if=\"question.type === \'email\'\"><btn-survey-grid-question-answer-email question=\"question\" answer=\"answer\" config=\"config\" users=\"users\"></btn-survey-grid-question-answer-email></div><div ng-if=\"question.type === \'info\' || question.type === \'error\'\"><btn-survey-grid-question-answer-info question=\"question\" answer=\"answer\"></btn-survey-grid-question-answer-info></div></li>");
$templateCache.put("app/templates/btn.survey.html","<div ng-show=\"canChangeMode == true\"><div class=\"panel panel-default\"><div class=\"panel-heading\"><button type=\"button\" ng-if=\"cfg.showConfig == true\" class=\"settings-button btn btn-secondary\" ng-click=\"showConfigurationPanel()\">&#9881;</button> Configuration</div><div class=\"panel-body configuration-panel\"><form><select class=\"form-control\" ng-options=\"item as item.name for item in modes\" ng-model=\"selectedMode\"></select></form></div></div></div><div ng-if=\"Object.keys(template).length !== 0\"><div ng-if=\"selectedMode.id === \'list\'\"><btn-survey-list></btn-survey-list></div><div ng-if=\"selectedMode.id === \'grid\'\"><btn-survey-grid></btn-survey-grid></div><div ng-if=\"selectedMode.id === \'graph\'\"><btn-survey-graph></btn-survey-graph></div><div ng-if=\"selectedMode.id === \'smallGrid\'\"><btn-survey-graph-modal-window></btn-survey-graph-modal-window></div><div ng-if=\"selectedMode.id === \'achievementGrid\'\"><btn-survey-achievement-grid></btn-survey-achievement-grid></div></div>");
$templateCache.put("app/templates/btn.survey.list.html","<div class=\"panel panel-default\"><div class=\"panel-heading\" ng-bind-html=\"template.name\"></div><div class=\"panel-body\"><small ng-bind-html=\"template.description\"></small><div class=\"wrapper text-center button-block-padding-bottom\"><btn-survey-buttons></btn-survey-buttons></div><input type=\"search\" ng-model=\"name\" placeholder=\"Find...\" aria-label=\"Find...\" class=\"form-control input-search\"><ul class=\"list-serach\"><div ng-repeat=\"item in items | orderBy: \'question.order\' | filter:name as results\" class=\"slide-left\" ng-animate=\"\'animate\'\"><btn-survey-list-question answer=\"item.answer\" question=\"item.question\"></btn-survey-list-question></div></ul><div class=\"wrapper text-center\"><btn-survey-buttons></btn-survey-buttons></div></div></div>");
$templateCache.put("app/templates/btn.survey.list.question.answer.email.html","<select ng-if=\"question.readOnly == false\" ng-init=\"answer.value = answer.value || question.options[0].id\" ng-model=\"answer.value\" class=\"form-control\" ng-options=\"item.id as item.name for item in question.options\"></select><p ng-if=\"question.readOnly\">{{answer.value}}</p>");
$templateCache.put("app/templates/btn.survey.list.question.answer.info.html","<span ng-bind-html=\"answer.value\"></span>&nbsp; &nbsp;<div ng-if=\"answer.value\" class=\"question-select-description\"><span ng-if=\"answer.description\" class=\"tooltiptext\" ng-bind-html=\"answer.description\"></span> <span ng-if=\"!answer.description\" class=\"tooltiptext\" ng-bind-html=\"\'Brak opisu. <br> Poproś admina o uzupełenienie!\'\"></span></div>");
$templateCache.put("app/templates/btn.survey.list.question.answer.percent.html","<label>{{value.percent}}%</label><div ng-if=\"question.readOnly == false\"><input type=\"range\" id=\"myRange\" ng-model=\"value.percent\"></div><div ng-if=\"question.readOnly\"><input type=\"range\" id=\"myRange\" ng-model=\"value.percent\" disabled=\"true\"></div>");
$templateCache.put("app/templates/btn.survey.list.question.answer.picklist.html","<select ng-if=\"question.readOnly == false\" ng-init=\"answer.value = answer.value || question.options[0].id\" ng-model=\"answer.value\" class=\"form-control\" ng-options=\"option.id as option.name for option in question.options\"></select><p ng-if=\"question.readOnly\">{{answer.value}}</p><p class=\"question-picklist-description\"><small ng-if=\"question.descriptions[answer.value]\" ng-bind-html=\"question.descriptions[answer.value]\"></small></p>");
$templateCache.put("app/templates/btn.survey.list.question.answer.positiveinteger.html","<div ng-if=\"question.readOnly == false\"><input type=\"numeric\" class=\"form-control\" onkeypress=\"return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57\" min=\"0\" ng-model=\"answer.value\"></div><div ng-if=\"question.readOnly\">{{answer.value}}</div>");
$templateCache.put("app/templates/btn.survey.list.question.answer.select.html","<div ng-if=\"question.readOnly == false\" ng-repeat=\"option in question.options\" class=\"radio\"><label><input type=\"radio\" value=\"{{option.id}}\" ng-model=\"answer.value\"> {{option.name}} &nbsp;&nbsp;<div ng-if=\"option.description\" class=\"question-select-description\"><span class=\"tooltiptext\" ng-bind-html=\"option.description\"></span></div></label></div><div ng-if=\"question.readOnly\">{{answer.value}}</div>");
$templateCache.put("app/templates/btn.survey.list.question.answer.text.html","<div ng-if=\"question.readOnly == false\"><textarea class=\"form-control\" rows=\"5\" ng-model=\"answer.value\"></textarea></div><div ng-if=\"question.readOnly\" ng-bind-html=\"answer.value\"></div>");
$templateCache.put("app/templates/btn.survey.list.question.html","<li class=\"list-serach-element\"><div class=\"question-padding-15 panel panel-success\"><div class=\"question-padding-bottom\"><p ng-bind-html=\"question.name\"></p><p><small ng-bind-html=\"question.descriptions.default\"></small></p></div><div ng-if=\"question.type === \'text\' && questionConfig.text\"><btn-survey-list-question-answer-text question=\"question\" answer=\"answer\"></btn-survey-list-question-answer-text></div><div ng-if=\"question.type === \'select\' && questionConfig.select\"><btn-survey-list-question-answer-select question=\"question\" answer=\"answer\"></btn-survey-list-question-answer-select></div><div ng-if=\"question.type === \'picklist\' && questionConfig.picklist\"><btn-survey-list-question-answer-picklist question=\"question\" answer=\"answer\"></btn-survey-list-question-answer-picklist></div><div ng-if=\"question.type === \'percent\' && questionConfig.percent\"><btn-survey-list-question-answer-percent question=\"question\" answer=\"answer\"></btn-survey-list-question-answer-percent></div><div ng-if=\"question.type === \'positiveInteger\' && questionConfig.positiveInteger\"><btn-survey-list-question-answer-positiveinteger question=\"question\" answer=\"answer\" config=\"config\"></btn-survey-list-question-answer-positiveinteger></div><div ng-if=\"question.type === \'email\' && questionConfig.email\"><btn-survey-list-question-answer-email question=\"question\" answer=\"answer\" config=\"config\" users=\"users\"></btn-survey-list-question-answer-email></div><div ng-if=\"question.type === \'info\' && questionConfig.info\"><btn-survey-list-question-answer-info question=\"question\" answer=\"answer\"></btn-survey-list-question-answer-info></div><div ng-if=\"question.type === \'error\' && questionConfig.error\"><btn-survey-list-question-answer-info question=\"question\" answer=\"answer\"></btn-survey-list-question-answer-info></div></div></li>");
$templateCache.put("app/templates/btn.survey.type.html","<div class=\"container\"><div ng-view=\"\"></div><btn-survey-alert></btn-survey-alert><div class=\"panel panel-default\"><div class=\"panel-heading\">Select survey</div><div class=\"panel-body\"><form><div class=\"form-group\"><label>Templates:</label><select class=\"form-control\" ng-options=\"item as item.name for item in templates\" ng-model=\"selectedTemplate\" ng-change=\"hideSurvey()\"></select></div><div class=\"form-group\"><label>Surveys:</label><ul class=\"list-group\" ng-if=\"selectedTemplate.surveys.length > 0\"><li class=\"list-group-item\" ng-if=\"selectedTemplate.multiSurvey\"><button class=\"btn btn-primary btn btn-secondary\" ng-click=\"newSurvey()\">New</button></li><li class=\"list-group-item\" ng-repeat=\"survey in selectedTemplate.surveys\">{{survey.name}} <span class=\"right-align\"><button class=\"btn btn-primary btn btn-secondary\" ng-click=\"editSurvey(survey)\" ng-model=\"currentSurvey\">Edit</button><button class=\"btn btn-primary btn btn-secondary\" ng-if=\"template.submitable\">Submit</button></span></li></ul><ul class=\"list-group\" ng-if=\"selectedTemplate.surveys.length == 0\"><li class=\"list-group-item\"><button class=\"btn btn-primary btn btn-secondary\" ng-click=\"newSurvey()\">New</button></li></ul></div></form></div></div><div ng-if=\"showSurvey\"><btn-survey template=\"template\" survey=\"survey\" mode=\"mode\" users=\"users\" can-change-mode=\"canChangeMode\" config=\"cfg\" user=\"user\" save=\"save()\" onclear=\"onClear()\" onedit=\"onEdit()\"></btn-survey></div></div>");
$templateCache.put("app/views/mainmenu/mainmenu.view.html","<span>{{mmc.message}}</span>");}]);