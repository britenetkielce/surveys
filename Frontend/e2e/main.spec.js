'use strict';

describe('The main view', function () {
  var page;

  beforeEach(function () {
    browser.get('/index.html');
    page = require('./main.po');
  });

  it('should write something down there', function() {
    expect(page.h1El.getText()).toBe('Angular Project working!');
  });

});
