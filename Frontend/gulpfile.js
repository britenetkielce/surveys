/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var server = require('gulp-server-livereload');
var watch = require('gulp-watch');
var uglify = require("gulp-uglify");

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
    return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
    require('./gulp/' + file);
});

/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

gulp.task('webserver', function() {
    gulp.src('./dist/')
        .pipe(server({
            livereload: true,
            open: 'http://localhost:8000/dist/'
    }));
});

gulp.task('watch',  function() {
    gulp.watch("./src/**",  ['devbuild']);
});

gulp.task('serv', ['webserver', 'watch']);

gulp.task('minify-js', function () {
    gulp.src('./**/.js') // path to your files
    .pipe(uglify())
    .pipe(gulp.dest('app/scripts'));
});

gulp.task('deploy', ['sfBuild', 'sfDeploy']);