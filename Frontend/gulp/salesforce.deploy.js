'use strict';

/**
 *  Creating StaticResource package and deployment to Salesforce
 */

var gulp = require('gulp');
var zip = require('gulp-zip');
var conf = require('./conf');
var path = require('path');
var file = require('gulp-file');
var gutil = require('gulp-util');

var jsforce = require('gulp-jsforce-deploy');
var sfconf = require('./salesforce.conf');

var tmpDirectory = path.join(conf.paths.tmp, '/sfDeploy/');
var deployPackDirectory = ('deployData/');

gulp.task('sfMetafile', function() {
  var resourceMeta = [ '<?xml version="1.0" encoding="UTF-8"?>',
                       '<StaticResource xmlns="http://soap.sforce.com/2006/04/metadata">',
                       '    <cacheControl>' + sfconf.staticResource.cacheControl + '</cacheControl>',
                       '    <contentType>application/x-zip-compressed</contentType>',
                       '    <description>' + sfconf.staticResource.description + '</description>',
                       '</StaticResource>'
                      ];

  gutil.log(gutil.colors.bgBlue('[SALESFORCE]'), sfconf.staticResource.name.concat('-meta.xml') + ' created');

  return file(sfconf.staticResource.name.concat('.resource-meta.xml'), resourceMeta.join('\n')).pipe(gulp.dest(tmpDirectory + deployPackDirectory + '/staticresources/'));
});

gulp.task('sfPackageXml', function() {
  var packageXml = [ '<?xml version="1.0" encoding="UTF-8"?>',
                     '<Package xmlns="http://soap.sforce.com/2006/04/metadata">',
                     '  <types>',
                     '    <members>' + sfconf.staticResource.name + '</members>',
                     '    <name>StaticResource</name>',
                     '  </types>',
                     '  <version>' + sfconf.api.version + '</version>',
                     '</Package>'
                   ];
  gutil.log(gutil.colors.bgBlue('[SALESFORCE]'), 'package.xml created');

  return file('package.xml', packageXml.join('\n'))
          .pipe(gulp.dest(tmpDirectory + deployPackDirectory));
});

gulp.task('sfStaticResource', ['build'], function() {
  gutil.log(gutil.colors.bgBlue('[SALESFORCE]'), sfconf.staticResource.name + ' static resource created');
  return gulp.src([
    path.join(conf.paths.dist + '/**/*'),
  ])
    .pipe(zip(sfconf.staticResource.name + '.resource'))
    .pipe(gulp.dest(tmpDirectory + deployPackDirectory + '/staticresources/'));
});

gulp.task('sfBuild', ['sfMetafile', 'sfPackageXml', 'sfStaticResource'], function() {
  gutil.log(gutil.colors.bgBlue('[BUILD]'), 'completed');
});

gulp.task('sfDeploy', function() {
  gutil.log(gutil.colors.bgBlue('[DEPLOYING TO SALESFORCE]'));
  return gulp.src(tmpDirectory + '**/*')
    .pipe(zip('deployData.zip'))
    .pipe(gulp.dest('.tmp/'))
    .pipe(jsforce({
      username: sfconf.org.username,
      password: sfconf.org.password + sfconf.org.securityToken,
      loginUrl: sfconf.org.domain
    }));  
});